//
//  StopServicesViewController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 14/12/21.
//

import UIKit

class StopServicesViewController: UIViewController {

    @IBOutlet weak var nameTF: CustomInput!
    @IBOutlet weak var phoneTF: CustomInput!
    @IBOutlet weak var problemTF: CustomInput!
    @IBOutlet weak var problemTextView: UITextView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarController?.tabBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameTF.placeholder = "Ваше имя"
        phoneTF.placeholder = "Номер телефона или email"
        problemTF.placeholder = "C чем у вас возникли проблемы?"
        problemTF.isHidden = true
        
        problemTextView.text = "C чем у вас возникли проблемы?"
        problemTextView.textColor = UIColor.lightGray
        problemTextView.delegate = self
        
        nameTF.textField.textContentType = .name
        phoneTF.textField.textContentType = .emailAddress
        problemTF.textField.textContentType = .none
        
        nameTF.hideIcon(.all)
        phoneTF.hideIcon(.all)
        problemTF.hideIcon(.all)
        
        let data = State.currentPersonalData
        nameTF.textField.text = data?.fullName
        phoneTF.textField.text = data?.phoneNumber.first?.phone
    }

    @IBAction func backTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendTapped(_ sender: Any) {
        
        guard let name = nameTF.textField.text, name.containsText(),
              let contact = phoneTF.textField.text,
              let message = problemTextView.text, message.containsText() else {
            ApiManager.shared.errorAlert(text: "Нужные поля не заполнены")
            return
        }
        
        guard contact.containsPhone() || contact.containsEmail() else {
            if !contact.containsPhone()  {
                ApiManager.shared.errorAlert(text: "Поле номера телефона заполнено некорректно")
            } else if !contact.containsEmail() {
                ApiManager.shared.errorAlert(text: "Поле Email заполнено некорректно")
            }
            return
        }
        
        ApiManager.shared.sendSupport(
            name: name,
            email: contact,
            phone: contact,
            message: message,
            subject: .service
        ) { info in
            ApiManager.shared.errorAlert(title: info, text: "") {
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}

extension StopServicesViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "C чем у вас возникли проблемы?"
            textView.textColor = UIColor.lightGray
        }
    }
}
