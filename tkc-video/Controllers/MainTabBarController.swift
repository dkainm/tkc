//
//  MainTabBarController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 20/9/21.
//

import UIKit
import SVProgressHUD

class MainTabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchDataForNextViews()
    }
    
    public func fetchDataForNextViews() {
        
        SVProgressHUD.show()
        
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)
        
        let navigationController = viewControllers?[0] as! UINavigationController
        let menuController = navigationController.viewControllers.first as! MenuViewController
        let personalController = (viewControllers?[1] as! UINavigationController).viewControllers.first as! PersonalViewController
        
        menuController.update = true
        
        ApiManager.shared.getUserData { (personalData) in
            personalController.personalData = personalData
            State.currentPersonalData = personalData
            
            NotificationManager.shared.updateServerMessages()
            
                DispatchQueue.main.async {
                    blurEffectView.removeFromSuperview()
                    SVProgressHUD.dismiss()
                }
        }
        
        
        
    }
    
}

extension MainTabBarController: RegistrationDelegate {
    
    func updateAppData() {
        fetchDataForNextViews()
    }
    
}
