//
//  PersonalOffersViewController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 20/12/21.
//

import UIKit

class PersonalOffersViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    enum ControllerType {
        case offers
        case akcis
    }
    
    var type: ControllerType!
    
    var offers = [PersonalOffer]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if type == ControllerType.akcis
        { titleLabel.text = "Акции" }
        
        tableView.register(UINib(nibName: "PersonalOfferTableViewCell", bundle: nil), forCellReuseIdentifier: "personalOfferCell")
        
        fetchData()
    }
    
    private func fetchData() {
        switch type {
        case .offers:
            ApiManager.shared.getPersonalOffers { [weak self] (offers) in
                self?.offers = offers
                self?.tableView.reloadData()
            }
        case .akcis:
            ApiManager.shared.getPersonalAkcis { [weak self] (offers) in
                self?.offers = offers
                self?.tableView.reloadData()
            }
        case .none: break
        }
    }

    @IBAction func backTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}

extension PersonalOffersViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return offers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "personalOfferCell", for: indexPath) as! PersonalOfferTableViewCell
        cell.configure(offer: offers[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let offer = offers[indexPath.row]
        let controller = storyboard?.instantiateViewController(withIdentifier: "PersonalOfferViewController") as! PersonalOfferViewController
        controller.offer = offer
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
