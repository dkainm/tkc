//
//  AdvertismentViewController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 14/12/21.
//

import UIKit

class AdvertismentViewController: UIViewController {

    @IBOutlet var views: [UIView]!
    
    @IBOutlet weak var personalOfferView: UIView!
    @IBOutlet weak var akciView: UIView!
    @IBOutlet weak var tariffsAndServicesView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setElements()
    }
    
    private func setElements() {
        
        //Shadows
        for adView in views {
            adView.layer.shadowColor = UIColor.black.cgColor
            adView.layer.shadowOffset = CGSize(width: 0, height: 4)
            adView.layer.shadowRadius = 15
            adView.layer.shadowOpacity = 0.4
        }
        
        //Gestures
        for adView in views {
            adView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(adTapped(_:))))
        }
    }
    
    @objc private func adTapped(_ recognizer: UITapGestureRecognizer) {
        guard let tag = recognizer.view?.tag else { return }
        views[tag].showAnimation() { [weak self] in
            
            var controller: UIViewController?
            
            switch tag {
            case 0:
                let offersController = self?.storyboard?.instantiateViewController(withIdentifier: "PersonalOffersViewController") as! PersonalOffersViewController
                offersController.type = .offers
                controller = offersController
            case 1:
                let akcisController = self?.storyboard?.instantiateViewController(withIdentifier: "PersonalOffersViewController") as! PersonalOffersViewController
                akcisController.type = .akcis
                controller = akcisController
            case 2:
                controller = self?.storyboard?.instantiateViewController(withIdentifier: "TariffsViewController") as! TariffsViewController
            default: break
            }
            
            guard let controllerToPush = controller else { return }
            self?.navigationController?.pushViewController(controllerToPush, animated: true)
        }
                
    }

    @IBAction func backTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
}
