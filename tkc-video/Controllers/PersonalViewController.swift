//
//  PersonalViewController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 17/9/21.
//

import UIKit
import SVProgressHUD
import AlignedCollectionViewFlowLayout

class PersonalViewController: UIViewController {

    @IBOutlet weak var cvHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var upView: UIView!
    @IBOutlet weak var middleView: UIView!
    
    @IBOutlet weak var personalButtonAnimationView: UIView!
    @IBOutlet weak var notificationsButtonView: UIView!
    @IBOutlet weak var badgeView: UIView!
    @IBOutlet weak var badgeLabel: UILabel!
    
    @IBOutlet weak var upStackView: UIStackView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var accountIdLabel: UILabel!
    @IBOutlet weak var payBeforeDateLabel: UILabel!
    @IBOutlet weak var toPayLabel: UILabel!
    @IBOutlet weak var tariffPriceLabel: UILabel!
    @IBOutlet weak var tariffNameLabel: UILabel!
    @IBOutlet weak var tariffButtonsView: UIView!
    @IBOutlet weak var tariffButtonsViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var loginStack: UIStackView!
    
    @IBOutlet weak var servicesTitle: UILabel!
    @IBOutlet weak var servicesStack: UIStackView!
    
    @IBOutlet var services: [UIView]!
    @IBOutlet var serviceImages: [UIImageView]!
    @IBOutlet var serviceNames: [UILabel]!
    
    @IBOutlet weak var payStack: UIStackView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var personalData: PersonalData!
    
    var personalServices: [Int: PersonalDataService] {
        var servcs = [Int: PersonalDataService]()
        for service in personalData.services {
            switch service.type {
            case "box":
                servcs[1] = service
            case "tvbox":
                servcs[0] = service
            case "staticip":
                servcs[2] = service
            case "plc":
                servcs[3] = service
            default: continue
            }
        }
        return servcs
    }
    
    var personalButtonAnimating = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchData()
        setUpViews()
        setValues()
        setGestures()
        setHeightToCV()
        
        scrollView.refreshControl = UIRefreshControl()
        scrollView.refreshControl?.tintColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.6)
        scrollView.refreshControl?.addTarget(self, action: #selector(didPullToRefresh), for: .valueChanged)
        
        collectionView.register(UINib(nibName: "TariffCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "tarif")
        
        let alignedFlowLayout = AlignedCollectionViewFlowLayout(
            horizontalAlignment: .left,
            verticalAlignment: .top
        )
        let width: CGFloat = {
            let cvWidth = (collectionView.bounds.width-8)/2
            return cvWidth
        }()
        alignedFlowLayout.estimatedItemSize = .init(width: width, height: 42)
        collectionView.collectionViewLayout = alignedFlowLayout
        collectionView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        startAnimatingButton()
        badgeUpdate()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopAnimatingButton()
    }
    
    private func startAnimatingButton() {
        let color = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0.20)
        
        personalButtonAnimationView.layer.cornerRadius = 4
        personalButtonAnimationView.layer.borderColor = color.cgColor
        personalButtonAnimationView.layer.borderWidth = 0
        personalButtonAnimationView.alpha = 0
        
        UIView.transition(
            with: personalButtonAnimationView,
            duration: 1.25,
            options: [.repeat, .curveEaseOut]) {
            self.personalButtonAnimationView.transform = CGAffineTransform(scaleX: 1.3, y: 1.4)
            self.personalButtonAnimationView.layer.borderWidth = 4
            self.personalButtonAnimationView.alpha = 1
        }

    }
    
    private func stopAnimatingButton() {
        personalButtonAnimationView.layer.removeAllAnimations()
        personalButtonAnimationView.transform = CGAffineTransform(scaleX: 0.76, y: 0.71)
    }
    
    private func fetchData() {
        ApiManager.shared.getUserData { [weak self] (personalData) in
            self?.scrollView.refreshControl?.endRefreshing()
            
            State.currentPersonalData = personalData
            self?.personalData = personalData
            self?.setValues()
            self?.collectionView.reloadData()
            self?.setHeightToCV()
        }
        
        NotificationManager.shared.updateBadgeCount()
    }
    
    @objc private func didPullToRefresh() {
        fetchData()
    }
    
    private func setValues() {
        guard let personalData = personalData else { return }
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        
        let uiDateFormatter = DateFormatter()
        uiDateFormatter.dateFormat = "dd.MM.yyyy"
        
        let balanceDate = uiDateFormatter.string(from: dateFormatter.date(from: personalData.balanceDate)!)
        let payBeforeDate = uiDateFormatter.string(from: dateFormatter.date(from: personalData.payBeforeDate)!)
        
        dateLabel.text = "Баланс на " + balanceDate
        balanceLabel.text = "\(personalData.balance)"
        accountIdLabel.text = "№" + personalData.login
        payBeforeDateLabel.text = "Сумма к оплате до " + payBeforeDate
        toPayLabel.text = "\(personalData.toPay)"
        tariffNameLabel.text = "Подключен тариф «" + personalData.tariffName + "»"
        tariffPriceLabel.text = "Стоимость тарифа \(personalData.tariffPrice)руб./мес."
        
        if State.currentPersonalData.interfaceType == 2 {
            upStackView.isHidden = true
            servicesTitle.isHidden = true
            servicesStack.isHidden = true
            payStack.isHidden = true
            loginStack.isHidden = true
            upView.isHidden = true
            tariffButtonsView.isHidden = true
            tariffButtonsViewHeightConstraint.constant = 0
        } else {
            upStackView.isHidden = false
            servicesTitle.isHidden = false
            servicesStack.isHidden = false
            payStack.isHidden = false
            loginStack.isHidden = false
            upView.isHidden = false
            tariffButtonsView.isHidden = false
            tariffButtonsViewHeightConstraint.constant = 40.5
        }
        
        for service in State.currentPersonalData.services {
            switch service.type {
            case "tvbox":
                serviceImages[0].image = serviceImages[0].image?.tintPicto(UIColor(named: "primary")!)
            case "box":
                serviceImages[1].image = serviceImages[1].image?.tintPicto(UIColor(named: "primary")!)
            case "staticip":
                serviceImages[2].image = serviceImages[2].image?.tintPicto(UIColor(named: "primary")!)
            case "plc":
                serviceImages[3].image = serviceImages[3].image?.tintPicto(UIColor(named: "primary")!)
            default: break
            }
        }
    }
    
    private func setGestures() {
        for service in services {
            service.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(serviceTapped(_:))))
        }
        
        payStack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(payTapped)))
        
        notificationsButtonView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(notificationsTapped)))
        
        NotificationCenter.default.addObserver(self, selector: #selector(badgeUpdate), name: NSNotification.Name("badgeUpdate"), object: nil)
    }
    
    private func setUpViews() {
        view.setBackground(#imageLiteral(resourceName: "redBg"))
        
        services.forEach { (service) in
            service.layer.cornerRadius = 15
            service.layer.shadowColor = UIColor.black.cgColor
            service.layer.shadowOffset = CGSize(width: 0, height: 4)
            service.layer.shadowRadius = 8
            service.layer.shadowOpacity = 0.2
        }
        
        upView.layer.shadowColor = UIColor.black.cgColor
        upView.layer.shadowRadius = 8
        upView.layer.shadowOffset = CGSize(width: 0, height: 4)
        upView.layer.shadowOpacity = 0.2
        
        middleView.layer.shadowColor = UIColor.black.cgColor
        middleView.layer.shadowRadius = 8
        middleView.layer.shadowOffset = CGSize(width: 0, height: 4)
        middleView.layer.shadowOpacity = 0.2
        
        badgeView.layer.borderWidth = 1.5
        badgeView.layer.borderColor = #colorLiteral(red: 0.8196078431, green: 0, blue: 0, alpha: 1)
    }
    
    @objc private func serviceTapped(_ recognizer: UITapGestureRecognizer) {
        if let tag = recognizer.view?.tag {
            services[tag].showAnimation() { [weak self] in
                
                if (self?.personalServices.count)! > tag  {
                    let tariffInfoController = self?.storyboard?.instantiateViewController(withIdentifier: "TariffInfoViewController") as! TariffInfoViewController
                    tariffInfoController.title = self?.personalServices[tag]?.service
                    tariffInfoController.tag = tag
                    tariffInfoController.tariffInfos = self?.personalServices[tag]?.subInfo ?? []
                    tariffInfoController.icon = self?.personalServices[tag]?.icon
                    self?.present(tariffInfoController, animated: true)
                } else {
                    ApiManager.shared.getTariffsAndSubServices { (_, _, _, subServices) in
                        let controller = self?.storyboard?.instantiateViewController(withIdentifier: "SubServicesViewController") as! SubServicesViewController
                        controller.subServices = subServices
                        self?.navigationController?.pushViewController(controller, animated: true)
                    }
                    
                }
                
            }
        }
    }
    
    @IBAction func personalTapped(_ sender: UIButton) {
        let vc = UIStoryboard(name: "Popups", bundle: nil).instantiateViewController(withIdentifier: "PersonalDataPopupViewController") as! PersonalDataPopupViewController
        vc.personalData = personalData
        vc.delegate = self
        vc.modalTransitionStyle = .crossDissolve
        vc.modalPresentationStyle = .overCurrentContext
        present(vc, animated: true)
    }
    
    @IBAction func stopTariff(_ sender: UIButton) {
        let stopTariffController = storyboard?.instantiateViewController(withIdentifier: "StopServicesViewController") as! StopServicesViewController
        navigationController?.pushViewController(stopTariffController, animated: true)
    }
    
    @IBAction func changeTariff(_ sender: Any) {
        let controller = storyboard?.instantiateViewController(withIdentifier: "TariffsViewController") as! TariffsViewController
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @objc func payTapped() {
        payStack.showAnimation { [weak self] in
            if State.currentPersonalData.paymentType.type == 1 {
                let paymentController = UIStoryboard(name: "Popups", bundle: nil).instantiateViewController(withIdentifier: "PaymentViewController") as! PaymentViewController
                paymentController.personal = self?.personalData
                paymentController.modalTransitionStyle = .crossDissolve
                paymentController.modalPresentationStyle = .overCurrentContext
                self?.present(paymentController, animated: true)
            } else {
                let message = State.currentPersonalData.paymentType.message
                showWarningView(text: message, info: nil)
            }
        }
    }
    
    @IBAction func questionTapped(_ sender: UIButton) {
        let webView = storyboard?.instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
        webView.url = URL(string: "https://telecom-service.site/payment-questions/")
        navigationController?.pushViewController(webView, animated: true)
    }
    
    private func setHeightToCV() {
        let tariffs = State.currentPersonalData.tariffs
        
        var height: CGFloat = 42
        if tariffs.count > 1 {
            
            if tariffs.count % 2 != 0 {
                height = CGFloat(52 * ((tariffs.count/2) + 1)) - 9
            } else {
                height = CGFloat(52 * (tariffs.count/2)) - 9
            }
            
        }
        
        cvHeightConstraint.constant = height
    }
    
    @objc private func notificationsTapped() {
        notificationsButtonView.showAnimation() { [weak self] in
            guard let `self` = self else { return }
            let notifications = self.storyboard?.instantiateViewController(withIdentifier: "NotificationsViewController") as! NotificationsViewController
            self.present(notifications, animated: true)
        }
    }
    
    @objc private func badgeUpdate() {
        let badgeCount = UIApplication.shared.applicationIconBadgeNumber
        badgeView.isHidden = (badgeCount == 0)
        badgeLabel.text = (badgeCount < 99) ? "\(badgeCount)" : "99+"
    }
    
}

extension PersonalViewController: PersonalDataPopupDelegate {
    
    func didDismiss(isChangedData: Bool) {
        guard isChangedData else { return }
        fetchData()
    }

}

extension PersonalViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return State.currentPersonalData.tariffs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tarif", for: indexPath) as! TariffCollectionViewCell
        cell.configure(tariff: State.currentPersonalData.tariffs[indexPath.item])
        return cell
    }
}
