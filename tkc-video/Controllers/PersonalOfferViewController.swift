//
//  PersonalOfferViewController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 27/12/21.
//

import UIKit
import Nuke

class PersonalOfferViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var fullTitleLabel: UILabel!
    @IBOutlet weak var fullDescLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var connectButton: UIButton!
    
    var offer: PersonalOffer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Nuke.loadImage(with: offer.imageUrlString, into: imageView)
        
        titleLabel.text = offer.title
        fullTitleLabel.text = offer.previewDesc
        fullDescLabel.text = offer.desc
        
        connectButton.isHidden = offer.isConnecting == "1"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.mm.yyyy"
        
        dateLabel.text = "c \(dateFormatter.string(from: offer.startDate())) по \(dateFormatter.string(from: offer.endDate()))"
    }

    @IBAction func connectTapped(_ sender: Any) {
        let controller = storyboard?.instantiateViewController(withIdentifier: "ApplicationFormViewController") as! ApplicationFormViewController
        controller.selectedTariffs = [offer.title]
        navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}
