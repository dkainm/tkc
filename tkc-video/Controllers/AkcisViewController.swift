//
//  AkcisViewController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 28/12/21.
//

import UIKit

class AkcisViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var akcis = [PersonalOffer]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "PersonalOfferTableViewCell", bundle: nil), forCellReuseIdentifier: "personalOfferCell")
        fetchData()
    }
    
    private func fetchData() {
        ApiManager.shared.getPersonalAkcis { [weak self] (akcis) in
            self?.akcis = akcis
            self?.tableView.reloadData()
        }
    }

    @IBAction func backTapped(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
}

extension AkcisViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return akcis.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "personalOfferCell", for: indexPath) as! PersonalOfferTableViewCell
        cell.configure(offer: akcis[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let akci = akcis[indexPath.row]
        let controller = storyboard?.instantiateViewController(withIdentifier: "PersonalOfferViewController") as! PersonalOfferViewController
        controller.offer = akci
        navigationController?.pushViewController(controller, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
