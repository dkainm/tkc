//
//  SubServicesViewController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 29/12/21.
//

import UIKit

class SubServicesViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var subServices = [SubService]()
    
    var selectedSubServices: [SubService] {
        var sSubServices: [SubService] = []
        for subServiceCell in tableView.visibleCells {
            guard let subServiceCell = subServiceCell as? SubServiceTableViewCell else { break }
            if subServiceCell.isCellSelected { sSubServices.append(subServiceCell.subService) }
        }
        return sSubServices
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.contentInset = UIEdgeInsets(top: 32, left: 0, bottom: 80, right: 0)
        tableView.register(UINib(nibName: "SubServiceTableViewCell", bundle: nil), forCellReuseIdentifier: "subService")
    }

    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func connectTapped(_ sender: Any) {
        guard !selectedSubServices.isEmpty else {
            ApiManager.shared.errorAlert(text: "Ни одна услуга не выбрана")
            return
        }
        let controller = storyboard?.instantiateViewController(withIdentifier: "ApplicationFormViewController") as! ApplicationFormViewController
        controller.selectedSubServices = selectedSubServices
        navigationController?.pushViewController(controller, animated: true)
    }
    
}

extension SubServicesViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subServices.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "subService", for: indexPath) as! SubServiceTableViewCell
        cell.selectionStyle = .none
        cell.configure(subService: subServices[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! SubServiceTableViewCell
        cell.isCellSelected ? cell.deselect() : cell.select()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
