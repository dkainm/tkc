//
//  ApplicationFormViewController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 27/12/21.
//

import UIKit
import Nuke

class ApplicationFormViewController: UIViewController {

    //MARK: – Storyboard Variables
    
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var phoneTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    
    @IBOutlet weak var selectedServicesTextViewView: UIView!
    @IBOutlet weak var selectedServicesTextView: UITextView!
    @IBOutlet weak var commentTextView: UITextView!
    
    @IBOutlet weak var textViewsStackView: UIStackView!
    
    @IBOutlet weak var supportView: UIView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var selectedServicesTextViewHeight: NSLayoutConstraint!
    
    //MARK: – Variables
    
    var selectedServices: [SubService] {
        var selectedCells = [SubService]()
        for cell in collectionView.visibleCells {
            if let serviceCell = cell as? ServiceCollectionViewCell,
               let service = serviceCell.subService {
                if serviceCell.isCellSelected { selectedCells.append(service) }
            }
        }
        return selectedCells
    }
    
    var selectedTariffs: [String]?
    var selectedSubServices: [SubService]?
    
    var subServices = [SubService]()
    
    //MARK: – Internal Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchServices()
        
        supportView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(supportTapped)))
        
        setupCV()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupTextViews()
    }
    
    override func viewDidLayoutSubviews() {
        updateViews()
    }
    
    //MARK: – Private Methods
    
    private func fetchServices() {
        ApiManager.shared.getTariffsAndSubServices { [weak self] (_, _, _, subServices) in
            self?.subServices = subServices
            
            self?.collectionView.frame = CGRect(x: 0, y: 33, width: 343, height: 100)
            self?.collectionView.reloadData()
            
            self?.updateViews()
        }
    }
    
    private func setupCV() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "ServiceCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "serviceCell")
    }
    
    private func setupTextViews() {
        nameTF.textContentType = .name
        
        phoneTF.text = "+7"
        phoneTF.keyboardType = .phonePad
        phoneTF.textContentType = .telephoneNumber
        phoneTF.delegate = phoneInputController
        
        emailTF.textContentType = .emailAddress
        emailTF.keyboardType = .emailAddress
        
        commentTextView.text = "Комментарий"
        commentTextView.textColor = UIColor.lightGray
        commentTextView.delegate = self
        
        selectedServicesTextViewView.translatesAutoresizingMaskIntoConstraints = false
        supportView.translatesAutoresizingMaskIntoConstraints = false
        
        let data = State.currentPersonalData
        nameTF.text = data?.fullName
        phoneTF.text = data?.phoneNumber.first?.phone
        emailTF.text = data?.email
    }
    
    private func updateViews() {
        textViewsStackView.isHidden = selectedServices.isEmpty

        if let selectedTariffs = selectedTariffs {
            textViewsStackView.isHidden = selectedTariffs.isEmpty
        }

        selectedServicesTextView.text = "Выбранные услуги:"

        if let selectedTariffs = selectedTariffs {
            for selectedTariff in selectedTariffs {
                selectedServicesTextView.text.append(" \(selectedTariff)")
                if selectedTariff != selectedTariffs.last || !selectedServices.isEmpty {
                    selectedServicesTextView.text.append(",")
                }
            }
        }

        for selectedService in selectedServices {
            selectedServicesTextView.text.append(" \(selectedService.name)")
            if selectedService.name != selectedServices.last?.name { selectedServicesTextView.text.append(",") }
        }

        let numberOfLines = selectedServicesTextView.contentSize.height / selectedServicesTextView.font!.lineHeight
        let heightForTextView = CGFloat(22 * numberOfLines)
        selectedServicesTextViewHeight.constant = heightForTextView
        
    }
    
    @objc func supportTapped() {
        let phone = "+74733004000"
        if let url = URL(string: "tel://\(phone)"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
    }
    
    //MARK: – Storyboard Actions

    @IBAction func sendTapped(_ sender: Any) {
        guard let name = nameTF.text, name.containsText(),
              let phone = phoneTF.text,
              let email = emailTF.text else {
            ApiManager.shared.errorAlert(text: "Нужные поля не заполнены")
            return
        }
        
        guard phone.containsPhone() else {
            ApiManager.shared.errorAlert(text: "Поле номера телефона заполнено некорректно")
            return
        }
        
        guard email.containsEmail() else {
            ApiManager.shared.errorAlert(text: "Поле Email заполнено некорректно")
            return
        }
        
        ApiManager.shared.sendSupport(
            name: name,
            email: email,
            phone: phone,
            message: selectedServicesTextView.text + "\n <br>" + commentTextView.text,
            subject: .sales
        ) { (info) in
            ApiManager.shared.errorAlert(title: info, text: "") { [weak self] in
                self?.navigationController?.popViewController(animated: true)
            }
        }
        
    }
    
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}

//MARK: – Collection View Delegate & Data Source

extension ApplicationFormViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return subServices.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "serviceCell", for: indexPath) as!
            ServiceCollectionViewCell
        
        let service = subServices[indexPath.row]
        cell.configure(service: service)
        
        Nuke.loadImage(with: service.icon, into: cell.imageView) { [weak self] (_) in
            cell.deselect()
            
            if let selectedSubServices = self?.selectedSubServices {
                for subService in selectedSubServices {
                    if service.name == subService.name {
                        cell.select()
                    }
                }
            }
            
            if let selectedTariffs = self?.selectedTariffs {
                for tariff in selectedTariffs {
                    if service.name == tariff {
                        cell.select()
                        self?.selectedTariffs?.removeAll(where: { $0 == tariff })
                    }
                }
            }
        }
    
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! ServiceCollectionViewCell
        cell.isCellSelected ? cell.deselect() : cell.select()
        updateViews()
    }
    
}

//MARK: – Text View Delegate

extension ApplicationFormViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        switch textView.tag {
        case 0:
            if commentTextView.textColor == UIColor.lightGray {
                commentTextView.text = nil
                commentTextView.textColor = UIColor.black
            }
        case 1:
            if selectedServicesTextView.textColor == UIColor.lightGray {
                selectedServicesTextView.text = nil
                selectedServicesTextView.textColor = UIColor.black
            }
        default: break
        }
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        switch textView.tag {
        case 0:
            if commentTextView.text.isEmpty {
                commentTextView.text = "Комментарий"
                commentTextView.textColor = UIColor.lightGray
            }
        case 1:
            if selectedServicesTextView.text.isEmpty {
                commentTextView.text = "Выбранные услуги"
                commentTextView.textColor = UIColor.lightGray
            }
        default: break
        }
    }
}
