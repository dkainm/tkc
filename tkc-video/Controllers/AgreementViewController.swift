//
//  AgreementViewController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 14/9/21.
//

import UIKit

class AgreementViewController: UIViewController, AgreementTableViewCellDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var acceptButton: UIButton!
    
    var agreements = [Agreement]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        acceptButton.isEnabled = false
        acceptButton.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        
        fetchData()
    }
    
    private func fetchData() {
        ApiManager.shared.getAgreements { [weak self] (agreements) in
            self?.agreements = agreements
            self?.tableView.reloadData()
        }
    }
    
    @IBAction func acceptTapped(_ sender: Any) {
        if checkAgrs() {
            ApiManager.shared.confirmPolicy(agrs: agreements) { [weak self] in
                UserDefaults.standard.set(State.tokenToSave, forKey: "session")
                let vc = self?.storyboard!.instantiateViewController(withIdentifier: "mainTabBar") as! UITabBarController
                vc.modalPresentationStyle = .fullScreen
                self?.present(vc, animated: true)
            }
        }
    }
    
    private func checkAgrs() -> Bool {
        return tableView.visibleCells.allSatisfy { (cell) -> Bool in
            guard let cell = cell as? AgreementTableViewCell else { return false }
            return cell.agr.confirmed
        }
    }
    
    func linkTapped(link: String) {
        let webController = storyboard!.instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
        webController.url = URL(string: link)
        present(webController, animated: true)
    }
    
    func valueChanged(value: Bool) {
        if checkAgrs() {
            acceptButton.isEnabled = true
            acceptButton.backgroundColor = UIColor(named: "primary")
        } else {
            acceptButton.isEnabled = false
            acceptButton.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        }
    }
    
}
extension AgreementViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return agreements.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "agr", for: indexPath) as! AgreementTableViewCell
        cell.configure(agreement: agreements[indexPath.row])
        cell.delegate = self
        return cell
    }
    
}
