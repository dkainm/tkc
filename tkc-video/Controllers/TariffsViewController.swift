//
//  TariffsViewController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 28/12/21.
//

import UIKit

class TariffsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var tariffSections: [(String, UIImage)] = [("Интернет + ТВ", #imageLiteral(resourceName: "intNtv")), ("Интернет", #imageLiteral(resourceName: "int")), ("ТВ", #imageLiteral(resourceName: "tv")), ("Дополнительные услуги", #imageLiteral(resourceName: "sebServs"))]
    
    var internetTariffs = [Tariff]()
    var tvTariffs = [Tariff]()
    var internetTvTariffs = [Tariff]()
    var subServices = [SubService]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "TariffTableViewCell", bundle: nil), forCellReuseIdentifier: "tariffCell")
        fetchData()
    }
    
    func fetchData() {
        ApiManager.shared.getTariffsAndSubServices { [weak self] (internetTvTariffs, internetTariffs, tvTariffs, subServices) in
            self?.internetTvTariffs = internetTvTariffs
            self?.internetTariffs = internetTariffs
            self?.tvTariffs = tvTariffs
            self?.subServices = subServices
        }
    }

    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}

extension TariffsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tariffSections.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tariffCell", for: indexPath) as! TariffTableViewCell
        cell.selectionStyle = .none
        cell.configure(name: tariffSections[indexPath.row].0, image: tariffSections[indexPath.row].1)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let section = tariffSections[indexPath.row].0
        var tariffs = [Tariff]()
        switch section {
        case "Интернет + ТВ":
            tariffs = internetTvTariffs
        case "Интернет":
            tariffs = internetTariffs
        case "ТВ":
            tariffs = tvTariffs
        default: break
        }
        if section != "Дополнительные услуги" {
            let controller = storyboard?.instantiateViewController(withIdentifier: "TariffViewController") as! TariffViewController
            controller.tariffs = tariffs
            controller.sectionName = section
            navigationController?.pushViewController(controller, animated: true)
        } else {
            let controller = storyboard?.instantiateViewController(withIdentifier: "SubServicesViewController") as! SubServicesViewController
            controller.subServices = subServices
            navigationController?.pushViewController(controller, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
