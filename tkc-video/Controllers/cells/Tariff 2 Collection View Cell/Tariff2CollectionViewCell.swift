//
//  Tariff2CollectionViewCell.swift
//  tkc-video
//
//  Created by Alex Rudoi on 29/12/21.
//

import UIKit
import Nuke

class Tariff2CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    
    var tariff: TariffType! {
        didSet {
            nameLabel.text = tariff.name
            infoLabel.text = tariff.data
            Nuke.loadImage(with: tariff.icon, into: iconImageView)
        }
    }
    
    func configure(tariff: TariffType) {
        self.tariff = tariff
    }

}
