//
//  TariffCollectionViewCell.swift
//  tkc-video
//
//  Created by Alex Rudoi on 17/9/21.
//a

import UIKit
import Nuke

class TariffCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    
    var tariff: PersonalDataTariff! {
        didSet {
            nameLabel.text = tariff.service
            infoLabel.text = tariff.info
            Nuke.loadImage(with: tariff.icon, into: iconImageView)
        }
    }
    
    func configure(tariff: PersonalDataTariff) {
        self.tariff = tariff
    }

}
