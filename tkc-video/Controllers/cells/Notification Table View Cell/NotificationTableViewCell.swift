//
//  NotificationTableViewCell.swift
//  tkc-video
//
//  Created by Alex Rudoi on 25/2/22.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var moreButton: UIButton!
    
    var not: FBNotification!
    var parentTableView: UITableView!
    
    var isFull: Bool?
    
    override func prepareForReuse() {
        moreButton.isHidden = true
        if let isRead = not.isRead, isRead {
            mainView.alpha = 0.5
        }
    }
    
    func configure(_ notification: FBNotification, date: String, parent: UITableView) {
        self.not = notification
        self.parentTableView = parent
        
        titleLabel.text = notification.title
        descLabel.text = notification.body
        dateLabel.text = date
        
        if descLabel.isTruncated && descLabel.calculateContentHeight() > 43 {
            moreButton.isHidden = false
            isFull = false
        }
        
        if let isRead = not.isRead, isRead {
            mainView.alpha = 0.5
        }
    }
    
    @IBAction func moreTapped(_ sender: Any) {
        guard let isFull = isFull else { return }
        self.isFull = !isFull
        
        parentTableView.beginUpdates()
        if isFull {
            print("making cell small")
            descLabel.numberOfLines = 3
            moreButton.setTitle("Подробнее", for: .normal)
            if let isRead = not.isRead, isRead {
                mainView.alpha = 0.5
            }
        } else {
            print("making cell full")
            descLabel.numberOfLines = 0
            moreButton.setTitle("Скрыть", for: .normal)
            if let isRead = not.isRead, isRead {
                mainView.alpha = 1
            }
        }
        parentTableView.endUpdates()
        
    }
}
