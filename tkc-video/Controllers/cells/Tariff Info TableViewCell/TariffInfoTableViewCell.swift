//
//  TariffInfoTableViewCell.swift
//  tkc-video
//
//  Created by Alex Rudoi on 15/12/21.
//

import UIKit
import Nuke

class TariffInfoTableViewCell: UITableViewCell {

    @IBOutlet weak var boxView: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    
    @IBOutlet weak var priceLabel: UILabel!
        
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var akciLabel: UILabel!
    
    func configure(subInfo: PersonalDataServiceSubInfo, icon: URL) {
        titleLabel.text = subInfo.title
        subTitleLabel.text = subInfo.sn
        
        Nuke.loadImage(with: icon, into: iconImageView) { [weak self] _ in
            self?.iconImageView.image = self?.iconImageView.image!.withColor(.white)
        }
        
        priceLabel.text = "\(subInfo.price)"
        descLabel.text = subInfo.desc
        
        let startDate = getDate(from: subInfo.startDate)
        let endDate = getDate(from: subInfo.endDate)
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.mm.yyyy"
    
        akciLabel.text = "Действует с \(dateFormatter.string(from: startDate)) до \(dateFormatter.string(from: endDate))"
        
        setup()
    }
    
    fileprivate func setup() {
        clipsToBounds = false
        contentView.clipsToBounds = false
        
        backgroundColor = .clear
        contentView.backgroundColor = .clear
        
        boxView.layer.masksToBounds = false
        boxView.layer.shadowColor = UIColor.black.cgColor
        boxView.layer.shadowOpacity = 0.2
        boxView.layer.shadowOffset = CGSize(width: 0, height: 4)
        boxView.layer.shadowRadius = 15
    }
}
