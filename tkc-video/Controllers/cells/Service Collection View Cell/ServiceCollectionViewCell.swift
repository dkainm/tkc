//
//  ServiceCollectionViewCell.swift
//  tkc-video
//
//  Created by Alex Rudoi on 27/12/21.
//

import UIKit
import Nuke

class ServiceCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var boxView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    var service: PersonalDataService?
    var subService: SubService?
    
    var isCellSelected = false
    
    func configure(service: PersonalDataService) {
        self.service = service
        setup()
    }
    
    func configure(service: SubService) {
        self.subService = service
        setup()
    }
    
    func select() {
        isCellSelected = true
        boxView.layer.borderWidth = 1
        boxView.layer.borderColor = #colorLiteral(red: 0.8509803922, green: 0.2470588235, blue: 0.2549019608, alpha: 1)
        imageView.image = imageView.image?.tintPicto(#colorLiteral(red: 0.8509803922, green: 0.2470588235, blue: 0.2549019608, alpha: 1))
    }
    
    func deselect() {
        isCellSelected = false
        boxView.layer.borderWidth = 0
        imageView.image = imageView.image?.tintPicto(#colorLiteral(red: 0.7568627451, green: 0.7921568627, blue: 0.8392156863, alpha: 1))
    }
    
    fileprivate func setup() {
        clipsToBounds = false
        contentView.clipsToBounds = false
        
        backgroundColor = .clear
        contentView.backgroundColor = .clear
        
        if let service = service {
            titleLabel.text = service.service
            priceLabel.text = "\(service.subInfo.last?.price ?? 0) руб./мес."
        } else if let subservice = subService {
            titleLabel.text = subservice.name
            priceLabel.text = "\(subservice.price) руб./мес."
        }
        
        
        boxView.layer.masksToBounds = false
        boxView.layer.shadowColor = UIColor.black.cgColor
        boxView.layer.shadowOpacity = 0.2
        boxView.layer.shadowOffset = CGSize(width: 0, height: 4)
        boxView.layer.shadowRadius = 15
        
        contentView.backgroundColor = .clear
        
        if let service = service {
            Nuke.loadImage(with: service.icon, into: imageView)
        }
        
        if let subService = subService {
            Nuke.loadImage(with: subService.icon, into: imageView)
        }
    }

}
