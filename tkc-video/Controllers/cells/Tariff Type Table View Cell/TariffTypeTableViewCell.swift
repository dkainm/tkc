//
//  TariffTypeTableViewCell.swift
//  tkc-video
//
//  Created by Alex Rudoi on 29/12/21.
//

import UIKit
import AlignedCollectionViewFlowLayout

class TariffTypeTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var boxView: UIView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var cvHeightConstraint: NSLayoutConstraint!
    
    var tariff: Tariff! {
        didSet {
            nameLabel.text = tariff.name
            priceLabel.text = tariff.price
        }
    }
    
    var tariffTypes = [TariffType]()
    
    var isCellSelected = false
    
    func configure(tariff: Tariff) {
        self.tariff = tariff
        self.tariffTypes = tariff.info
        
        setupCV()
        setHeightToCV()
        
        setupShadows()
    }
    
    func select() {
        isCellSelected = true
        boxView.layer.borderWidth = 1
        boxView.layer.borderColor = #colorLiteral(red: 0.8509803922, green: 0.2470588235, blue: 0.2549019608, alpha: 1)
    }
    
    func deselect() {
        isCellSelected = false
        boxView.layer.borderWidth = 0
    }
    
    private func setupCV() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: "Tariff2CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "tarif2")
        
        let alignedFlowLayout = AlignedCollectionViewFlowLayout(
            horizontalAlignment: .left,
            verticalAlignment: .top
        )
        let width: CGFloat = {
            let cvWidth = (collectionView.bounds.width-8)/2
            return cvWidth
        }()
        alignedFlowLayout.estimatedItemSize = .init(width: width, height: 42)
        collectionView.collectionViewLayout = alignedFlowLayout
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        collectionView.reloadData()
    }
    
    private func setHeightToCV() {
        var height: CGFloat = 42
        if tariffTypes.count > 1 {
            
            if tariffTypes.count % 2 != 0 {
                height = CGFloat(52 * ((tariffTypes.count/2) + 1)) - 9
            } else {
                height = CGFloat(52 * (tariffTypes.count/2)) - 9
            }
            
        }
        cvHeightConstraint.constant = height
    }
    
    fileprivate func setupShadows() {
        clipsToBounds = false
        contentView.clipsToBounds = false
        
        backgroundColor = .clear
        contentView.backgroundColor = .clear
        
        boxView.layer.masksToBounds = false
        boxView.layer.shadowColor = UIColor.black.cgColor
        boxView.layer.shadowOpacity = 0.2
        boxView.layer.shadowOffset = CGSize(width: 0, height: 4)
        boxView.layer.shadowRadius = 15
    }
}

extension TariffTypeTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tariffTypes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tarif2", for: indexPath) as! Tariff2CollectionViewCell
        cell.configure(tariff: tariffTypes[indexPath.row])
        return cell
    }

}
