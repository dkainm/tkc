//
//  TariffTableViewCell.swift
//  tkc-video
//
//  Created by Alex Rudoi on 28/12/21.
//

import UIKit

class TariffTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var boxView: UIView!
    
    func configure(name: String, image: UIImage) {
        nameLabel.text = name
        iconImageView.image = image
        
        setup()
    }
    
    fileprivate func setup() {
        clipsToBounds = false
        contentView.clipsToBounds = false
        
        backgroundColor = .clear
        contentView.backgroundColor = .clear
        
        boxView.layer.masksToBounds = false
        boxView.layer.shadowColor = UIColor.black.cgColor
        boxView.layer.shadowOpacity = 0.1
        boxView.layer.shadowOffset = CGSize(width: 0, height: 4)
        boxView.layer.shadowRadius = 10
    }
}
