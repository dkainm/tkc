//
//  SubServiceTableViewCell.swift
//  tkc-video
//
//  Created by Alex Rudoi on 29/12/21.
//

import UIKit
import Nuke

class SubServiceTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var descLabel: UILabel!
    
    @IBOutlet weak var boxView: UIView!

    var subService: SubService! {
        didSet {
            nameLabel.text = subService.name
            priceLabel.text = subService.price
            Nuke.loadImage(with: subService.icon, into: iconImageView)
            descLabel.text = subService.desc
        }
    }
    
    var isCellSelected = false
    
    func configure(subService: SubService) {
        self.subService = subService
        setupShadows()
    }
    
    func select() {
        isCellSelected = true
        boxView.layer.borderWidth = 1
        boxView.layer.borderColor = #colorLiteral(red: 0.8509803922, green: 0.2470588235, blue: 0.2549019608, alpha: 1)
    }
    
    func deselect() {
        isCellSelected = false
        boxView.layer.borderWidth = 0
    }
    
    fileprivate func setupShadows() {
        clipsToBounds = false
        contentView.clipsToBounds = false
        
        backgroundColor = .clear
        contentView.backgroundColor = .clear
        
        boxView.layer.masksToBounds = false
        boxView.layer.shadowColor = UIColor.black.cgColor
        boxView.layer.shadowOpacity = 0.2
        boxView.layer.shadowOffset = CGSize(width: 0, height: 4)
        boxView.layer.shadowRadius = 15
    }
}
