//
//  PersonalOfferTableViewCell.swift
//  tkc-video
//
//  Created by Alex Rudoi on 20/12/21.
//

import UIKit

class PersonalOfferTableViewCell: UITableViewCell {

    @IBOutlet weak var boxView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    
    func configure(offer: PersonalOffer) {
        nameLabel.text = offer.title
        descLabel.text = offer.previewDesc
        
        setup()
    }
    
    fileprivate func setup() {
        clipsToBounds = false
        contentView.clipsToBounds = false
        
        backgroundColor = .clear
        contentView.backgroundColor = .clear
        
        boxView.layer.masksToBounds = false
        boxView.layer.shadowColor = UIColor.black.cgColor
        boxView.layer.shadowOpacity = 0.2
        boxView.layer.shadowOffset = CGSize(width: 0, height: 4)
        boxView.layer.shadowRadius = 15
    }
}
