//
//  CategoriesViewController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 24/8/21.
//

import UIKit
import Nuke
import SVProgressHUD

class CategoriesViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var favoritesChannels = [Chanell]()
    var allChannels = [Chanell]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.register(UINib(nibName: "CategoryCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "category")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchData()
    }
    
    @IBAction func backTapped(_ sender: Any) {
        //dissmis
    }
    
    func fetchData() {
        SVProgressHUD.show()
        
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        tabBarController?.view.addSubview(blurEffectView)
        
        ApiManager.shared.updateCategories { [weak self] categories in
            TvCategory.categories = categories
            
            ApiManager.shared.updateChanells(categoryId: 0) { (channels) in
                self?.allChannels = channels
                
                DatabaseManager.shared.getAllChannels { (channels) in
                    guard let channels = channels else {
                        self?.collectionView.reloadData()
                        return
                    }
                    
                    self?.favoritesChannels = channels
                    self?.collectionView.reloadData()
                    
                    blurEffectView.removeFromSuperview()
                    SVProgressHUD.dismiss()
                }
            }
        }
    }
    
}

extension CategoriesViewController:
    UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return TvCategory.categories.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "category", for: indexPath) as! CategoryCollectionViewCell
        
        if indexPath.row != 0 {
            cell.configure(category: TvCategory.categories[indexPath.row - 1])
            Nuke.loadImage(with: TvCategory.categories[indexPath.row - 1].icon, into: cell.categoryImageView)
        } else {
            cell.configure(favorites: favoritesChannels)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row != 0 {
            let category = TvCategory.categories[indexPath.row - 1]
            let chanellsController = storyboard!.instantiateViewController(withIdentifier: "ChanellsViewController") as! ChanellsViewController
            chanellsController.category = category
            chanellsController.allChannels = allChannels
            navigationController?.pushViewController(chanellsController, animated: true)
        } else {
            let chanellsController = storyboard!.instantiateViewController(withIdentifier: "ChanellsViewController") as! ChanellsViewController
            chanellsController.channels = favoritesChannels
            chanellsController.allChannels = allChannels
            navigationController?.pushViewController(chanellsController, animated: true)
        }
        
    }
    
}
