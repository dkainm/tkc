//
//  ChanellViewController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 28/8/21.
//

import UIKit
import SVProgressHUD
import DynamicMobileVLCKit
import MediaPlayer

public enum PlayerChange {
    case toOnline
    case toArchive
}

class ChanellViewController: UIViewController {

    //MARK: – Storyboard Variables
    
    @IBOutlet weak var fullScreenButtonRightConstraint: NSLayoutConstraint!
    @IBOutlet weak var soundButtonRightContstraing: NSLayoutConstraint!
    
    @IBOutlet weak var tvView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var programLabel: UILabel! {
        didSet {
            programLabel.backgroundColor = .clear
        }
    }
    @IBOutlet weak var genreLabel: UILabel! {
        didSet {
            genreLabel.backgroundColor = .clear
        }
    }
    @IBOutlet weak var descLabel: UILabel! {
        didSet {
            descLabel.backgroundColor = .clear
        }
    }
    @IBOutlet weak var datesCollectionView: UICollectionView!
    @IBOutlet weak var showMoreButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var playerInstrumentsView: UIView!
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var fullSreenButtonView: UIView!
    @IBOutlet weak var fullScreenButton: UIButton!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var allTimeLabel: UILabel!
    @IBOutlet weak var timeSlider: UISlider!
    @IBOutlet weak var soundButtonView: UIView!
    @IBOutlet weak var soundButton: UIButton!
    @IBOutlet weak var goForwardButton: UIButton!
    @IBOutlet weak var goBackwardButton: UIButton!
    @IBOutlet weak var changeButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleBackView: UIView!
    @IBOutlet weak var favoritesButton: UIButton!
    
    @IBOutlet weak var infoLabel: UILabel!
    
    //MARK: – Variables
    
    
    var portraitConstraints: [NSLayoutConstraint]?
    var landscapeConstraints: [NSLayoutConstraint]?
    
    var chanell: Chanell!
    
    var dates = [Date]()
    var tlpms: [String: [Teleprogram]] = [:] {
        didSet {
            guard !tableView.visibleCells.isEmpty else { return }
            setActualRow()
        }
    }
    
    var foundBySearch = false
    
    var showsMoreText = false
    
    var selectedTeleprogramIndex: Int?
    var selectedDateIndex: Int?
    
    var selectedDate: Date?
    
    var actualTeleprogramCell:
        TeleprogramTableViewCell?
    
    var isSeeking = false
    var isMuted = false
    
    var time: VLCTime?
    var justDismissed = false
    
    var isOnline = true
    
    var isFavorite = false
    
    var fullScreened = false
    
    var changeState: PlayerChange? {
        didSet {
            switch changeState {
            case .toArchive:
                changeButton.setTitle("В начало", for: .normal)
            case .toOnline:
                changeButton.setTitle("Онлайн", for: .normal)
            default: break
            }
        }
    }
    
    var info: String? {
        didSet {
            guard let info = info else {
                infoLabel.isHidden = true
                return
            }
            
            infoLabel.text = info
            
            mediaPlayer.stop()
            setInstrumentsHidden(true)
            infoLabel.isHidden = false
        }
    }
    
    var mediaPlayer = VLCMediaPlayer()
    
    var teleprograms = [Teleprogram]()
    
    var actualTeleprogram: Teleprogram?
    
    var onlineUrl: URL? {
        return URL(string: "\(chanell.server)/\(chanell.codeName)/index.m3u8?token=\(chanell.token)")
    }
    var archiveUrl: URL? {
        guard let actualTeleprogram = actualTeleprogram else { return nil }
        return URL(string: "\(chanell.server)/\(chanell.codeName)/archive-\(actualTeleprogram.startDate)-\(Int(actualTeleprogram.stopDate)!-Int(actualTeleprogram.startDate)!).m3u8?token=\(chanell.token)")
    }
    
    var mUrl: URL?
    
    var duration: Int32?
    
    var isArchive = false
    
    var systemVolume: Float?
    
    var isInstrumentsHidden: Bool {
        return soundButtonView.isHidden
    }
    
    var timer: Timer?
    var timerCounter = 0
    
    //MARK: – Life Circle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = chanell.name
        
        timeSlider.layer.shadowColor = UIColor.black.cgColor
        timeSlider.layer.shadowOpacity = 0.4
        timeSlider.layer.shadowOffset = CGSize(width: 0, height: 2)
        timeSlider.layer.shadowRadius = 15
        
        timeLabel.layer.shadowColor = UIColor.black.cgColor
        timeLabel.layer.shadowOpacity = 0.4
        timeLabel.layer.shadowOffset = CGSize(width: 0, height: 2)
        timeLabel.layer.shadowRadius = 15
        
        tableView.clipsToBounds = true
        tableView.layer.masksToBounds = true
        
        setInstrumentsHidden(true)
        
        setupConstraints()
        
        playerInstrumentsView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(instrumentsTapped)))
        
        datesCollectionView.register(UINib(nibName: "DateCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "date")
        tableView.register(UINib(nibName: "TeleprogramTableViewCell", bundle: nil), forCellReuseIdentifier: "teleprogram")
        
        AppDelegate.orientationLock = UIInterfaceOrientationMask.allButUpsideDown
        UINavigationController.attemptRotationToDeviceOrientation()
        NotificationCenter.default.addObserver(self, selector: #selector(rotated), name: UIDevice.orientationDidChangeNotification, object: nil)
        
        channelIsFavorite { [weak self] (isFavorite) in
            if isFavorite {
                self?.favoritesButton.setImage(UIImage(named: "star"), for: .normal)
            } else {
                self?.favoritesButton.setImage(UIImage(named: "starOutlined"), for: .normal)
            }
        }
        
        fetchData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.isNavigationBarHidden = true
        setupMediaPlayer()
        playOnlineUrl()
        
        if let index = selectedDateIndex {
            datesCollectionView.cellForItem(at: IndexPath(item: index, section: 0))?.isSelected = true
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        mediaPlayer.stop()
        mediaPlayer.drawable = UIView()
        navigationController?.isNavigationBarHidden = !foundBySearch
    }
    
    override func viewDidLayoutSubviews() {
        changeConstraints()
    }
    
    override func viewWillLayoutSubviews() {
        if !fullScreened {
            NSLayoutConstraint.deactivate(landscapeConstraints!)
            NSLayoutConstraint.activate(portraitConstraints!)
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        switch keyPath {
        case "time":
            if duration == nil {
                duration = (-1 * mediaPlayer.remainingTime.intValue) + mediaPlayer.time.intValue
            }
            
            guard isArchive, !isSeeking,
                  let time = mediaPlayer.time,
                  let _ = duration else { return }
            
            timeLabel.text = time.stringValue
            allTimeLabel.text = VLCTime(int: duration!)?.stringValue
            
            timeSlider.setValue(mediaPlayer.position, animated: false)
            
        case "isPlaying":
            guard mediaPlayer.isPlaying, justDismissed, isArchive,
                  let time = time else { return }
            mediaPlayer.time = time
            justDismissed = false
        default: break
        }
    }
    
    //MARK: – Private Methods
    
    private func setupMediaPlayer() {
        mediaPlayer.delegate = self
        mediaPlayer.drawable = tvView
        timeSlider.addTarget(self, action: #selector(onSliderValChanged(slider:event:)), for: .valueChanged)
    }
    
    private func setupConstraints() {
        let superview = view.layoutMarginsGuide
        
        portraitConstraints = [
            tvView.topAnchor.constraint(equalTo: backButton.bottomAnchor, constant: 14),
            titleBackView.topAnchor.constraint(equalTo: tvView.bottomAnchor)
        ]
        
        landscapeConstraints = [
            tvView.topAnchor.constraint(equalTo: superview.topAnchor),
            tvView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ]
    }
    
    private func channelIsFavorite(completion: @escaping (Bool) -> ()) {
        DatabaseManager.shared.getAllChannels { [weak self] (channels) in
            guard let channels = channels else {
                self?.isFavorite = false
                completion(false)
                return
            }
            if let _ = channels.first(where: {$0.id == self?.chanell.id}) {
                self?.isFavorite = true
                completion(true)
            } else {
                self?.isFavorite = false
                completion(false)
            }
        }
    }
    
    private func favoriteChanged() {
        if isFavorite {
            favoritesButton.setImage(UIImage(named: "star"), for: .normal)
        } else {
            favoritesButton.setImage(UIImage(named: "starOutlined"), for: .normal)
        }
    }
    
    private func changeConstraints(toLandscape: Bool? = false) {
        var isPortrait = UIDevice.current.orientation.isPortrait
        
        guard UIDevice.current.orientation != .portraitUpsideDown else {
            return
        }
        
        if UIDevice.current.orientation.isFlat { isPortrait = true }
        
        switch UIDevice.current.orientation.rawValue {
        case 1:
            isPortrait = true
        case 2:
            isPortrait = true
        case 3:
            isPortrait = false
        case 4:
            isPortrait = false
        case 5:
            isPortrait = true
        case 6:
            isPortrait = true
        default:
            isPortrait = true
        }
        
        if isPortrait {
            print("portrait")
            NSLayoutConstraint.deactivate(landscapeConstraints!)
            NSLayoutConstraint.activate(portraitConstraints!)
            fullScreenButtonRightConstraint.constant = 8
            soundButtonRightContstraing.constant = 8
        } else {
            print("landscape")
            NSLayoutConstraint.deactivate(portraitConstraints!)
            NSLayoutConstraint.activate(landscapeConstraints!)
            fullScreenButtonRightConstraint.constant = 16
            soundButtonRightContstraing.constant = 16
        }
        
        titleBackView.isHidden = !isPortrait
        tabBarController?.tabBar.isHidden = !isPortrait
    }
    
    @objc private func onSliderValChanged(slider: UISlider, event: UIEvent) {
        if let touchEvent = event.allTouches?.first {
            switch touchEvent.phase {
            case .began:
                isSeeking = true
            case .ended:
                if duration == nil {
                    duration = (-1 * mediaPlayer.remainingTime.intValue) + mediaPlayer.time.intValue
                }

                let time = Int32(Float(duration!) * timeSlider.value)
                mediaPlayer.time = VLCTime(int: time)
                
                isSeeking = false
                
            default: break
            }
        }
    }
    
    private func openPlayer(with url: URL) {
        mediaPlayer.stop()
        mediaPlayer.drawable = UIView()
        
        let player = storyboard!.instantiateViewController(withIdentifier: "TvPlayerViewController") as! TvPlayerViewController

        player.onlineUrl = onlineUrl
        player.mediaUrl = url
        player.chanell = chanell
        player.actualTeleprogram = actualTeleprogram
        player.isArchive = isArchive
        player.isOnline = isOnline
        player.rotation = UIDevice.current.orientation

        if isArchive {
            player.time = mediaPlayer.time
        }

        player.delegate = self
        player.modalPresentationStyle = .overCurrentContext
        player.modalTransitionStyle = .coverVertical

        present(player, animated: true)
    }
    
    @objc private func instrumentsTapped() {
        setInstrumentsHidden(!isInstrumentsHidden)
    }
    
    private func mediaTapped() {
        if playPauseButton.image(for: .normal) == UIImage(named: "Pause") {
            mediaPlayer.pause()
            playerInstrumentsView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.6)
            playPauseButton.setImage(UIImage(named: "Play"), for: .normal)
        } else {
            mediaPlayer.play()
            playerInstrumentsView.backgroundColor = .clear
            playPauseButton.setImage(UIImage(named: "Pause"), for: .normal)
        }
    }
    
    fileprivate func setActualRow() {
        let timeIntervalNow = TimeService.shared.getUnixFromDate(Date())
        
        let day = Calendar.current.component(.day, from: Date())
        
        if let index = tlpms["\(day)"]!.firstIndex(where: {
            TimeInterval($0.startDate)! < timeIntervalNow &&
            TimeInterval($0.stopDate)! > timeIntervalNow
        }) {
            let tlpm = tlpms["\(day)"]![index]
            actualTeleprogram = tlpm
            selectedDate = Date()
            tableView.selectRow(at: IndexPath(row: index, section: 0), animated: true, scrollPosition: .middle)
            
            print("1selected: ", tlpm.name)
        }
        
        if let teleprogram = actualTeleprogram {
            programLabel.text = "\(teleprogram.name) [\(teleprogram.pg)+]"
            genreLabel.text = "\(teleprogram.category) / \(teleprogram.genre)"
            descLabel.text = teleprogram.desc
        }
    }
    
    fileprivate func setInstrumentsHidden(_ isHidden: Bool) {
        
        UIView.animate(withDuration: 0.4, delay: 0, options: .transitionCrossDissolve) {
            self.fullSreenButtonView.isHidden = isHidden
            self.soundButtonView.isHidden = isHidden
            
            if self.isArchive {
                self.playPauseButton.isHidden = isHidden
            }
            
        }
    }
    
    private func fetchData() {
        
        SVProgressHUD.show()
        
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        tabBarController?.view.addSubview(blurEffectView)
        
        ApiManager.shared.getTeleprogram(chanellId: chanell.idChanell) { [weak self] (teleprograms) in
            self?.teleprograms = teleprograms
            self?.filterTeleprograms()
            
            DispatchQueue.main.async {
                self?.tableView.reloadData()
                
                self?.datesCollectionView.scrollToItem(at: IndexPath(item: self?.selectedDateIndex ?? 0, section: 0), at: .centeredHorizontally, animated: true)
                
                self?.setActualRow()
                
                if let teleprogramIndex = self?.selectedTeleprogramIndex {
                    self?.tableView.selectRow(at: IndexPath(row: teleprogramIndex, section: 0), animated: true, scrollPosition: .middle)
                    print("selected: ", self?.selectedTeleprogramIndex)
                }
                
                blurEffectView.removeFromSuperview()
                SVProgressHUD.dismiss()
            }
        }
    }
    
    private func filterTeleprograms() {
        
        let tls = Dictionary(grouping: teleprograms) { $0.day }
        tlpms = tls
        
        let now = Date().timeIntervalSince1970
        dates = [Date(timeIntervalSince1970: TimeInterval(now - (3 * 86400))),
                 Date(timeIntervalSince1970: TimeInterval(now - (2 * 86400))),
                 Date(timeIntervalSince1970: TimeInterval(now - 86400)),
                 Date(),
                 Date(timeIntervalSince1970: TimeInterval(now + 86400)),
                 Date(timeIntervalSince1970: TimeInterval(now + (2 * 86400))),
                 Date(timeIntervalSince1970: TimeInterval(now + (3 * 86400))),
                 Date(timeIntervalSince1970: TimeInterval(now + (4 * 86400)))]
        
        datesCollectionView.reloadData()
    }
    
    private func playOnlineUrl() {
        guard let onlineUrl = onlineUrl else { return }
        playMedia(url: onlineUrl.absoluteString)
    }
    
    private func playArchiveUrl() {
        guard let archiveUrl = archiveUrl else { return }
        playMedia(url: archiveUrl.absoluteString)
    }
    
    private func playArchiveUrl(from teleprogram: Teleprogram) {
        guard let archiveUrl = URL(string: "\(chanell.server)/\(chanell.codeName)/archive-\(teleprogram.startDate)-\(Int(teleprogram.stopDate)!-Int(teleprogram.startDate)!).m3u8?token=\(chanell.token)") else { return }
        playMedia(url: archiveUrl.absoluteString)
    }
    
    func playMedia(url: String) {
        guard let mediaUrl = URL(string: url) else { return }
        
        if mediaUrl.absoluteString.contains("archive") { isArchive = true }

        mediaPlayer.stop()
        mediaPlayer.drawable = tvView
        mediaPlayer.media = VLCMedia(url: mediaUrl)
        mUrl = mediaUrl
        duration = nil
        mediaPlayer.play()
        mediaPlayer.time = VLCTime(int: 0)
    }
    
    //MARK: – Storyboard Methods
    
    @IBAction func playPauseTapped(_ sender: Any) {
        mediaTapped()
    }
    
    @IBAction func soundTapped(_ sender: UIButton) {
        soundButton.setImage(UIImage(named: isMuted ? "sound" : "noSound"), for: .normal)
        mediaPlayer.audio.isMuted = !isMuted
        isMuted = !isMuted
    }
    
    @IBAction func skipButton(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            mediaPlayer.jumpBackward(15)
        case 1:
            mediaPlayer.jumpForward(15)
        default: break
        }
    }
    
    @IBAction func fullScreenTapped(_ sender: Any) {
        fullScreened = !fullScreened
        let orientation = fullScreened ? UIInterfaceOrientation.landscapeRight : .portrait
        UIDevice.current.setValue(orientation.rawValue, forKey: "orientation")
    }
    
    @IBAction func showMoreTapped(_ sender: Any) {
        if showsMoreText {
            UIView.performWithoutAnimation {
                self.showMoreButton.setTitle("Показать больше", for: .normal)
                self.showMoreButton.layoutIfNeeded()
            }
            descLabel.numberOfLines = 2
        } else {
            UIView.performWithoutAnimation {
                self.showMoreButton.setTitle("Скрыть", for: .normal)
                self.showMoreButton.layoutIfNeeded()
            }
            descLabel.numberOfLines = 0
        }
        showsMoreText = !showsMoreText
    }
    
    @IBAction func changeTapped(_ sender: UIButton) {
        setInstrumentsHidden(true)
        
        if isArchive {
            
            // Переход в онлайн
            playOnlineUrl()
            changeState = .toArchive
            
            isArchive = false
        } else {
            
            // Переход в архив
            playArchiveUrl()
            changeState = .toOnline
            
            isArchive = true
        }
        
        guard let teleprogramIndex = selectedTeleprogramIndex else { return }
        tableView.selectRow(at: IndexPath(row: teleprogramIndex, section: 0), animated: true, scrollPosition: .middle)
        print("2selected: ", teleprogramIndex)
        
    }
    
    @IBAction func backTapped(_ sender: Any) {
        SVProgressHUD.show()
        mediaPlayer.stop()
        mediaPlayer.drawable = UIView()
        navigationController?.popViewController(animated: true)
        SVProgressHUD.dismiss()
    }
    
    @IBAction func favoritesTapped(_ sender: UIButton) {
        isFavorite = !isFavorite
        favoriteChanged()
        channelIsFavorite { [weak self] (isFavorite) in
            guard let `self` = self else { return }
            if isFavorite {
                DatabaseManager.shared.deleteChannel(self.chanell)
            } else {
                DatabaseManager.shared.saveChannel(self.chanell)
            }
        }
    }
}

extension ChanellViewController: VLCMediaPlayerDelegate {
    
    func mediaPlayerStateChanged(_ aNotification: Notification!) {
        print("state changed: ", mediaPlayer.isPlaying)
    }
    
}

//MARK: – TvPlayerDelegate

extension ChanellViewController: TvPlayerDelegate {
    
    func dismissed(mediaUrl: String, time: VLCTime?) {
        playMedia(url: mediaUrl)
        self.time = time
        justDismissed = true
    }
    
}

//MARK: – Table View Delegate & Data Source

extension ChanellViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let date = selectedDate ?? Date()
        let day = Calendar.current.component(.day, from: date)
        guard let tlpms = tlpms["\(day)"] else {
            return 0
        }
        return tlpms.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "teleprogram", for: indexPath) as! TeleprogramTableViewCell
        
        let date = selectedDate ?? Date()
        let day = Calendar.current.component(.day, from: date)
        let tlpm = tlpms["\(day)"]![indexPath.row]
        
        cell.selectionStyle = .none
        
        if let actualTeleprogram = actualTeleprogram,
           tlpm.startDate == actualTeleprogram.startDate {
            cell.isSelected = true
            actualTeleprogramCell = cell
            selectedTeleprogramIndex = indexPath.row
        }
        
        cell.configure(teleprogram: tlpm)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! TeleprogramTableViewCell
        let date = selectedDate ?? Date()
        let day = Calendar.current.component(.day, from: date)
        let tlpm = tlpms["\(day)"]![indexPath.row]
        
        guard (Int(tlpm.startDate)! - Int(Date().timeIntervalSince1970)) <= 0 else {
            info = "Эта программа еще не началась"
            return
        }
        info = nil
        
        if cell != actualTeleprogramCell {
            actualTeleprogramCell?.isSelected = false
            isOnline = false
            changeState = .toArchive
        } else {
            isOnline = true
            changeState = .toOnline
        }
        cell.isSelected = true
        print("cell selected")
        
        programLabel.text = "\(tlpm.name) [\(tlpm.pg)+]"
        genreLabel.text = "\(tlpm.category) / \(tlpm.genre)"
        descLabel.text = tlpm.desc
        
        playArchiveUrl(from: tlpm)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.isSelected = false
    }
    
}

//MARK: – Collection View Delegate & Data Source

extension ChanellViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dates.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "date", for: indexPath) as! DateCollectionViewCell
        
        let date = dates[indexPath.row]
        
        if Calendar.current.component(.day, from: date) == Calendar.current.component(.day, from: Date()) {
            cell.isSelected = true
            selectedDateIndex = indexPath.item
        }
        
        cell.backView.frame.size.height = 44
        cell.configure(date: date)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.item != 3 {
            collectionView.cellForItem(at: IndexPath(item: 3, section: 0))?.isSelected = false
        }
        
        let date = dates[indexPath.row]
        selectedDate = date
        tableView.reloadData()
    }
    
}
