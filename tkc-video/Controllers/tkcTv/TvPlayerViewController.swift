//
//  TvPlayerViewController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 10/11/21.
//

import UIKit
import DynamicMobileVLCKit
import MediaPlayer

protocol TvPlayerDelegate {
    func dismissed(mediaUrl: String, time: VLCTime?)
}

class TvPlayerViewController: UIViewController {

    @IBOutlet weak var backIcon: UIButton!
    @IBOutlet weak var closeButtonView: UIView!
    @IBOutlet weak var instrumentalsView: UIView!
    
    @IBOutlet weak var playPauseButton: UIButton!
    @IBOutlet weak var timeSlider: UISlider!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var allTimeLabel: UILabel!
    @IBOutlet weak var goForwardButton: UIButton!
    @IBOutlet weak var goBackwardButton: UIButton!
    @IBOutlet weak var changeButton: UIButton!
    
    @IBOutlet weak var soundButtonView: UIView!
    @IBOutlet weak var soundButton: UIButton!
    
    @IBOutlet weak var tvView: UIView!
    
    var chanell: Chanell!
    
    var delegate: TvPlayerDelegate?
    
    var mediaPlayer = VLCMediaPlayer()
    
    var onlineUrl: URL?
    var mediaUrl: URL!
    
    var isArchive = false
    var isSeeking = false
    var isMuted = false
    
    var actualTeleprogram: Teleprogram?
    
    var isOnline = true {
        didSet {
            guard let _ = changeButton else { return }
        }
    }
    
    var time: VLCTime?
    
    var duration: Int32? {
        didSet {
            guard let _ = duration else { return }
            allTimeLabel.text = VLCTime(int: duration!).stringValue
        }
    }
    
    var rotation: UIDeviceOrientation?
    
    var timer: Timer?
    var timerCounter = 0
    
    var systemVolume: Float?
    
    var changeState: PlayerChange? {
        didSet {
            switch changeState {
            case .toArchive:
                changeButton.setTitle("В начало", for: .normal)
            case .toOnline:
                changeButton.setTitle("Онлайн", for: .normal)
            default: break
            }
        }
    }
    
    var isInstrumentsHidden: Bool {
        return soundButtonView.isHidden
    }
    
    override var prefersHomeIndicatorAutoHidden: Bool {
        return true
    }
    
    enum Rotated {
        case landscape
        case portrait
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        setupMediaPLayer()
        playMedia(url: mediaUrl)
        
        setInstrumentsHidden(true)
        
        if let duration = duration {
            allTimeLabel.text = VLCTime(int: duration).stringValue
        }
        
        rotated()
        AppDelegate.orientationLock = UIInterfaceOrientationMask.landscape
        NotificationCenter.default.addObserver(self, selector: #selector(rotated), name: UIDevice.orientationDidChangeNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppDelegate.orientationLock = UIInterfaceOrientationMask.landscape
        UINavigationController.attemptRotationToDeviceOrientation()
        UIDevice.current.setValue(UIDeviceOrientation.landscapeLeft.rawValue, forKey: "orientation")
        presentingViewController?.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        AppDelegate.orientationLock = UIInterfaceOrientationMask.portrait
        UINavigationController.attemptRotationToDeviceOrientation()
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        presentingViewController?.tabBarController?.tabBar.isHidden = false
        
        mediaPlayer.stop()
        mediaPlayer.drawable = UIView()
        mediaPlayer.removeObserver(self, forKeyPath: "time")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        guard isArchive, let time = time else { return }
        mediaPlayer.time = time
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        switch keyPath {
        case "time":
            
            if duration == nil {
                duration = (-1 * mediaPlayer.remainingTime.intValue) + mediaPlayer.time.intValue
            }
            
            guard isArchive, !isSeeking,
                  let time = mediaPlayer.time,
                  let duration = duration else { return }
            
            timeLabel.text = time.stringValue
            allTimeLabel.text = VLCTime(int: duration)?.stringValue
            
            timeSlider.setValue(mediaPlayer.position, animated: false)
        default: break
        }
    }
    
    private func setup() {
        
        // UI
        
        backIcon.setImage(#imageLiteral(resourceName: "back").tintPicto(.white), for: .normal)
        
        timeSlider.layer.shadowColor = UIColor.black.cgColor
        timeSlider.layer.shadowOpacity = 0.4
        timeSlider.layer.shadowOffset = CGSize(width: 0, height: 2)
        timeSlider.layer.shadowRadius = 15
        
        timeLabel.layer.shadowColor = UIColor.black.cgColor
        timeLabel.layer.shadowOpacity = 0.4
        timeLabel.layer.shadowOffset = CGSize(width: 0, height: 2)
        timeLabel.layer.shadowRadius = 15
        
        // Gestures
        
        instrumentalsView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(instrumentsTapped)))
        
        let slideDown = UISwipeGestureRecognizer(target: self, action: #selector(dismissView(gesture:)))
        slideDown.direction = .down
        
        let slideUp = UISwipeGestureRecognizer(target: self, action: #selector(dismissView(gesture:)))
        slideUp.direction = .up
        
        view.addGestureRecognizer(slideDown)
        view.addGestureRecognizer(slideUp)
    }
    
    private func setupMediaPLayer() {
        mediaPlayer.delegate = self
        mediaPlayer.drawable = tvView
        mediaPlayer.addObserver(self, forKeyPath: "time", options: .new, context: nil)
        timeSlider.addTarget(self, action: #selector(onSliderValChanged(slider:event:)), for: .valueChanged)
    }
    
    @objc func onSliderValChanged(slider: UISlider, event: UIEvent) {
        if let touchEvent = event.allTouches?.first {
            switch touchEvent.phase {
            case .began:
                isSeeking = true
            case .ended:
                if duration == nil {
                    duration = (-1 * mediaPlayer.remainingTime.intValue) + mediaPlayer.time.intValue
                }

                let time = Int32(Float(duration!) * timeSlider.value)

                mediaPlayer.time = VLCTime(int: time)
                
                isSeeking = false
            default: break
            }
        }
    }
    
    private func playMedia(url: URL) {
        mediaPlayer.media = VLCMedia(url: url)
        duration = nil
        mediaPlayer.time = VLCTime(int: 0)
        mediaPlayer.play()
    }
    
    private func mediaTapped() {
        if mediaPlayer.isPlaying {
            mediaPlayer.pause()
            instrumentalsView.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.6)
            playPauseButton.setImage(UIImage(named: "Play"), for: .normal)
        } else {
            mediaPlayer.play()
            instrumentalsView.backgroundColor = .clear
            playPauseButton.setImage(UIImage(named: "Pause"), for: .normal)
        }
    }
    
    private func screenRotated(rotatedTo rotation: Rotated) {
        if rotation == .portrait {
            dismiss(animated: true)
        }
    }
    
    private func dismiss(animated: Bool) {
        guard mediaPlayer.isPlaying else { return }
        mediaPlayer.stop()
        dismiss(animated: animated) { [weak self] in
            self?.delegate?.dismissed(mediaUrl: (self?.mediaUrl.absoluteString)!, time: self?.mediaPlayer.time)
        }
    }
    
    @objc private func rotated() {
        
        switch UIDevice.current.orientation {
        case .portrait:
            screenRotated(rotatedTo: .portrait)
        case .landscapeLeft:
            screenRotated(rotatedTo: .landscape)
        case .landscapeRight:
            screenRotated(rotatedTo: .landscape)
        default: break
        }
        
    }
    
    @objc private func instrumentsTapped() {
        setInstrumentsHidden(!isInstrumentsHidden)
    }
    
    @objc private func timerTick() {
        timerCounter += 1
        print("timer tick: ", timerCounter)
        
        if timerCounter >= 4 {
            timerCounter = 0
            setInstrumentsHidden(true)
            isSeeking = false
        }
    }
    
    @objc private func dismissView(gesture: UISwipeGestureRecognizer) {
        UIView.animate(withDuration: 0.2) {
            if let theWindow = UIApplication.shared.keyWindow {
                switch gesture.direction {
                case .down:
                    gesture.view?.frame = CGRect(x: theWindow.frame.origin.x, y: theWindow.frame.size.height, width: theWindow.frame.size.width, height: theWindow.frame.size.height)
                case .up:
                    gesture.view?.frame = CGRect(x: theWindow.frame.origin.x, y: -theWindow.frame.size.height, width: theWindow.frame.size.width, height: theWindow.frame.size.height)
                default: break
                }
            }
        } completion: { (_) in
            self.dismiss(animated: false)
        }
    }
    
    @IBAction func playPauseTapped(_ sender: UIButton) {
        mediaTapped()
    }
    
    @IBAction func soundTapped(_ sender: UIButton) {
        soundButton.setImage(UIImage(named: isMuted ? "sound" : "noSound"), for: .normal)
        mediaPlayer.audio.isMuted = !isMuted
        isMuted = !isMuted
    }
    
    @objc private func timeSliderValueChanged() {
        
    }
    
    @IBAction func skipButton(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            mediaPlayer.jumpBackward(15)
        case 1:
            mediaPlayer.jumpForward(15)
        default: break
        }
    }
    
    @IBAction func changeTapped(_ sender: UIButton) {
        setInstrumentsHidden(true)
        
        guard isOnline else { return }
        
        if isArchive {
            // Переход в онлайн
            guard let onlineUrl = onlineUrl else { return }
            
            playMedia(url: onlineUrl)
            changeState = .toArchive
            
            isArchive = false
        } else {
            // Переход в архив
            guard let actualTeleprogram = actualTeleprogram,
                  let archiveUrl = URL(string: "\(chanell.server)/\(chanell.codeName)/archive-\(actualTeleprogram.startDate)-\(Int(actualTeleprogram.stopDate)!-Int(actualTeleprogram.startDate)!).m3u8?token=\(chanell.token)") else { return }
            
            playMedia(url: archiveUrl)
            changeState = .toOnline
            
            isArchive = true
        }
    }
    
    @IBAction func backTapped(_ sender: Any) {
        dismiss(animated: true)
    }
    
    fileprivate func setInstrumentsHidden(_ isHidden: Bool) {
        
        UIView.animate(withDuration: 0.4, delay: 0, options: .transitionCrossDissolve) {
            self.soundButtonView.isHidden = isHidden
            self.closeButtonView.isHidden = isHidden
            
            if self.isArchive {
                self.playPauseButton.isHidden = isHidden
                self.goForwardButton.isHidden = isHidden
                self.goBackwardButton.isHidden = isHidden
            }
        }
    }
}

extension TvPlayerViewController: VLCMediaPlayerDelegate {
    
    func mediaPlayerStateChanged(_ aNotification: Notification!) {
        print("state changed: \(mediaPlayer.isPlaying)")
    }
    
    func mediaPlayerTimeChanged(_ aNotification: Notification!) {
        print("time changed: \(mediaPlayer.time ?? VLCTime())")
    }
    
}
