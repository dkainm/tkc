//
//  ChanellsViewController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 26/8/21.
//

import UIKit

import Nuke

import AVKit
import AVFoundation

class ChanellsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var categoryLabel: UILabel!
    
    var category: TvCategory?
    var channels: [Chanell]?
    var allChannels = [Chanell]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Chanell.chanells = []
        
        if let category = category {
            categoryLabel.text = category.name
        } else {
            categoryLabel.text = "Избранные"
        }
        
        tableView.clipsToBounds = true
        tableView.layer.masksToBounds = false
        
        tableView.register(UINib(nibName: "TvChanellTableViewCell", bundle: nil), forCellReuseIdentifier: "chanell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchData()
    }
    
    func fetchData() {
        
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        tabBarController?.view.addSubview(blurEffectView)
        
        if let category = category {
            ApiManager.shared.updateChanells(categoryId: Int(category.id)!) { [weak self] chanells in
                Chanell.chanells = chanells
                self?.tableView.reloadData()
                
                DispatchQueue.main.async {
                    blurEffectView.removeFromSuperview()
                }
            }
        } else {
            tableView.reloadData()
            DatabaseManager.shared.getAllChannels { [weak self] (channels) in
                self?.channels = channels
                self?.tableView.reloadData()
                
                DispatchQueue.main.async {
                    blurEffectView.removeFromSuperview()
                }
            }
        }
        
    }
    
    @IBAction private func searchTapped(_ sender: UIButton) {
        let nc = storyboard!.instantiateViewController(withIdentifier: "search") as! UINavigationController
        let vc = nc.viewControllers[0] as! SearchTvTableViewController
        vc.channels = allChannels
        nc.modalPresentationStyle = .fullScreen
        present(nc, animated: true)
    }
    
    @IBAction private func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}

extension ChanellsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let channels = channels {
            return channels.count
        } else {
            return Chanell.chanells.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "chanell", for: indexPath) as! TvChanellTableViewCell
        
        if let channels = channels {
            Nuke.loadImage(with: channels[indexPath.row].icon, into: cell.chanellImageView)
            cell.configure(chanell: channels[indexPath.row])
        } else {
            Nuke.loadImage(with: Chanell.chanells[indexPath.row].icon, into: cell.chanellImageView)
            cell.configure(chanell: Chanell.chanells[indexPath.row])
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.contentView.clipsToBounds = false
        cell.contentView.layer.masksToBounds = true
        let radius = cell.contentView.layer.cornerRadius
        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: radius).cgPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let chanellController = storyboard!.instantiateViewController(withIdentifier: "ChanellViewController") as! ChanellViewController
        
        if let channels = channels {
            chanellController.chanell = channels[indexPath.row]
        } else {
            chanellController.chanell = Chanell.chanells[indexPath.row]
        }
        
        chanellController.foundBySearch = false
        
        navigationController?.pushViewController(chanellController, animated: true)
    }
    
    private func play(chanell: Chanell) {
        
        let url = URL(string: "https://www.bloomberg.com/media-manifest/streams/eu.m3u8")!
        
        let asset = AVAsset(url: url)
        let playerItem = AVPlayerItem(asset: asset)
        
        let player = AVPlayer()
        player.replaceCurrentItem(with: playerItem)
        
        let vc = AVPlayerViewController()
        vc.player = player
        
        self.present(vc, animated: true) {
            vc.player?.play()
        }
        
    }
    
}
