//
//  SearchTvTableViewController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 22/3/22.
//

import UIKit

class SearchTvTableViewController: UITableViewController {

    var channels: [Chanell]!
    var filteredChannels: [Chanell] = []
    
    let searchController = UISearchController(searchResultsController: nil)

    var isSearchBarEmpty: Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    var isFiltering: Bool {
        return searchController.isActive && !isSearchBarEmpty
    }

    override open var shouldAutorotate: Bool {
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchController.title = "Поиск"
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Поиск"
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.isNavigationBarHidden = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.isNavigationBarHidden = true
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        let channel: Chanell
        if isFiltering {
            channel = filteredChannels[indexPath.row]
        } else {
            channel = channels[indexPath.row]
        }
        cell.textLabel?.text = channel.name
        return cell
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering {
            return filteredChannels.count
        }
        return channels.count
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let channel: Chanell
        if isFiltering {
            channel = filteredChannels[indexPath.row]
        } else {
            channel = channels[indexPath.row]
        }
        
        let chanellController = storyboard!.instantiateViewController(withIdentifier: "ChanellViewController") as! ChanellViewController
        chanellController.chanell = channel
        chanellController.foundBySearch = true
        navigationController?.pushViewController(chanellController, animated: true)
    }
    
    func filterContentForSearchText(_ searchText: String, channel: Chanell? = nil) {
        filteredChannels = channels.filter { (channel: Chanell) -> Bool in
            return channel.name.lowercased().contains(searchText.lowercased())
        }
        tableView.reloadData()
    }
    
    @IBAction func cancelTapped(_ sender: Any) {
        self.dismiss(animated: true)
    }
}

// MARK: UISearchResultsUpdating

extension SearchTvTableViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        filterContentForSearchText(searchBar.text!)
    }
}
