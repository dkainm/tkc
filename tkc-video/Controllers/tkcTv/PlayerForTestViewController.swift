//
//  PlayerForTestViewController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 12/12/21.
//

import UIKit
import FWPlayerCore

class PlayerForTestViewController: UIViewController {

    var containerView: UIImageView {
        let imageView = UIImageView()
        imageView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16).isActive = true
        imageView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 16).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 130).isActive = true
        imageView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        return imageView
    }
    
    var controlView: FWPlayerControlView {
        let controlView = FWPlayerControlView()
        controlView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 16).isActive = true
        controlView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 16).isActive = true
        controlView.heightAnchor.constraint(equalToConstant: 130).isActive = true
        controlView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        return controlView
    }
    
    private var player: FWPlayerController?
    let playerManager = FWAVPlayerManager()
    
    var url: URL!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        controlView.fastViewAnimated = true
        controlView.autoHiddenTimeInterval = 5.0
        controlView.autoFadeTimeInterval = 0.5
        controlView.prepareShowLoading = true
        controlView.prepareShowControlView = true
        
        playerManager.isEnableMediaCache = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupPlayer()
    }
    
    private func setupPlayer() {
        // Setup player
        self.player = FWPlayerController(playerManager: playerManager, containerView: self.containerView)
        self.player?.controlView = self.controlView
        
        // Setup continue playing in the background
        self.player?.pauseWhenAppResignActive = true
        
        self.player?.orientationWillChange = { [weak self] (player, isFullScreen) in
            self?.setNeedsStatusBarAppearanceUpdate()
        }
        
        // Finished playing
        self.player?.playerDidToEnd = { [weak self] (asset) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.player?.currentPlayerManager.replay!()
            strongSelf.player?.playTheNext()
            if strongSelf.player?.isLastAssetURL == false {
                strongSelf.controlView.showTitle("Video Title", coverURLString: nil, fullScreenMode: .landscape)
            } else {
                strongSelf.player?.stop()
            }
        }
        
        var urls = [URL]()
        urls.append(url)
        print(urls)
        self.player?.assetURLs = urls
    }
    

}
