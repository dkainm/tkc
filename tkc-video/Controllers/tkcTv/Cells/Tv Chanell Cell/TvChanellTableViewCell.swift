//
//  TvChanellTableViewCell.swift
//  tkc-video
//
//  Created by Alex Rudoi on 26/8/21.
//

import UIKit
import Nuke

class TvChanellTableViewCell: UITableViewCell {

    @IBOutlet weak var nameTitle: UILabel!
    @IBOutlet weak var chanellImageView: UIImageView!
    @IBOutlet weak var imageBackView: UIView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var pgLabel: UILabel!
    @IBOutlet weak var pgBackView: UIView!
    @IBOutlet weak var programLabel: UILabel!
    
    var chanell: Chanell! {
        didSet {
            nameTitle.text = chanell.name
            pgLabel.text = "\(chanell.pg)+"
            programLabel.text = chanell.program
            setup()
        }
    }
    
    private func setup() {
        
        backgroundColor = .clear
        
        imageBackView.layer.shadowColor = UIColor.black.cgColor
        imageBackView.layer.shadowOpacity = 0.2
        imageBackView.layer.shadowOffset = CGSize(width: 0, height: 0)
        imageBackView.layer.shadowRadius = 12
        imageBackView.layer.cornerRadius = 5
        
        pgBackView.layer.shadowColor = UIColor.black.cgColor
        pgBackView.layer.shadowOpacity = 0.2
        pgBackView.layer.shadowOffset = CGSize(width: 0, height: 2)
        pgBackView.layer.shadowRadius = 3
        pgBackView.layer.cornerRadius = 5
        pgBackView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMaxYCorner]

        backView.layer.shadowColor = UIColor.black.cgColor
        backView.layer.shadowOpacity = 0.1
        backView.layer.shadowOffset = CGSize(width: 0, height: 0)
        backView.layer.shadowRadius = 12
        
        contentView.backgroundColor = .white
        contentView.layer.cornerRadius = 8
    }
    
    func configure(chanell: Chanell) {
        self.chanell = chanell
        clipsToBounds = false
    }
}
