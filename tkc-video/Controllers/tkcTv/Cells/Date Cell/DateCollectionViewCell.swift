//
//  DateCollectionViewCell.swift
//  tkc-video
//
//  Created by Alex Rudoi on 2/9/21.
//

import UIKit

class DateCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var weekDayLabel: UILabel!
    
    let activeColor = UIColor(named: "primary")!
    let passiveColor = #colorLiteral(red: 0.1294117647, green: 0.1294117647, blue: 0.1294117647, alpha: 1)
    
    let textGrayColor = #colorLiteral(red: 0.7607843137, green: 0.7607843137, blue: 0.7607843137, alpha: 1)
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                backView.backgroundColor = activeColor
                weekDayLabel.textColor = .white
            } else {
                backView.backgroundColor = passiveColor
                weekDayLabel.textColor = textGrayColor
            }
        }
    }
    
    var date: Date! {
        didSet {
            dateLabel.text = TimeService.shared.getPartFromDate(date, part: .dayAndMonth)
            weekDayLabel.text = TimeService.shared.getPartFromDate(date, part: .weekday)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(date: Date) {
        self.date = date
        
        backView.layer.cornerRadius = 5
    }

}
