//
//  ContractTableViewCell.swift
//  tkc-video
//
//  Created by Alex Rudoi on 13/9/21.
//

import UIKit

class ContractTableViewCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    
    let selectedColor = UIColor(named: "primary")!
    let unselectedColor = #colorLiteral(red: 0.7568627451, green: 0.7960784314, blue: 0.8352941176, alpha: 1)
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                nameLabel.textColor = selectedColor
            } else {
                nameLabel.textColor = unselectedColor
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(contract: Contract) {
        nameLabel.text = contract.name
    }
}
