//
//  TeleprogramCollectionViewCell.swift
//  tkc-video
//
//  Created by Alex Rudoi on 2/9/21.
//

import UIKit

class TeleprogramCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                backView.layer.borderWidth = 1
                backView.layer.borderColor = UIColor(named: "primary")!.cgColor
            } else {
                backView.layer.borderWidth = 0
            }
        }
    }
    
    var teleprogram: Teleprogram! {
        didSet {
            timeLabel.text = getTime(date: getDate(from: teleprogram.startDate))
            nameLabel.text = teleprogram.name
            descLabel.text = teleprogram.desc
        }
    }
    
    private func getTime(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "hh:mm"
        return formatter.string(from: date)
    }
    
    func configure(teleprogram: Teleprogram) {
        self.teleprogram = teleprogram
    }

}
