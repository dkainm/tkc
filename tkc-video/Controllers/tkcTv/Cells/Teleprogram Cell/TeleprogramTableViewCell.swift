//
//  TeleprogramTableViewCell.swift
//  tkc-video
//
//  Created by Alex Rudoi on 2/9/21.
//

import UIKit

class TeleprogramTableViewCell: UITableViewCell {
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                backView.layer.borderWidth = 1
                backView.layer.borderColor = UIColor(named: "primary")!.cgColor
            } else {
                backView.layer.borderWidth = 0
            }
        }
    }
    
    var teleprogram: Teleprogram! {
        didSet {
            timeLabel.text = getTime(date: TimeService.shared.getDateFromUnix(TimeInterval(teleprogram.startDate)!))
            nameLabel.text = teleprogram.name
            descLabel.text = teleprogram.desc
            setup()
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    private func setup() {
        backgroundColor = .clear
        
        backView.layer.masksToBounds = false
        backView.layer.shadowOffset = CGSize(width: 0, height: 4)
        backView.layer.shadowRadius = 4
        backView.layer.shadowColor = UIColor.black.cgColor
        backView.layer.shadowOpacity = 0.15
        
        contentView.backgroundColor = .white
    }
    
    private func getTime(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        return formatter.string(from: date)
    }
    
    func configure(teleprogram: Teleprogram) {
        self.teleprogram = teleprogram
        
        clipsToBounds = false
        contentView.clipsToBounds = false
        
        backgroundColor = .clear
        contentView.backgroundColor = .clear
    }
    
}
