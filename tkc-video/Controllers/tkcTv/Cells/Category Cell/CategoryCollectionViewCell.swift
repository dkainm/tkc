//
//  CategoryCollectionViewCell.swift
//  tkc-tv
//
//  Created by Alex Rudoi on 23/8/21.
//

import UIKit
import Nuke

class CategoryCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var whiteBG: UIView!
    
    var category: TvCategory? {
        didSet {
            titleLabel.text = category?.name
            setup()
        }
    }
    
    private func setup() {
        
        if category == nil {
            titleLabel.text = "Избранные"
            categoryImageView.image = UIImage(named: "starFavorites")
        }
        
        whiteBG.layer.masksToBounds = false
        whiteBG.layer.shadowColor = UIColor.black.cgColor
        whiteBG.layer.shadowOpacity = 0.1
        whiteBG.layer.shadowOffset = CGSize(width: 0, height: 4)
        whiteBG.layer.shadowRadius = 15
    }
    
    func configure(category: TvCategory) {
        self.category = category
        
        clipsToBounds = false
        contentView.clipsToBounds = false
        
        backgroundColor = .clear
        contentView.backgroundColor = .clear
    }
    
    func configure(favorites: [Chanell]) {
        setup()
        
        clipsToBounds = false
        contentView.clipsToBounds = false
        
        backgroundColor = .clear
        contentView.backgroundColor = .clear
    }

}
