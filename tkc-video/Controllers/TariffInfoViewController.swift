//
//  TariffInfoViewController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 15/12/21.
//

import UIKit

class TariffInfoViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var tag = 0
    
    var icon: URL?
    
    var tariffInfos = [PersonalDataServiceSubInfo]()
    
    override func viewDidLoad() {
        tableView.register(UINib(nibName: "TariffInfoTableViewCell", bundle: nil), forCellReuseIdentifier: "tariffInfo")
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
        
        guard let title = title else { return }
        titleLabel.text = title
    }
    
    @IBAction func backTapped(_ sender: UIButton) {
        dismiss(animated: true)
    }
}

extension TariffInfoViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tariffInfos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tariffInfo", for: indexPath) as! TariffInfoTableViewCell
        let sIcon = State.currentPersonalData.services[tag].icon
        cell.configure(subInfo: tariffInfos[indexPath.row], icon: icon ?? sIcon)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
