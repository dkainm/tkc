//
//  WebViewViewController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 23/9/21.
//

import UIKit
import WebKit

class WebViewViewController: UIViewController, WKNavigationDelegate {

    @IBOutlet weak var webView: WKWebView!
    
    var url: URL!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
        tabBarController?.tabBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.navigationDelegate = self
        webView.load(URLRequest(url: url))
    }
    
    override func viewWillLayoutSubviews() {
        if #available(iOS 13.0, *) {
            webView.topAnchor.constraint(equalTo: view.topAnchor, constant: 40).isActive = true
            let cancelButton = UIButton(frame: CGRect(x: 16, y: 0, width: 80, height: 40))
            
            cancelButton.setTitleColor(UIColor(named: "primary"), for: .normal)
            
            cancelButton.setTitle("Закрыть", for: .normal)
            cancelButton.addTarget(self, action: #selector(cancelTapped), for: .touchUpInside)
            
            view.addSubview(cancelButton)
        } else {
            webView.topAnchor.constraint(equalTo: view.topAnchor, constant: 72).isActive = true
            let cancelButton = UIButton(frame: CGRect(x: 16, y: 40, width: 80, height: 25))
            
            cancelButton.setTitleColor(UIColor(named: "primary"), for: .normal)
            
            cancelButton.setTitle("Закрыть", for: .normal)
            cancelButton.addTarget(self, action: #selector(cancelTapped), for: .touchUpInside)
            
            view.addSubview(cancelButton)
        }
    }
    
    @objc func cancelTapped() {
        dismiss(animated: true)
    }
    
}
