//
//  MainSupportViewController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 21/9/21.
//

import UIKit
import Whisper

class MainSupportViewController: UIViewController {

    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var phoneTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var messageTV: UITextView!
    @IBOutlet weak var messageBackView: UIView!
    @IBOutlet weak var instructionView: UIView!
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var messageBackConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        phoneTF.delegate = phoneInputController
        phoneTF.keyboardType = .namePhonePad
        phoneTF.textContentType = .telephoneNumber

        messageTV.textColor = UIColor.lightGray
        messageTV.delegate = self
        
        let fixedWidth = messageTV.frame.size.width
        let newSize = messageTV.sizeThatFits(CGSize(width: fixedWidth, height: CGFloat.greatestFiniteMagnitude))
        messageTV.frame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
        
        instructionView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(instructionTapped)))
        phoneView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(phoneTapped)))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        clean()
        if let personal = State.currentPersonalData {
            nameTF.text = personal.fullName
            if let phone = personal.phoneNumber.first?.phone.tail(from: 0) {
                phoneTF.text = phoneDataFormatter.format(phone)
            }
            emailTF.text = personal.email
        }
    }
    
    @IBAction func sendTapped(_ sender: Any) {
        let phone = "+7" + phoneFormatter.unformat(phoneTF.text!)!
        guard nameTF.text!.containsText(),
              phone.containsText(),
              phone.containsPhone(),
              emailTF.text!.containsText(),
              messageTV.text.containsText() ,
              messageTV.text != "C чем у вас возникли проблемы?" else {
            
            var info = ""
            if !nameTF.text!.containsText() {
                info = "Поле «ФИО» должно быть заполнено"
            }
            if !phone.containsText() || !phone.containsPhone() {
                info = "Поле «Номер телефона» должно быть заполнено"
            }
            if !emailTF.text!.containsText() {
                info = "Поле «Email» должно быть заполнено"
            }
            if !messageTV.text!.containsText() ||
                messageTV.text == "C чем у вас возникли проблемы?" {
                info = "Поле «Описание» должно быть заполнено"
            }
            
            showWarningView(text: "Некорректное значение",
                            info: info)
            return
        }
        ApiManager.shared.sendSupport(name: nameTF.text!,
                                      email: emailTF.text!,
                                      phone: phone,
                                      message: messageTV.text,
                                      subject: .support) { [weak self] (info) in
            if info == "Сообщение успешно отправленно" {
                let alert = UIAlertController(title: "Заявка успешно создана", message: nil, preferredStyle: .alert)
                alert.view.tintColor = UIColor(named: "primary")!
                alert.addAction(UIAlertAction(title: "OK", style: .default))
                self?.present(alert, animated: true)
                
                self?.clean()
            }
        }
    }
    
    @objc func instructionTapped() {
        if let url = URL(string: "https://telecom-service.site/application-instruction/") {
            let webController = storyboard!.instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
            webController.url = url
            navigationController?.pushViewController(webController, animated: true)
        }
    }
    
    @objc func phoneTapped() {
        let phone = "+74733004000"
        if let url = URL(string: "tel://\(phone)"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
    }
    
    private func clean() {
        nameTF.text = ""
        phoneTF.text = ""
        emailTF.text = ""
        messageTV.textColor = UIColor.lightGray
        messageTV.text = "C чем у вас возникли проблемы?"
    }
}

extension MainSupportViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        
        sizeToFit(textView: textView)
    }
    
    func sizeToFit(textView: UITextView) {
        textView.translatesAutoresizingMaskIntoConstraints = true
        textView.sizeToFit()
        
        let height = textView.bounds.size.height
        messageBackView.translatesAutoresizingMaskIntoConstraints = false
        messageBackConstraint.constant = height - 12
        
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.widthAnchor.constraint(equalToConstant: messageBackView.bounds.size.width - 2).isActive = true
        textView.bottomAnchor.constraint(equalTo: messageBackView.topAnchor).isActive = true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "C чем у вас возникли проблемы?"
            textView.textColor = UIColor.lightGray
        }
    }
}
