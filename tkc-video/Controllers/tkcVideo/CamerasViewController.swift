//
//  ViewController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 16/5/21.
//

import UIKit
import AVKit
import FlussonicSDK
import AlignedCollectionViewFlowLayout

class CamerasViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var orientButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    var cams: [Camera] = [] {
        didSet {
            checkOrient()
        }
    }
    var object: Object?
    
    var listImage = UIImage(named: "list")!
    var collectionImage = UIImage(named: "collection")!
    var backImage = UIImage(named: "back")!
    
    var isObj = false
    var isSearch = false
    
    var orient: Orient = .list {
        didSet {
            if orient == .list {
                orientButton.setImage(collectionImage, for: .normal)
            } else if orient == .collection {
                orientButton.setImage(listImage, for: .normal)
            }
            UserDefaults.standard.setValue(orient.rawValue, forKey: "orient")
        }
    }
    
    enum Orient: String {
        case list = "list"
        case collection = "collection"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if isSearch {
            navigationController?.setNavigationBarHidden(false, animated: animated)
        }
    }
    
    override func viewDidLoad() {
        
        fetchData()
        
        if let or = UserDefaults.standard.string(forKey: "orient") {
            let orr = Orient(rawValue: or)
            orient = orr ?? .list
        }
        super.viewDidLoad()
        
        if let titleName = title {
            titleLabel.text = titleName
        }
        
        let alignedFlowLayout = AlignedCollectionViewFlowLayout(
            horizontalAlignment: .justified,
            verticalAlignment: .top
        )

        alignedFlowLayout.estimatedItemSize = .zero
        alignedFlowLayout.itemSize = .init(width: 160, height: 130)
        
        collectionView.collectionViewLayout = alignedFlowLayout
        
        collectionView.register(UINib(nibName: "CameraCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cCell")
        tableView.register(UINib(nibName: "CameraTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        collectionView.reloadData()
        tableView.reloadData()
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
       get {
          return .portrait
       }
    }
    
    func fetchData() {
        ApiManager.shared.getAllCams { [weak self] cams in
            self?.cams = cams
            
            if self!.isObj {
                let name = self?.object!.name
                var arr: [Camera] = []
                for cam in cams {
                    if cam.object == name {
                        arr.append(cam)
                    }
                }
                self?.cams = arr
            }
        }
    }
    
    @IBAction func dismissTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func changeOrientTapped(_ sender: UIButton) {
        switch orient {
        case .list:
            orient = .collection
            break
        case .collection:
            orient = .list
            break
        }
        checkOrient()
    }
    
    private func checkOrient() {
        switch orient {
        case .collection:
            collectionView.isHidden = false
            tableView.isHidden = true
            collectionView.reloadData()
        case .list:
            collectionView.isHidden = true
            tableView.isHidden = false
            tableView.reloadData()
        }
    }
    
    @IBAction func searchTapped(_ sender: UIButton) {
        let nc = storyboard!.instantiateViewController(withIdentifier: "search") as! UINavigationController
        let vc = nc.viewControllers[0] as! SearchTableViewController
        vc.cams = self.cams
        nc.modalPresentationStyle = .fullScreen
        present(nc, animated: true)
    }
}

extension CamerasViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cams.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CameraTableViewCell
        
        var camera = cams[indexPath.row]
        
        if camera.renamename != "" {
            camera.name = camera.renamename
        }
        
        cell.selectionStyle = .none
        cell.camera = camera
        cell.delegate = self
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! CameraTableViewCell
        
        let vc = storyboard!.instantiateViewController(withIdentifier: "PlayerViewController") as! PlayerViewController
        vc.url = cell.playerUrl!
        vc.camera = cell.camera!
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    override open var shouldAutorotate: Bool {
        return false
    }
    
}

extension UINavigationController {
    
    override open var shouldAutorotate: Bool {
        get {
            if let visibleVC = visibleViewController {
                return visibleVC.shouldAutorotate
            }
            return super.shouldAutorotate
        }
    }
    
    override open var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation{
        get {
            if let visibleVC = visibleViewController {
                return visibleVC.preferredInterfaceOrientationForPresentation
            }
            return super.preferredInterfaceOrientationForPresentation
        }
    }
    
    override open var supportedInterfaceOrientations: UIInterfaceOrientationMask{
        get {
            if let visibleVC = visibleViewController {
                return visibleVC.supportedInterfaceOrientations
            }
            return super.supportedInterfaceOrientations
        }
    }
}

extension CamerasViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cams.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cCell", for: indexPath) as! CameraCollectionViewCell
        
        var camera = cams[indexPath.row]
        
        if camera.renamename != "" {
            camera.name = camera.renamename
        }
        
        cell.camera = camera
        cell.delegate = self
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! CameraCollectionViewCell
        
        let vc = storyboard!.instantiateViewController(withIdentifier: "PlayerViewController") as! PlayerViewController
        vc.url = cell.playerUrl!
        vc.camera = cell.camera!
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
}

extension CamerasViewController: CameraCellDelegate {
    func favoriteChanged() {
        self.fetchData()
    }
}


