//
//  HelpViewController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 19/7/21.
//

import UIKit

class HelpViewController: UIViewController {
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var button5: UIView!
    @IBOutlet weak var button6: UIView!
    @IBOutlet weak var button5Image: UIImageView!
    @IBOutlet weak var button6Image: UIImageView!
    
    @IBOutlet var buttons: [CustomButton]!
    @IBOutlet var views: [UIView]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        views.forEach { (view) in
            view.layer.cornerRadius = 10
            view.layer.shadowColor = UIColor.black.cgColor
            view.layer.shadowOpacity = 0.15
            view.layer.shadowOffset = CGSize(width: 0, height: 4)
            view.layer.shadowRadius = 15
            view.isHidden = true
        }
        
        buttons.forEach { (button) in
            button.hideIcon(.left)
            button.rightImage = #imageLiteral(resourceName: "arrowDown")
        }
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(button5Tapped))
        button5.addGestureRecognizer(tap)
        
        button5Image.image = #imageLiteral(resourceName: "arrowDown")
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(button6Tapped))
        button6.addGestureRecognizer(tap2)
        
        button6Image.image = #imageLiteral(resourceName: "arrowDown")
        
        button5.layer.cornerRadius = 10
        button5.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        button5.layer.shadowOffset = CGSize(width: 0, height: 4)
        button5.layer.shadowOpacity = 0.16
        button5.layer.shadowRadius = 8
        
        button6.layer.cornerRadius = 10
        button6.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        button6.layer.shadowOffset = CGSize(width: 0, height: 4)
        button6.layer.shadowOpacity = 0.16
        button6.layer.shadowRadius = 8
    }
    
    @objc func button5Tapped() {
        button5.showAnimation {}
        if views[4].isHidden {
            button5Image.image = #imageLiteral(resourceName: "arrowUp")
        } else {
            button5Image.image = #imageLiteral(resourceName: "arrowDown")
        }
        views[4].isHidden.toggle()
    }
    
    @objc func button6Tapped() {
        button6.showAnimation {}
        if views[5].isHidden {
            button6Image.image = #imageLiteral(resourceName: "arrowUp")
        } else {
            button6Image.image = #imageLiteral(resourceName: "arrowDown")
        }
        views[5].isHidden.toggle()
    }
    
    @IBAction func buttonTapped(_ sender: CustomButton) {
        viewToggle(button: sender, viewToAnimate: views[sender.tag])
    }
    
    private func viewToggle(button: CustomButton, viewToAnimate: UIView) {
        if viewToAnimate.isHidden {
            button.rightImage = #imageLiteral(resourceName: "arrowUp")
        } else {
            button.rightImage = #imageLiteral(resourceName: "arrowDown")
        }
        viewToAnimate.isHidden.toggle()
    }
    
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}
