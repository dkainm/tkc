//
//  OtherViewController.swift
//  tkc-video
//
//  Created by Алексей Волков on 13.06.2021.
//

import UIKit

class OtherViewController: UIViewController {
    
    @IBOutlet weak var callButton: CustomButton!
    @IBOutlet weak var helpButton: CustomButton!
    @IBOutlet weak var logOutButton: CustomButton!
    
    override open var shouldAutorotate: Bool {
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        callButton.leftImage = UIImage(named: "settings")!
        callButton.rightImage = UIImage(named: "next")!
        helpButton.leftImage = UIImage(named: "support")!
        helpButton.rightImage = UIImage(named: "next")!
        logOutButton.leftImage = UIImage(named: "logOut")!
        logOutButton.rightImage = UIImage(named: "next")!
    }
    
    @IBAction func helpTapped(_ sender: Any) {
        let vc = storyboard!.instantiateViewController(withIdentifier: "HelpViewController") as! HelpViewController
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func logOutButton(_ sender: Any) {
        let alert = UIAlertController(title: "Вы уверены что хотите выйти из учетной записи?", message: nil, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Отменить", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Выйти", style: .default) { (_) in
            endSession()
            ApiManager.shared.validateSession()
        })
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func callToSupport(_ sender: Any) {
        let vc = storyboard!.instantiateViewController(withIdentifier: "SupportViewController") as! SupportViewController
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
