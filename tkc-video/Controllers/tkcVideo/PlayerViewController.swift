//
//  PlayerViewController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 14/6/21.
//

import UIKit
import FlussonicSDK
import DynamicMobileVLCKit
import AVKit
import Photos
import SVProgressHUD
import Alamofire
import Digger

class PlayerViewController: UIViewController, AVPlayerViewControllerDelegate, FlussonicDownloadRequestListener, FlussonicWatcherDelegateProtocol {
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var toolView: UIView!
    @IBOutlet weak var playerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var renameIcon: UIButton!
    @IBOutlet weak var backIcon: UIButton!
    @IBOutlet weak var fixBButton: UIButton!
    @IBOutlet weak var resetBButton: UIButton!
    @IBOutlet weak var playBButton: UIButton!
    @IBOutlet weak var fixStack: UIStackView!
    @IBOutlet weak var playStack: UIStackView!
    @IBOutlet weak var resetStack: UIStackView!
    @IBOutlet weak var fixView: UIView!
    @IBOutlet weak var playView: UIView!
    @IBOutlet weak var resetView: UIView!
    @IBOutlet weak var toolViewHeight: NSLayoutConstraint!
    @IBOutlet weak var backBgView: UIView!
    
    var cameraView: PreviewMp4View!
    var camera: Camera!
    
    var isArchiving = false
    var savedTime: Int64?
    
    private var playerAdapter = FlussonicPlayerAdapter()
    private var watcherView = FlussonicWatcherView()
    
    var url: URL!
    var downloadingUrl: URL!
    
    enum Rotated {
        case landscape
        case portrait
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        SVProgressHUD.setBackgroundColor(UIColor.white)
        AppDelegate.orientationLock = UIInterfaceOrientationMask.allButUpsideDown
        UINavigationController.attemptRotationToDeviceOrientation()
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        SVProgressHUD.setBackgroundColor(UIColor.clear)
        AppDelegate.orientationLock = UIInterfaceOrientationMask.portrait
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        UINavigationController.attemptRotationToDeviceOrientation()
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    func setCameraName() {
        if camera.renamename.containsText() {
            camera.name = camera.renamename
        }
    }
    
    override func viewDidLoad() {
        watcherView.delegate = self
        
        fixBButton.layer.borderWidth = 1
        fixBButton.layer.borderColor = #colorLiteral(red: 0.2156862745, green: 0.231372549, blue: 0.262745098, alpha: 1)
        resetBButton.layer.borderWidth = 1
        resetBButton.layer.borderColor = #colorLiteral(red: 0.2156862745, green: 0.231372549, blue: 0.262745098, alpha: 1)
        playBButton.layer.borderWidth = 1
        playBButton.layer.borderColor = #colorLiteral(red: 0.2156862745, green: 0.231372549, blue: 0.262745098, alpha: 1)
        
        if UserDefaults.standard.value(forKey: "savedTime") != nil {
            resetStack.isHidden = false
            playStack.isHidden = false
            fixStack.isHidden = true
        } else {
            resetStack.isHidden = true
            playStack.isHidden = true
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        setCameraName()
        
        let photos = PHPhotoLibrary.authorizationStatus()
        if photos == .notDetermined {
            PHPhotoLibrary.requestAuthorization() { (status) in
                print(status) 
            }
        }
        
        titleLabel.text = camera.name
        backIcon.setImage(#imageLiteral(resourceName: "back").tintPicto(.white), for: .normal)
        
        if ApiManager.shared.isDemoUser() {
            renameIcon.isHidden = true
        } else {
            renameIcon.isHidden = false
            renameIcon.setImage(renameIcon.image(for: .normal)?.tintPicto(.white), for: .normal)
        }
        
        guard var playerUrl = self.url else { return }
        
        setUp()
        watcherView.configure(withUrl: playerUrl, playerAdapter: playerAdapter)
        watcherView.downloadRequestListener = self
        watcherView.alertDelegate = self
        
        rotated()
        NotificationCenter.default.addObserver(self, selector: #selector(rotated), name: UIDevice.orientationDidChangeNotification, object: nil)
        
        if camera.active == "0" {
            let alert = UIAlertController(title: "Доступ для просмотра этой камеры ограничен администратором. Пожалуйста, обратитесь по номеру +7 (473) 300-40-00 в случае нарушения правопорядка.", message: nil, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
                self.dismiss(animated: true)
            }))
            present(alert, animated: true)
        }
        
    }
    
    func animateNav(show: Bool) {
        //show
        if show {
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear) {
                self.navView.isHidden = false
            }
        }
        //hide
        else {
            UIView.animate(withDuration: 0.3, delay: 0, options: .curveLinear) {
                self.navView.isHidden = true
            }
        }
    }
    
    func screenRotated(rotatedTo rotation: Rotated) {
        if rotation == .landscape {
            animateNav(show: false)
            navView.alpha = 0.75
            toolViewHeight.constant = 0
            toolView.isHidden = true
            backBgView.isHidden = true
        } else if rotation == .portrait {
            animateNav(show: true)
            titleLabel.isHidden = false
            if !ApiManager.shared.isDemoUser() {
                renameIcon.isHidden = false
            }
            navView.backgroundColor = .black
            toolViewHeight.constant = 30
            toolView.isHidden = false
            backBgView.isHidden = false
            backIcon.setImage(#imageLiteral(resourceName: "back").tintPicto(.white), for: .normal)
        }
    }
    
    @objc func rotated() {
        
        switch UIDevice.current.orientation {
        case .portrait:
            screenRotated(rotatedTo: .portrait)
        case .landscapeLeft:
            screenRotated(rotatedTo: .landscape)
        case .landscapeRight:
            screenRotated(rotatedTo: .landscape)
        default: break
        }
        
    }
    
    //Вызывается плеером, когда началось разворачивание нижней панели инструментов.
    func expandToolbar() {
        animateNav(show: true)
    }
    
    //Вызывается плеером, когда началось сворачивание нижней панели инструментов.
    func collapseToolbar() {
        animateNav(show: false)
    }
    
    //Вызывается плеером, когда нижняя панель инструментов появилась без анимации
    func showToolbar() {
        
    }
    
    //Вызывается плеером, когда нижняя панель инструментов скрылась без анимации.
    func hideToolbar() {
        
    }
    
    @IBAction func fixTapped(_ sender: Any) {
        resetStack.isHidden = false
        playStack.isHidden = false
        fixStack.isHidden = true
        
//        savedTime = watcherView.getCurrentUtcInSeconds()
        UserDefaults.standard.set(watcherView.getCurrentUtcInSeconds(), forKey: "savedTime")
        watcherView.pause()
        
        temporaryAlert(text: "Время сохранено")
    }
    
    @IBAction func reset(_ sender: Any) {
        savedTime = nil
        UserDefaults.standard.set(nil, forKey: "savedTime")
        watcherView.resume()
        
        resetStack.isHidden = true
        playStack.isHidden = true
        fixStack.isHidden = false
        
        temporaryAlert(text: "Время удалено")
    }
    
    @IBAction func playTapped(_ sender: Any) {
        guard let time = UserDefaults.standard.value(forKey: "savedTime") as? Int else { return }
        watcherView.seek(seconds: TimeInterval(time))
    }
    
    private func temporaryAlert(text: String) {
        
        let alert = UIAlertController(title: "", message: text, preferredStyle: .alert)
        self.present(alert, animated: true, completion: nil)
        
        let when = DispatchTime.now() + 0.8
        DispatchQueue.main.asyncAfter(deadline: when) {
          alert.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func editTapped(_ sender: Any) {
        let alert = UIAlertController(title: "Введите новое название для этой камеры", message: nil, preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.placeholder = ""
        }
        let textField = alert.textFields?.first!
        alert.addAction(UIAlertAction(title: "Сохранить", style: .default, handler: { (_) in
            guard let text = textField?.text else { return }
            ApiManager.shared.renameName(to: text, for: self.camera.id) {
                ApiManager.shared.errorAlert(title: "Успешно сохранено", text: "")
            }
        }))
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel))
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    private func setUp() {
        self.watcherView.translatesAutoresizingMaskIntoConstraints = false
        self.playerView.insertSubview(self.watcherView, at: 1)
        self.watcherView.leadingAnchor.constraint(equalTo: self.playerView.leadingAnchor).isActive = true
        self.watcherView.trailingAnchor.constraint(equalTo: self.playerView.trailingAnchor).isActive = true
        self.watcherView.topAnchor.constraint(equalTo: self.playerView.topAnchor).isActive = true
        self.watcherView.bottomAnchor.constraint(equalTo: self.playerView.bottomAnchor).isActive = true
    }
    
    func onDownloadRequest(from: Int64, to: Int64) {
        
        let videoUnits = to - from
        self.downloadingUrl = URL(string: "\(camera.server)/cam_\(camera.id)/archive-\(from)-\(videoUnits).mp4?precise=true&token=\(camera.token)")
        print(self.downloadingUrl!)
        
        SVProgressHUD.show(withStatus: "Скачивание")
        
        DispatchQueue.global(qos: .background).async {
            if let url = URL(string: self.downloadingUrl.absoluteString),
               let urlData = NSData(contentsOf: url) {
                let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0];
                let filePath="\(documentsPath)/tempFile.mp4"
                DispatchQueue.main.async {
                    urlData.write(toFile: filePath, atomically: true)
                    PHPhotoLibrary.shared().performChanges({
                        PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: URL(fileURLWithPath: filePath))
                    }) { completed, error in
                        if completed {
                            SVProgressHUD.dismiss()
                            DispatchQueue.main.async {
                                ApiManager.shared.errorAlert(title: "Success", text: "Video is saved to photo library")
                            }
                        } else {
                            SVProgressHUD.dismiss()
                            DispatchQueue.main.async {
                                ApiManager.shared.errorAlert(text: error!.localizedDescription)
                            }
                        }
                    }
                }
            }
        }
        
    }
    
}

