//
//  OnboardingViewController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 1/6/21.
//

import UIKit
class OnboardingViewController: UIViewController{
    
    @IBOutlet weak var onboadingLabel: UILabel!
    @IBOutlet weak var onboardingImage: UIImageView!
    @IBOutlet weak var onboardingButton: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    
    //Данные для онбордига
    var labelArray = ["Просматривайте все доступные камеры", "Добавляейте камеры в избранное для быстрого доступа", "Войдите и получите доступ к видеокамерам своей организации"]
    var imageArray = [UIImage(named: "onboarding1"), UIImage(named: "onboarding2"), UIImage(named: "onboarding3")]
    var buttonArray = ["Далее", "Далее","Начнём!"]
    
    var indexOnboarding = 0 //Стартовая страница онбординга
    
    override func viewDidLoad() {
        
        
        //создаем онбординг
        onboadingLabel.text = labelArray.first
        onboardingImage.image = imageArray.first!
        onboardingButton.setTitle(buttonArray.first!, for: .normal)
        indexOnboarding += 1
    }
    @IBAction func onboardingTapped(_ sender: Any) {
        //Прописываем механику онбординга
        if indexOnboarding == labelArray.count {
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RegistrationViewController") as! RegistrationViewController
            
            vc.modalPresentationStyle = .fullScreen
            present(vc, animated: true, completion: nil)
            
        } else {
            onboadingLabel.text = labelArray[indexOnboarding]
            onboardingImage.image = imageArray[indexOnboarding]
            onboardingButton.setTitle(buttonArray[indexOnboarding], for: .normal)
            indexOnboarding += 1
            pageControl.currentPage += 1
        }
    }
    
    
    
    
}
