//
//  VideoMenuViewController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 21/9/21.
//

import UIKit

class VideoMenuViewController: UIViewController {

    @IBOutlet var tabs: [UIView]?
    
    @IBOutlet weak var videoTab: UIView!
    @IBOutlet weak var favoritesTab: UIView!
    @IBOutlet weak var objectsTab: UIView!
    @IBOutlet weak var helpTab: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setUpViews()
    }
    
    private func setUpViews() {
        setGestures()
        setShadows()
        setGradientBackground(tab: videoTab)
        setGradientBackground(tab: favoritesTab)
        setGradientBackground(tab: objectsTab)
        setGradientBackground(tab: helpTab)
    }
    
    @objc func videoTapped() {
        videoTab.showAnimation() { [weak self] in
            let cameras = self?.storyboard!.instantiateViewController(withIdentifier: "CamerasViewController") as! CamerasViewController
            self?.navigationController?.pushViewController(cameras, animated: true)
        }
    }
    
    @objc func favoritesTapped() {
        favoritesTab.showAnimation() { [weak self] in
            let fav = self?.storyboard!.instantiateViewController(withIdentifier: "FavoritesViewController") as! FavoritesViewController
            self?.navigationController?.pushViewController(fav, animated: true)
        }
    }
    
    @objc func objectsTapped() {
        objectsTab.showAnimation() { [weak self] in
            let objects = self?.storyboard!.instantiateViewController(withIdentifier: "ObjectsViewController") as! ObjectsViewController
            self?.navigationController?.pushViewController(objects, animated: true)
        }
    }
    
    @objc func helpTapped() {
        helpTab.showAnimation() { [weak self] in
            let help = self?.storyboard!.instantiateViewController(withIdentifier: "HelpViewController") as! HelpViewController
            self?.navigationController?.pushViewController(help, animated: true)
        }
    }
    
    private func setGradientBackground(tab: UIView) {
        
        tab.clipsToBounds = true
        tab.layer.cornerRadius = 16
        
        let colorTop: CGColor =  #colorLiteral(red: 0.8698514104, green: 0.1220991984, blue: 0, alpha: 1)
        let colorBottom: CGColor = #colorLiteral(red: 0.5881579518, green: 0.06815788895, blue: 0, alpha: 1)
                    
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.frame = tab.bounds
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 1)
            
        tab.backgroundColor = .clear
        tab.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    private func setGestures() {
        videoTab.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(videoTapped)))
        favoritesTab.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(favoritesTapped)))
        objectsTab.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(objectsTapped)))
        helpTab.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(helpTapped)))
    }
    
    private func setShadows() {
        tabs?.forEach({ (tab) in
            tab.layer.shadowColor = UIColor.black.cgColor
            tab.layer.shadowOffset = CGSize(width: 0, height: 4)
            tab.layer.shadowRadius = 4
            tab.layer.shadowOpacity = 0.25
        })
    }

}
