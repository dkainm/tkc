//
//  SupportViewController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 5/7/21.
//

import UIKit
import SVProgressHUD

class SupportViewController: UIViewController {
    
    @IBOutlet weak var nameTF: CustomInput!
    @IBOutlet weak var phoneTF: CustomInput!
    @IBOutlet weak var problemTF: CustomInput!
    @IBOutlet weak var supportView: UIView!
    @IBOutlet weak var problemTextView: UITextView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarController?.tabBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(supportTapped))
        supportView.addGestureRecognizer(tapGesture)
        
        nameTF.placeholder = "Ваше имя"
        phoneTF.placeholder = "Номер телефона или email"
        problemTF.placeholder = "C чем у вас возникли проблемы?"
        problemTF.isHidden = true
        
        problemTextView.text = "C чем у вас возникли проблемы?"
        problemTextView.textColor = UIColor.lightGray
        problemTextView.delegate = self
        
        nameTF.textField.textContentType = .name
        phoneTF.textField.textContentType = .emailAddress
        problemTF.textField.textContentType = .none
        
        nameTF.hideIcon(.all)
        phoneTF.hideIcon(.all)
        problemTF.hideIcon(.all)
        
        let data = State.currentPersonalData
        nameTF.textField.text = data?.fullName
        phoneTF.textField.text = data?.phoneNumber.first?.phone
    }
    
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendTapped(_ sender: Any) {
        
        if nameTF.textField.text != "" || phoneTF.textField.text != "" || problemTF.textField.text != "" {
            
            guard let name = nameTF.textField.text else { return }
            guard let contact = phoneTF.textField.text else { return }
            guard let message = problemTextView.text else { return }
            
            ApiManager.shared.sendSupportMail(name: name, contact: contact, message: message) {
                ApiManager.shared.errorAlert(title: "Ваш запрос был успешно отправлен", text: "") {
                    self.navigationController?.popViewController(animated: true)
                }
            }
        } else {
            ApiManager.shared.errorAlert(text: "Нужные поля не заполнены")
        }
        
        
    }
    
    @objc func supportTapped() {
        let phone = "+74733004000"
        if let url = URL(string: "tel://\(phone)"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
    }
}

extension SupportViewController: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "C чем у вас возникли проблемы?"
            textView.textColor = UIColor.lightGray
        }
    }
}
