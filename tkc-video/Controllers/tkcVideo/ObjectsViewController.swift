//
//  ObjectsViewController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 29/6/21.
//

import UIKit
import AlignedCollectionViewFlowLayout

class ObjectsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var orientButton: UIButton!
    
    var objects: [Object] = []
    
    var listImage = UIImage(named: "list")!
    var collectionImage = UIImage(named: "collection")!
    
    var orient: Orient = .list {
        didSet {
            if orient == .list {
                orientButton.setImage(collectionImage, for: .normal)
            } else if orient == .collection {
                orientButton.setImage(listImage, for: .normal)
            }
            UserDefaults.standard.setValue(orient.rawValue, forKey: "orient")
        }
    }
    
    enum Orient: String {
        case list = "list"
        case collection = "collection"
    }
    
    override open var shouldAutorotate: Bool {
        return false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        fetchData()
    }
    
    override func viewDidLoad() {
        if let or = UserDefaults.standard.string(forKey: "orient") {
            let orr = Orient(rawValue: or)
            orient = orr ?? .list
        }
        super.viewDidLoad()
        
        let alignedFlowLayout = AlignedCollectionViewFlowLayout(
            horizontalAlignment: .justified,
            verticalAlignment: .top
        )

        alignedFlowLayout.estimatedItemSize = .zero
        alignedFlowLayout.itemSize = .init(width: 160, height: 130)
        
        collectionView.collectionViewLayout = alignedFlowLayout
        
        collectionView.register(UINib(nibName: "CameraCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cCell")
        tableView.register(UINib(nibName: "CameraTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        collectionView.reloadData()
        tableView.reloadData()
    }
    
    func fetchData() {
        ApiManager.shared.getAllOrganizations { (objects) in
            self.objects = objects
            self.checkOrient()
        }
    }
    
    @IBAction func changeOrientTapped(_ sender: UIButton) {
        switch orient {
        case .list:
            orient = .collection
            break
        case .collection:
            orient = .list
            break
        }
        checkOrient()
    }
    
    private func checkOrient() {
        switch orient {
        case .collection:
            collectionView.isHidden = false
            tableView.isHidden = true
            collectionView.reloadData()
            break
        case .list:
            collectionView.isHidden = true
            tableView.isHidden = false
            tableView.reloadData()
            break
        }
    }
    
    @IBAction func dismissTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func searchTapped(_ sender: UIButton) {
        let nc = storyboard!.instantiateViewController(withIdentifier: "search") as! UINavigationController
        let vc = nc.viewControllers[0] as! SearchTableViewController
        vc.isObj = true
        vc.objects = self.objects
        nc.modalPresentationStyle = .fullScreen
        present(nc, animated: true)
    }
    
}
    
extension ObjectsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return objects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CameraTableViewCell
        
        let object = objects[indexPath.row]
        cell.object = object
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let object = objects[indexPath.row]
        
        let vc = storyboard!.instantiateViewController(withIdentifier: "CamerasViewController") as! CamerasViewController
        vc.title = object.name
        vc.isObj = true
        vc.object = object
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension ObjectsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return objects.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cCell", for: indexPath) as! CameraCollectionViewCell
        
        let object = objects[indexPath.row]
        
        cell.object = object
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let object = objects[indexPath.row]
        
        let vc = storyboard!.instantiateViewController(withIdentifier: "CamerasViewController") as! CamerasViewController
        vc.title = object.name
        vc.isObj = true
        vc.object = object
        
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
}
