//
//  favoritViewController.swift
//  tkc-video
//
//  Created by Алексей Волков on 09.06.2021.
//

import UIKit
import FlussonicSDK
import AlignedCollectionViewFlowLayout

class FavoritesViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var orientButton: UIButton!
    
    override open var shouldAutorotate: Bool {
        return false
    }
    
    var listImage = UIImage(named: "list")!
    var collectionImage = UIImage(named: "collection")!
    
    var orient: Orient = .list {
        didSet {
            if orient == .list {
                orientButton.setImage(collectionImage, for: .normal)
            } else if orient == .collection {
                orientButton.setImage(listImage, for: .normal)
            }
            UserDefaults.standard.setValue(orient.rawValue, forKey: "orient")
        }
    }
    
    enum Orient: String {
        case list = "list"
        case collection = "collection"
    }
    
    var cams: [Camera] = []
    var sortedCameras: [Camera] = []
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        collectionView.register(UINib(nibName: "CameraCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cCell")
        tableView.register(UINib(nibName: "CameraTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        fetchData()
    }
    
    func fetchData() {
        ApiManager.shared.getAllCams { cams in
            self.cams = cams
            self.filterFavorites()
            self.checkOrient()
        }
    }
    
    func filterFavorites() {
        let sortedCams = cams.filter {
            $0.favorite == "1"
        }
        self.sortedCameras = sortedCams
    }
    
    @IBAction func changeOrientTapped(_ sender: UIButton) {
        
        switch orient {
        case .list:
            orient = .collection
            break
        case .collection:
            orient = .list
            break
        }
        checkOrient()
    }
    
    private func checkOrient() {
        switch orient {
        case .collection:
            collectionView.isHidden = false
            tableView.isHidden = true
            collectionView.reloadData()
            break
        case .list:
            collectionView.isHidden = true
            tableView.isHidden = false
            tableView.reloadData()
            break
        }
    }
    
    override func viewDidLoad() {
        if let or = UserDefaults.standard.string(forKey: "orient") {
            let orr = Orient(rawValue: or)
            orient = orr ?? .list
        }
        super.viewDidLoad()
        
        let alignedFlowLayout = AlignedCollectionViewFlowLayout(
            horizontalAlignment: .justified,
            verticalAlignment: .top
        )
        
        alignedFlowLayout.estimatedItemSize = .zero
        alignedFlowLayout.itemSize = .init(width: 160, height: 130)
        
        collectionView.collectionViewLayout = alignedFlowLayout
        
        collectionView.register(UINib(nibName: "CameraCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "cCell")
        tableView.register(UINib(nibName: "CameraTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        collectionView.reloadData()
        tableView.reloadData()
    }
    
    @IBAction func dismissTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func searchTapped(_ sender: UIButton) {
        let nc = storyboard!.instantiateViewController(withIdentifier: "search") as! UINavigationController
        let vc = nc.viewControllers[0] as! SearchTableViewController
        vc.cams = self.sortedCameras
        nc.modalPresentationStyle = .fullScreen
        present(nc, animated: true)
    }
    
}

extension FavoritesViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sortedCameras.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CameraTableViewCell
        
        var camera = sortedCameras[indexPath.row]
        
        if camera.renamename.containsText() {
            camera.name = camera.renamename
        }
        
        cell.camera = camera
        cell.selectionStyle = .none
        cell.delegate = self
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! CameraTableViewCell
        
        let vc = storyboard!.instantiateViewController(withIdentifier: "PlayerViewController") as! PlayerViewController
        vc.url = cell.playerUrl!
        vc.camera = cell.camera!
        
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension FavoritesViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return sortedCameras.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cCell", for: indexPath) as! CameraCollectionViewCell
        
        var camera = sortedCameras[indexPath.row]
        
        if camera.renamename.containsText() {
            camera.name = camera.renamename
        }
        
        cell.camera = camera
        cell.delegate = self
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! CameraCollectionViewCell
        
        let vc = storyboard!.instantiateViewController(withIdentifier: "PlayerViewController") as! PlayerViewController
        vc.url = cell.playerUrl!
        vc.camera = cell.camera!
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
}

extension FavoritesViewController: CameraCellDelegate {
    func favoriteChanged() {
        self.fetchData()
    }
}
