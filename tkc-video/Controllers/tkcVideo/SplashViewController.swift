//
//  SplashViewController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 16/7/21.
//

import UIKit

class SplashViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")
        
        if !launchedBefore  {
            print("First launch, setting UserDefault.")
            UserDefaults.standard.set(true, forKey: "launchedBefore")
            
        }
        
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "mainTabBar") as! UITabBarController
        vc.modalPresentationStyle = .fullScreen
        
        ApiManager.shared.validateSession() { [weak self] in
            let vc = self?.storyboard!.instantiateViewController(withIdentifier: "mainTabBar") as! UITabBarController
            vc.modalPresentationStyle = .fullScreen
            self?.present(vc, animated: true)
        }
        
        
        super.viewWillAppear(animated)
    }
    
}
