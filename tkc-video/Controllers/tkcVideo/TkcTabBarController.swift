//
//  TkcTabBarController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 16/7/21.
//

import UIKit

class TkcTabBarController: UITabBarController {
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if UserDefaults.standard.string(forKey: "session") != nil {
            let objNav = storyboard!.instantiateViewController(withIdentifier: "objNC")
            if viewControllers?.count == 4 {
                viewControllers![2] = objNav
            } else {
                viewControllers!.insert(objNav, at: 2)
            }
            if UserDefaults.standard.bool(forKey: "noObjects") {
                    removeTab(at: 2)
            }
        }
        
    }
    
    func removeTab(at index: Int) {
        var viewControllerS = self.viewControllers
        viewControllerS?.remove(at: index)
        self.viewControllers = viewControllerS
    }
    
    func addTab(at index: Int, vc: UIViewController) {
        var viewControllerS = self.viewControllers
        viewControllerS?.insert(vc, at: index)
        self.viewControllers = viewControllerS
    }
}
