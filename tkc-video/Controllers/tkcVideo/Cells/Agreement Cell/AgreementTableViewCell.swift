//
//  AgreementTableViewCell.swift
//  tkc-video
//
//  Created by Alex Rudoi on 17/9/21.
//

import UIKit

protocol AgreementTableViewCellDelegate {
    func valueChanged(value: Bool)
    func linkTapped(link: String)
}

class AgreementTableViewCell: UITableViewCell, CustomCheckboxDelegate {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var checkbox: CustomCheckbox!
    
    var delegate: AgreementTableViewCellDelegate?
    
    var agr: Agreement! {
        didSet {
            label.text = agr.text
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        checkbox.delegate = self
    }
    
    func configure(agreement: Agreement) {
        self.agr = agreement
    }
    
    @IBAction func agrTapped(_ sender: Any) {
        delegate?.linkTapped(link: agr.link)
    }
    
    func valueChanged() {
        agr.confirmed = !checkbox.isOn
        delegate?.valueChanged(value: agr.confirmed)
    }

}
