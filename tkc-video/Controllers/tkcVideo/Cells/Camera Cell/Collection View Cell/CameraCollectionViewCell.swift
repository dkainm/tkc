//
//  CameraCollectionViewCell.swift
//  tkc-video
//
//  Created by Alex Rudoi on 4/7/21.
//

import UIKit
import FlussonicSDK
import SVProgressHUD

class CameraCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var cameraNameLabel: UILabel!
    @IBOutlet weak var mainView: UIView!
    
    @IBOutlet weak var favoriteIcon: UIButton!
    
    @IBOutlet weak var cameraPreviewView: UIView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var cameraView: PreviewMp4View!
    var cameraUrl: URL?
    var playerUrl: URL?
    var camCache = ""
    
    var delegate: CameraCellDelegate?
    
    public var object: Object? {
        didSet {
            guard let `object` = object else { return }
            
            DispatchQueue.main.async { [weak self] in
                self?.activityIndicator.startAnimating()
                self?.cameraView = PreviewMp4View(frame: self!.cameraPreviewView.frame)
                self?.cameraNameLabel?.text = object.name
                self?.cameraPreviewView.insertSubview(self!.cameraView, at: 0)
                self?.favoriteIcon.isHidden = true
                self?.camCache = "\(object.name)_\(lrint(Date().timeIntervalSince1970/10))"
                self?.cameraUrl = URL(string: "\(object.server)/cam_\(object.id)/preview.mp4?token=\(object.token)")!
                self?.cameraView.configure(withUrl: self!.cameraUrl!, cacheKey: self?.camCache)
                self?.activityIndicator.stopAnimating()
                
                self?.mainView.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.04)
                self?.mainView.layer.shadowOpacity = 1
                self?.mainView.layer.shadowOffset = CGSize(width: 0, height: 4)
                self?.mainView.layer.shadowRadius = 15
                
                self?.contentView.layer.cornerRadius = 8.0
                self?.contentView.layer.masksToBounds = true
                
                self?.layer.cornerRadius = 8.0
                self?.layer.masksToBounds = false
            }
        }
    }
    
    public var camera: Camera? {
        didSet {
            guard let `camera` = camera else { return }
            
            DispatchQueue.main.async { [weak self] in
                
                self?.activityIndicator.startAnimating()
                self?.cameraView = PreviewMp4View(frame: self!.cameraPreviewView.frame)
                self?.cameraNameLabel?.text = camera.name
                if camera.active == "1" {
                    self?.activityIndicator.isHidden = false
                    self?.cameraPreviewView.insertSubview(self!.cameraView, at: 0)
                    self?.camCache = "\(camera.name)_\(lrint(Date().timeIntervalSince1970/10))"
                    self?.cameraUrl = URL(string: "\(camera.server)/cam_\(camera.id)/preview.mp4?token=\(camera.token)")!
                    self?.playerUrl = URL(string: "https://\(UserDefaults.standard.string(forKey:"session")!)@telecom-service.site/cam_\(camera.id)")
                    self?.cameraView.configure(withUrl: self!.cameraUrl!, cacheKey: self?.camCache)
                } else {
                    self?.activityIndicator.isHidden = true
                }
                
                if camera.favorite == "1" {
                    self?.favoriteIcon.setImage(UIImage(named: "star")!, for: .normal)
                } else {
                    self?.favoriteIcon.setImage(UIImage(named: "starEmpty")!, for: .normal)
                }
                
                self?.mainView.layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.04)
                self?.mainView.layer.shadowOpacity = 1
                self?.mainView.layer.shadowOffset = CGSize(width: 0, height: 4)
                self?.mainView.layer.shadowRadius = 15
                
                self?.activityIndicator.stopAnimating()
                
                self?.contentView.layer.cornerRadius = 8.0
                self?.contentView.layer.masksToBounds = true
                
                self?.layer.cornerRadius = 8.0
                self?.layer.masksToBounds = false
            }
        }
    }
    
    
    override func prepareForReuse() {
        cameraView.reset()
    }
    
    func onStatusChanged(_ status: Int8, _ code: String, _ message: String) {
        if status == PreviewMp4StatusEnum.error.rawValue {
            print("onstatusChanged error \(status) \(message)")
        }
    }
    
    @IBAction func favoriteTapped(_ sender: UIButton) {
        var favorite = "0"
        if sender.image(for: .normal) == UIImage(named: "star")! {
            self.favoriteIcon.setImage(#imageLiteral(resourceName: "starEmpty"), for: .normal)
        } else {
            self.favoriteIcon.setImage(#imageLiteral(resourceName: "star"), for: .normal)
            favorite = "1"
        }
        
        ApiManager.shared.changeFavorite(for: camera!.id, to: favorite) {
            self.delegate?.favoriteChanged()
        }
    }
    
}
