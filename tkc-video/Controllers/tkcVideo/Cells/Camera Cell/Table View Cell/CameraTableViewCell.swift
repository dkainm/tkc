//
//  CameraTableViewCell.swift
//  tkc-video
//
//  Created by Alex Rudoi on 4/7/21.
//

import UIKit
import FlussonicSDK
import SVProgressHUD

protocol CameraCellDelegate {
    func favoriteChanged()
}

class CameraTableViewCell: UITableViewCell, PreviewMp4ViewStatusListener {
    
    @IBOutlet var cameraNameLabel: UILabel!
    
    @IBOutlet weak var favoriteIcon: UIButton!
    
    @IBOutlet weak var cameraPreviewView: UIView!
    @IBOutlet weak var grayView: UIView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var delegate: CameraCellDelegate?
    
    var cameraView: PreviewMp4View!
    var cameraUrl: URL?
    var playerUrl: URL?
    var camCache = ""
    
    public var object: Object? {
        didSet {
            guard let `object` = object else { return }
            
            DispatchQueue.main.async { [weak self] in
                self?.activityIndicator.startAnimating()
                self?.cameraView = PreviewMp4View(frame: self!.cameraPreviewView.frame)
                self?.cameraNameLabel?.text = object.name
                self?.favoriteIcon.isHidden = true
                self?.cameraPreviewView.addSubview(self!.cameraView)
                self?.camCache = "\(object.id)_\(lrint(Date().timeIntervalSince1970/10))"
                self?.cameraUrl = URL(string: "\(object.server)/cam_\(object.id)/preview.mp4?token=\(object.token)")!
                self?.cameraView.configure(withUrl: self!.cameraUrl!, cacheKey: self?.camCache)
                self?.activityIndicator.stopAnimating()
                
                self?.grayView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
                self?.grayView.layer.cornerRadius = 8
                
                self?.cameraPreviewView.layer.cornerRadius = 8
                self?.cameraPreviewView.layer.masksToBounds = true
            }
        }
    }
    
    public var camera: Camera? {
        didSet {
            guard let `camera` = camera else { return }
            
            DispatchQueue.main.async { [weak self] in
                
                self?.activityIndicator.startAnimating()
                self?.cameraView = PreviewMp4View(frame: self!.cameraPreviewView.frame)
                self?.cameraNameLabel?.text = camera.name
                if camera.active == "1" {
                    self?.activityIndicator.isHidden = false
                    self?.cameraPreviewView.addSubview(self!.cameraView)
                    self?.camCache = "\(camera.id)_\(lrint(Date().timeIntervalSince1970/10))"
                    self?.cameraUrl = URL(string: "\(camera.server)/cam_\(camera.id)/preview.mp4?token=\(camera.token)")!
                    self?.playerUrl = URL(string: "https://\(UserDefaults.standard.string(forKey:"session")!)@telecom-service.site/cam_\(camera.id)")
                    self?.cameraView.configure(withUrl: self!.cameraUrl!, cacheKey: self?.camCache)
                } else {
                    self?.activityIndicator.isHidden = true
                }
                
                if camera.favorite == "1" {
                    self?.favoriteIcon.setImage(UIImage(named: "star")!, for: .normal)
                } else {
                    self?.favoriteIcon.setImage(UIImage(named: "starEmpty")!, for: .normal)
                }
                
                self?.activityIndicator.stopAnimating()
                
                self?.grayView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
                self?.grayView.layer.cornerRadius = 8
                
                self?.cameraPreviewView.layer.cornerRadius = 8
                self?.cameraPreviewView.layer.masksToBounds = true
            }
        }
    }
    
    override func prepareForReuse() {
        cameraView.reset()
    }
    
    func onStatusChanged(_ status: Int8, _ code: String, _ message: String) {
        if status == PreviewMp4StatusEnum.error.rawValue {
            print("onstatusChanged error \(status) \(message)")
        }
    }
    @IBAction func favoriteTapped(_ sender: UIButton) {
        var favorite = "0"
        if sender.image(for: .normal) == UIImage(named: "star")! {
            self.favoriteIcon.setImage(#imageLiteral(resourceName: "starEmpty"), for: .normal)
        } else {
            self.favoriteIcon.setImage(#imageLiteral(resourceName: "star"), for: .normal)
            favorite = "1"
        }
        
        ApiManager.shared.changeFavorite(for: camera!.id, to: favorite) {
            self.delegate?.favoriteChanged()
        }
    }
    
}

