//
//  SearchTableViewController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 4/7/21.
//

import UIKit

class SearchTableViewController: UITableViewController {
    
    var cams: [Camera]!
    var objects: [Object]!
    var filteredCams: [Camera] = []
    var filteredObjects: [Object] = []
    
    let searchController = UISearchController(searchResultsController: nil)
    
    var isSearchBarEmpty: Bool {
        return searchController.searchBar.text?.isEmpty ?? true
    }
    var isFiltering: Bool {
        return searchController.isActive && !isSearchBarEmpty
    }
    
    override open var shouldAutorotate: Bool {
        return false
    }
    
    var isObj = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchController.title = "Поиск"
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Поиск"
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        if isObj {
            let object: Object
            if isFiltering {
                object = filteredObjects[indexPath.row]
            } else {
                object = objects[indexPath.row]
            }
            cell.textLabel?.text = object.name
            return cell
        } else {
            let camera: Camera
            if isFiltering {
                camera = filteredCams[indexPath.row]
            } else {
                camera = cams[indexPath.row]
            }
            cell.textLabel?.text = camera.name
            return cell
        }
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isObj {
            if isFiltering {
                return filteredObjects.count
            }
            return objects.count
        } else {
            if isFiltering {
                return filteredCams.count
            }
            
            return cams.count
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if isObj {
            let object: Object
            if isFiltering {
                object = filteredObjects[indexPath.row]
            } else {
                object = objects[indexPath.row]
            }
            
            let vc = storyboard!.instantiateViewController(withIdentifier: "CamerasViewController") as! CamerasViewController
            vc.title = object.name
            vc.isObj = true
            vc.isSearch = true
            vc.object = object
            
            navigationController?.pushViewController(vc, animated: true)
        } else {
            let camera: Camera
            if isFiltering {
                camera = filteredCams[indexPath.row]
            } else {
                camera = cams[indexPath.row]
            }
            
            let vc = storyboard!.instantiateViewController(withIdentifier: "PlayerViewController") as! PlayerViewController
            vc.url = URL(string: "http://\(UserDefaults.standard.string(forKey:"session")!)@xn--b1agd0aean.xn--80asehdb/cam_\(camera.id)")
            vc.camera = camera
            
            navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
    func filterContentForSearchText(_ searchText: String,
                                    camera: Camera? = nil, object: Object? = nil) {
        if isObj {
            filteredObjects = objects.filter { (object: Object) -> Bool in
                return object.name.lowercased().contains(searchText.lowercased())
            }
        } else {
            
            filteredCams = cams.filter { (camera: Camera) -> Bool in
                return camera.name.lowercased().contains(searchText.lowercased())
            }
            
        }
        tableView.reloadData()
    }
    
    @IBAction func cancelTapped(_ sender: Any) {
        self.dismiss(animated: true)
    }
}

extension SearchTableViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        filterContentForSearchText(searchBar.text!)
    }
}
