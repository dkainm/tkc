//
//  RegistrationViewController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 1/6/21.
//

import UIKit

public protocol RegistrationDelegate {
    func updateAppData()
}

class RegistrationViewController: UIViewController {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var loginTF: UITextField!
    @IBOutlet weak var passTF: UITextField!
    @IBOutlet weak var infoLabel: UILabel!
    
    var delegate: RegistrationDelegate?
    
    var phone = ""
    var isNotStart = false
    
    var restoreType = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.setBackground(#imageLiteral(resourceName: "redBg"))
        
        mainView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        mainView.layer.cornerRadius = 15
        
        loginTF.attributedPlaceholder = NSAttributedString(string: "Введите лицевой счет",
                                                                     attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.7568627451, green: 0.7960784314, blue: 0.8352941176, alpha: 1)])
        passTF.attributedPlaceholder = NSAttributedString(string: "Введите пароль",
                                                                    attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.7568627451, green: 0.7960784314, blue: 0.8352941176, alpha: 1)])
        if #available(iOS 11.0, *) {
            loginTF.textContentType = .username
            passTF.textContentType = .password
        }
        passTF.isSecureTextEntry = true
        
        let attrs1 = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 9), NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.2784313725, green: 0.2784313725, blue: 0.2784313725, alpha: 1)] as [NSAttributedString.Key : Any]
        let attrs2 = [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 9), NSAttributedString.Key.foregroundColor : UIColor(named: "primary")!] as [NSAttributedString.Key : Any]
        
        let attributedString1 = NSMutableAttributedString(string:"Нажимая «Войти», вы принимаете ", attributes:attrs1)
        let attributedString2 = NSMutableAttributedString(string:"пользовательское соглашение", attributes: attrs2)
        
        attributedString1.append(attributedString2)
        infoLabel.attributedText = attributedString1
        
        infoLabel.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(infoTapped)))
    }
    
    override open var shouldAutorotate: Bool {
        return false
    }
    
    private func restorePass() {
        setPhoneAlert()
    }
    
    private func setPhoneAlert() {
        var titl = ""
        if restoreType == 0 {
            titl = "Восстановление пароля"
        } else {
            titl = "Получение лицевого счета"
        }
        let alert = UIAlertController(title: titl, message: "Введите номер телефона привязанный  к договору", preferredStyle: .alert)
        alert.view.tintColor = UIColor(named: "primary")!
        
        alert.addTextField { (textField : UITextField!) -> Void in
            textField.keyboardType = .phonePad
            textField.textContentType = .telephoneNumber
            textField.text = "+7"
            textField.delegate = phoneInputController
        }
        
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Отправить СМС", style: .default, handler: { [weak self] (_) in
            guard let phoneUnwrapped = alert.textFields?.first?.text,
                  let phone = phoneFormatter.unformat(phoneUnwrapped),
                  ("+7" + phone).containsPhone() else {
                showWarningView(text: "Некорректное значение",
                                info: "Вы ввели некорректный номер телефона") {
                    self?.setPhoneAlert()
                }
                  return
              }
            self?.checkCode(phone: phone)
        }))
        
        present(alert, animated: true)
    }
    
    private func checkCode(phone: String) {
        ApiManager.shared.restorePassWithPhone(phone: phone) { [weak self] (isSuccess) in
            if isSuccess {
                self?.sendCodeAlert()
            } else {
                let alert = UIAlertController(title: "Попробуйте использовать другой номер, или обратитесь в службу технической поддержки", message: nil, preferredStyle: .alert)
                alert.view.tintColor = UIColor(named: "primary")!
                alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
                alert.addAction(UIAlertAction(title: "Попробовать еще раз", style: .default, handler: { (_) in
                    self?.setPhoneAlert()
                }))
                alert.addAction(UIAlertAction(title: "+7 (473) 300-40-00", style: .default, handler: { (_) in
                    let phone = "+74733004000"
                    if let url = URL(string: "tel://\(phone)"), UIApplication.shared.canOpenURL(url) {
                        UIApplication.shared.open(url)
                    }
                }))
                self?.present(alert, animated: true)
            }
        }
    }
    
    private func sendCodeAlert() {
        let alert = UIAlertController(title: "Введите код из СМС", message: nil, preferredStyle: .alert)
        alert.view.tintColor = UIColor(named: "primary")!
        
        alert.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Код"
            if #available(iOS 12.0, *) {
                textField.textContentType = .oneTimeCode
            }
        }
        
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Подтвердить", style: .default, handler: { (_) in
            guard let code = alert.textFields?.first?.text else { return }
            ApiManager.shared.getContracts(phone: State.phoneToRestore!, code: code) { [weak self] (smsCorrect, phoneExists, contracts) in
                
                if smsCorrect && phoneExists {
                    let contractsController = UIStoryboard(name: "Popups", bundle: nil).instantiateViewController(withIdentifier: "ContractsViewController") as! ContractsViewController
                    contractsController.contracts = contracts
                    contractsController.isDissolving()
                    self?.present(contractsController, animated: true)
                } else if !smsCorrect {
                    ApiManager.shared.wrongCode {
                        self?.sendCodeAlert()
                    }
                } else if !phoneExists {
                    let alert = UIAlertController(title: "Попробуйте использовать другой номер, или обратитесь в службу технической поддержки", message: nil, preferredStyle: .alert)
                    alert.view.tintColor = UIColor(named: "primary")!
                    alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
                    alert.addAction(UIAlertAction(title: "Попробовать еще раз", style: .default, handler: { (_) in
                        self?.setPhoneAlert()
                    }))
                    alert.addAction(UIAlertAction(title: "+7 (473) 300-40-00", style: .default, handler: { (_) in
                        let phone = "+74733004000"
                        if let url = URL(string: "tel://\(phone)"), UIApplication.shared.canOpenURL(url) {
                            UIApplication.shared.open(url)
                        }
                    }))
                    self?.present(alert, animated: true)
                }
            }
        }))
        
        present(alert, animated: true)
    }
    
    @objc func infoTapped() {
        let webView = storyboard!.instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
        webView.url = URL(string: "https://telecom-service.site/faq/")
        present(webView, animated: true)
    }
    
    @IBAction func questionTapped(_ sender: Any) {
        let webView = storyboard!.instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
        webView.url = URL(string: "https://telecom-service.site/policy/")
        present(webView, animated: true)
    }
    
    @IBAction func registration(_ sender: Any) {
        openUrl(urlStr: "https://xn--b1agd0aean.xn--80asehdb/registration/")
    }
    
    @IBAction func forgetPass(_ sender: Any) {
        restoreType = (sender as! UIButton).tag
        restorePass()
    }
    
    @IBAction func demoButton(_ sender: Any) {
        ApiManager.shared.logIn(login: "androiddev1",
                                password: "androiddev1") { (session) in
            
            UserDefaults.standard.set(session.session, forKey: "session")
            
            ApiManager.shared.getAllOrganizations { (objects) in
                if objects.count == 0 {
                    UserDefaults.standard.set(true, forKey: "noObjects")
                } else {
                    UserDefaults.standard.set(false, forKey: "noObjects")
                }
                if self.isNotStart {
                    self.dismiss(animated: true)
                } else {
                    let vc = self.storyboard!.instantiateViewController(withIdentifier: "tabBar") as! TkcTabBarController
                    vc.modalPresentationStyle = .fullScreen
                    self.present(vc, animated: true)
                }
            }
        }
        
    }
    
    
    @IBAction func nextTapped(_ sender: UIButton) {
        
        if loginTF.text!.containsText() && passTF.text!.containsText() {
            ApiManager.shared.authorizate(login: loginTF.text!, pass: passTF.text!) { [weak self] in
                
                KeychainManager.savePassword(
                    service: "TKC",
                    account: self!.loginTF.text!,
                    data: self!.passTF.text!
                )
                
                ApiManager.shared.registerTokenOnServer(token: State.fcmToken ?? "") { success in
                    print("Send token: success - ", success)
                    
                    if self!.isNotStart {
                        self?.delegate?.updateAppData()
                        self?.dismiss(animated: true)
                    } else {
                        let vc = self?.storyboard!.instantiateViewController(withIdentifier: "mainTabBar") as! UITabBarController
                        vc.modalPresentationStyle = .fullScreen
                        self?.present(vc, animated: true)
                    }
                }
                
            }
        }
        
    }
    
    func openUrl(urlStr:String!) {
        
        if let url = URL(string: urlStr) {
            UIApplication.shared.open(url)
        }
    }
    
    
}
