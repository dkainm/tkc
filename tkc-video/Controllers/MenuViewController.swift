//
//  MenuViewController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 20/9/21.
//

import UIKit
import SVProgressHUD

class MenuViewController: UIViewController {

    @IBOutlet var tabs: [UIView]?
    
    @IBOutlet weak var personalTab: UIView!
    @IBOutlet weak var videoTab: UIView!
    @IBOutlet weak var tvTab: UIView!
    
    @IBOutlet weak var notificationsView: UIView!
    @IBOutlet weak var badgeView: UIView!
    @IBOutlet weak var badgeLabel: UILabel!
    
    @IBOutlet weak var logOutStack: UIStackView!
    @IBOutlet weak var supportButton: CustomButton!
    
    @IBOutlet weak var advirtismentView: UIView!
    
    var update = true
    var isVideoEnabled = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        supportButton.leftImage = UIImage(named: "setIcon")!
        supportButton.tintAdjustmentMode = .normal
        supportButton.hideIcon(.right)
        
        badgeView.layer.borderWidth = 1.5
        badgeView.layer.borderColor = UIColor.white.cgColor
        
        notificationsView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(notificationsTapped)))
        
        NotificationCenter.default.addObserver(self, selector: #selector(badgeUpdate), name: NSNotification.Name("badgeUpdate"), object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if update {
            fetchData()
            update = false
        }
        badgeUpdate()
    }
    
    private func fetchData() {
        
        ApiManager.shared.isVideoEnabled() { [weak self] result in
            State.isVideoEnabled = result
            self?.isVideoEnabled = result
            
            if let message = State.serviceMessage,
               message.activate {
                let alert = UIAlertController(
                    title: message.message,
                    message: nil,
                    preferredStyle: .alert
                )
                alert.view.tintColor = UIColor(named: "primary")!
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: { (_) in
                    ApiManager.shared.confirmReadDoc(id: message.id!) {
                        alert.dismiss(animated: true)
                    }
                }))
                self?.present(alert, animated: true)
            }
            
            if !State.isVideoEnabled,
               self?.tabBarController?.viewControllers?.count == 5 {
                self?.tabBarController?.viewControllers?.remove(at: 2)
            } else if self?.tabBarController?.viewControllers?.count == 4,
                          State.isVideoEnabled {
                let video = self?.storyboard!.instantiateViewController(withIdentifier: "videoNav") as! UINavigationController
                    video.tabBarItem = UITabBarItem(title: "Камеры", image: UIImage(named: "cameraIconTB"), tag: 2)
                self?.tabBarController?.viewControllers?.insert(video, at: 2)
                }
                
            self?.setUpViews()
            
        }
    }
    
    @objc private func notificationsTapped() {
        notificationsView.showAnimation() { [weak self] in
            guard let `self` = self else { return }
            let notifications = self.storyboard?.instantiateViewController(withIdentifier: "NotificationsViewController") as! NotificationsViewController
            self.present(notifications, animated: true)
        }
    }
    
    @objc private func badgeUpdate() {
        let badgeCount = UIApplication.shared.applicationIconBadgeNumber
        badgeView.isHidden = (badgeCount == 0)
        badgeLabel.text = (badgeCount < 99) ? "\(badgeCount)" : "99+"
    }
    
    @IBAction func supportTapped(_ sender: UIButton) {
        tabBarController?.selectedIndex = (tabBarController?.viewControllers!.count)! - 1
    }
    
    @objc func personalTabTapped() {
        personalTab.showAnimation { [weak self] in
            self?.tabBarController?.selectedIndex = 1
        }
    }
    
    @objc func advirtismentTapped() {
        advirtismentView.showAnimation() { [weak self] in
            let advertismentController = self?.storyboard?.instantiateViewController(withIdentifier: "AdvertismentViewController") as! AdvertismentViewController
            self?.navigationController?.pushViewController(advertismentController, animated: true)
        }
    }
    
    @objc func videoTabTapped() {
        
        if isVideoEnabled {
            videoTab.showAnimation { [weak self] in
                self?.tabBarController?.selectedIndex = 2
            }
        } else if State.privateHouse {
            let alert = UIAlertController(title: "Подключение видеонаблюдения", message: nil, preferredStyle: .alert)
            alert.view.tintColor = UIColor(named: "primary")!
            alert.addAction(UIAlertAction(title: "Подробнее об услуге", style: .default, handler: { [weak self] (_) in
                let webView = self?.storyboard!.instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
                webView.url = URL(string: "http://videovdom.xn--q1ab.xn--p1ai/")
                self?.present(webView, animated: true)
            }))
            alert.addAction(UIAlertAction(title: "Оставить заявку", style: .default, handler: { [weak self] (_) in
                let connectController = UIStoryboard(name: "Popups", bundle: nil).instantiateViewController(withIdentifier: "VideoConnectPopupViewController") as! VideoConnectPopupViewController
                connectController.isDissolving()
                self?.present(connectController, animated: true)
            }))
            alert.addAction(UIAlertAction(title: "Отмена", style: .cancel))
            present(alert, animated: true)
        } else {
            let webView = storyboard!.instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
            webView.url = URL(string: "http://videovdommkd.xn--q1ab.xn--p1ai/")
            present(webView, animated: true)
        }
    }
    
    @objc func tvTabTapped() {
        tvTab.showAnimation { [weak self] in
            if State.isVideoEnabled {
                self?.tabBarController?.selectedIndex = 3
            } else {
                self?.tabBarController?.selectedIndex = 2
            }
        }
    }
    
    @objc func logOutTapped() {
        logOutStack.showAnimation { [weak self] in
            let alert = UIAlertController(title: "Вы уверены что хотите выйти с учетной записи?", message: nil, preferredStyle: .alert)
            alert.view.tintColor = UIColor(named: "primary")
            alert.addAction(UIAlertAction(title: "Назад", style: .cancel))
            alert.addAction(UIAlertAction(title: "Выйти", style: .default, handler: { (_) in
                let rDelegate = self?.tabBarController as! MainTabBarController
                endSession(registrationDelegate: rDelegate)
            }))
            self?.present(alert, animated: true)
        }
    }
    
    func setUpViews() {
        setGestures()
        setShadows()
        setGradientBackground(tab: personalTab, active: true)
        setGradientBackground(tab: videoTab, active: isVideoEnabled)
        setGradientBackground(tab: tvTab, active: true)
    }
    
    private func setGradientBackground(tab: UIView, active: Bool) {
        
        tab.clipsToBounds = true
        tab.layer.cornerRadius = 16
        
        let colorTop: CGColor = active ? #colorLiteral(red: 0.8698514104, green: 0.1220991984, blue: 0, alpha: 1) : #colorLiteral(red: 0.831372549, green: 0.831372549, blue: 0.831372549, alpha: 1)
        let colorBottom: CGColor = active ? #colorLiteral(red: 0.5881579518, green: 0.06815788895, blue: 0, alpha: 1) : #colorLiteral(red: 0.5333333333, green: 0.5333333333, blue: 0.5333333333, alpha: 1)
                    
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.frame = tab.bounds
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 1)
            
        tab.backgroundColor = .clear
        tab.layer.addSublayer(gradientLayer)
    }
    
    private func setGestures() {
        personalTab.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(personalTabTapped)))
        videoTab.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(videoTabTapped)))
        tvTab.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tvTabTapped)))
        
        logOutStack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(logOutTapped)))
        
        advirtismentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(advirtismentTapped)))
    }
    
    private func setShadows() {
        tabs?.forEach({ (tab) in
            tab.isHidden = false
            tab.layer.shadowColor = UIColor.black.cgColor
            tab.layer.shadowOffset = CGSize(width: 0, height: 4)
            tab.layer.shadowRadius = 4
            tab.layer.shadowOpacity = 0.25
        })
        advirtismentView.layer.shadowColor = UIColor.black.cgColor
        advirtismentView.layer.shadowOffset = CGSize(width: 0, height: 4)
        advirtismentView.layer.shadowRadius = 4
        advirtismentView.layer.shadowOpacity = 0.25
    }

}
