//
//  NotificationsViewController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 25/2/22.
//

import UIKit

class NotificationsViewController: UIViewController {

    @IBOutlet weak var noNotsLabel: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    
    var messages = [FBNotification]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "NotificationTableViewCell", bundle: nil), forCellReuseIdentifier: "NotificationTableViewCell")
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 126
        
        noNotsLabel.isHidden = true
        
        LoadingService.shared.show()
        
        ApiManager.shared.getAllServerMessages { (nots) in
            
            NotificationManager.shared.fetchMessages { [weak self] (_, notifications) in
                guard var messages = notifications else { return }
                
                messages.reverse()
                
                if let nots = nots {
                    for not in nots {
                        if !messages.contains(where: {$0.body == not.body}) {
                            NotificationManager.shared.saveMessage(notification: not)
                            messages.append(not)
                        }
                    }
                }
                
                messages.sort(by: {$0.date ?? Date() < $1.date ?? Date()})
                
                self?.messages = messages.reversed()
                self?.tableView.reloadData()
                self?.noNotsLabel.isHidden = !messages.isEmpty
                
                LoadingService.shared.dismiss()
            }
            
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationManager.shared.readMessages(messages)
    }

    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationManager.shared.readMessages(messages)
    }
    
    @IBAction func backTapped(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name("badgeUpdate"), object: nil)
        dismiss(animated: true)
    }
}

extension NotificationsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell", for: indexPath) as! NotificationTableViewCell
        
        let message = messages[indexPath.row]
        
        let date = (message.date ?? Date()).timeIntervalSince1970.dateString()
        
        cell.configure(message, date: date, parent: tableView)
        cell.selectionStyle = .none
        
        if let isRead = message.isRead, isRead { cell.alpha = 0.5 }
        
        return cell
    }
}
