//
//  PersonalDataPopupViewController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 20/9/21.
//

import UIKit

protocol PersonalDataPopupDelegate {
    func didDismiss(isChangedData: Bool)
}

class PersonalDataPopupViewController: PopUpViewController {
    
    @IBOutlet weak var nameStack: UIStackView!
    @IBOutlet weak var contractStack: UIStackView!
    @IBOutlet weak var loginStack: UIStackView!
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var adressLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var contractLabel: UILabel!
    @IBOutlet weak var loginLabel: UILabel!
    @IBOutlet weak var contractIsActiveLabel: UILabel!
    @IBOutlet weak var logOutStack: UIStackView!
    @IBOutlet weak var dismissView: UIView!
    
    var personalData: PersonalData!
    
    var delegate: PersonalDataPopupDelegate?
    
    var isChangedData = false
    
    var name: String {
        let data = personalData.fullName
        if data == "" {
            return "Пусто"
        } else {
            return data
        }
    }
    var address: String {
        let data = personalData.address
        if data == "" {
            return "Пусто"
        } else {
            return data
        }
    }
    var numberContact: String {
        let data = personalData.numberContact
        if data == "" {
            return "Пусто"
        } else {
            return data
        }
    }
    var login: String {
        let data = personalData.login
        if data == "" {
            return "Пусто"
        } else {
            return data
        }
    }
    var contractActivity: String {
        let data = personalData.contractActivity
        if data == "" {
            return "Пусто"
        } else {
            return data
        }
    }
    var phone: String {
        let data = phoneDataFormatter.format(personalData.phoneNumber.first?.phone)
        if data == nil ||
           data == "" {
            return "Пусто"
        } else {
            return data!
        }
    }
    var email: String {
        let data = personalData.email
        if data == nil ||
           data == "" {
            return "Пусто"
        } else {
            return data
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if personalData.interfaceType == 2 {
            nameStack.isHidden = true
            contractStack.isHidden = true
            loginStack.isHidden = true
        }
        
        nameLabel.text = name
        adressLabel.text = address
        contractLabel.text = numberContact
        loginLabel.text = login
        contractIsActiveLabel.text = contractActivity
        phoneLabel.text = phone
        emailLabel.text = email
        
        mainView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        mainView.layer.cornerRadius = 15
        
        logOutStack.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(logOutTapped)))
        dismissView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissTapped)))
    }
    
    @objc private func logOutTapped() {
        logOutStack.showAnimation { [weak self] in
            let alert = UIAlertController(title: "Вы уверены что хотите выйти с учетной записи?", message: nil, preferredStyle: .alert)
            alert.view.tintColor = UIColor(named: "primary")
            alert.addAction(UIAlertAction(title: "Назад", style: .cancel))
            alert.addAction(UIAlertAction(title: "Выйти", style: .default, handler: { (_) in
                let rDelegate = self?.presentingViewController?.tabBarController as! MainTabBarController
                endSession(registrationDelegate: rDelegate)
            }))
            self?.present(alert, animated: true)
        }
    }
    
    @objc private func dismissTapped() {
        dismiss(animated: true) { [unowned self] in
            self.delegate?.didDismiss(isChangedData: self.isChangedData)
        }
    }
    
    @IBAction func editTapped(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            editPhoneNumber()
        case 1:
            editEmail()
        default: break
        }
    }
    
    private func editPhoneNumber() {
        dismiss(animated: true)
        let alert = UIAlertController(
            title: "Изменение номера телефона",
            message: "Введите новый номер телефона",
            preferredStyle: .alert
        )
        alert.view.tintColor = UIColor(named: "primary")!
        alert.addTextField { (textField) in
            textField.text = "+7"
            textField.keyboardType = .phonePad
            textField.textContentType = .telephoneNumber
            textField.delegate = phoneInputController
        }
        alert.addAction(
            UIAlertAction(
                title: "Отмена",
                style: .cancel
            )
        )
        alert.addAction(
            UIAlertAction(
                title: "Изменить",
                style: .default,
                handler: { (_) in
                    guard let phoneUnwrapped = alert.textFields?.first?.text,
                          let phone = phoneFormatter.unformat(phoneUnwrapped),
                          ("+7" + phone).containsPhone() else {
                        showWarningView(text: "Некорректное значение",
                                        info: "Вы ввели некорректный номер телефона") {
                            ApiManager.shared.activeView().present(alert, animated: true)
                        }
                        return
                    }
                    ApiManager.shared.changePhone(
                        to: phone) { [weak self] (success) in
                        if success {
                            self?.isChangedData = true
                            let sAlert = UIAlertController(
                                title: "Номер телефона успешно изменен",
                                message: nil,
                                preferredStyle: .alert
                            )
                            sAlert.view.tintColor = UIColor(named: "primary")!
                            sAlert.addAction(
                                UIAlertAction(
                                    title: "OK",
                                    style: .default
                                ) { _ in
                                    self?.dismiss(animated: true) {
                                        self?.delegate?.didDismiss(isChangedData: self!.isChangedData)
                                    }
                                }
                            )
                            ApiManager.shared.activeView().present(sAlert, animated: true)
                        } else {
                            ApiManager.shared.showFailure()
                        }
                    }
                }
            )
        )
        ApiManager.shared.activeView().present(alert, animated: true)
    }
    
    private func editEmail() {
        dismiss(animated: true)
        let alert = UIAlertController(
            title: "Изменение электроной почты",
            message: "Введите новый Email",
            preferredStyle: .alert
        )
        alert.view.tintColor = UIColor(named: "primary")!
        alert.addTextField { (textField) in
            textField.keyboardType = .emailAddress
            textField.textContentType = .emailAddress
        }
        alert.addAction(
            UIAlertAction(
                title: "Отмена",
                style: .cancel
            )
        )
        alert.addAction(
            UIAlertAction(
                title: "Изменить",
                style: .default,
                handler: { (_) in
                    guard let email = alert.textFields?.first?.text,
                          email.containsEmail() else {
                        showWarningView(text: "Некорректное значение",
                                        info: "Вы ввели некорректный Email") {
                            ApiManager.shared.activeView().present(alert, animated: true)
                        }
                        return
                    }
                    
                    ApiManager.shared.changeEmail(to: email) { [weak self] (success) in
                        if success {
                            self?.isChangedData = true
                            let sAlert = UIAlertController(
                                title: "Email успешно изменен",
                                message: nil,
                                preferredStyle: .alert
                            )
                            sAlert.view.tintColor = UIColor(named: "primary")!
                            sAlert.addAction(
                                UIAlertAction(
                                    title: "OK",
                                    style: .default
                                ) { _ in
                                    self?.dismiss(animated: true) { 
                                        self?.delegate?.didDismiss(isChangedData: self!.isChangedData)
                                    }
                                }
                            )
                            ApiManager.shared.activeView().present(sAlert, animated: true)
                        } else {
                            ApiManager.shared.showFailure()
                        }
                    }
                }
            )
        )
        ApiManager.shared.activeView().present(alert, animated: true)
    }
}


