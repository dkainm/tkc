//
//  VideoConnectPopupViewController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 26/9/21.
//

import UIKit
import SVProgressHUD

class VideoConnectPopupViewController: PopUpViewController {

    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var phoneTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var commentTF: UITextField!
    @IBOutlet weak var camerasCountTF: UITextField!
    
    @IBOutlet weak var rightArrowView: UIImageView!
    @IBOutlet weak var leftArrowView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let personal = State.currentPersonalData {
            nameTF.text = personal.fullName
            if let phone = personal.phoneNumber.first?.phone.tail(from: 0) {
                phoneTF.text = phoneDataFormatter.format(phone)
            }
        } else {
//            phoneTF.attributedPlaceholder = phoneAttributedPlaceholder
            phoneTF.text = "+7"
        }
        
        phoneTF.delegate = phoneInputController
        phoneTF.keyboardType = .phonePad
        phoneTF.textContentType = .telephoneNumber
        
        let data = State.currentPersonalData
        nameTF.text = data?.fullName
        phoneTF.text = data?.phoneNumber.first?.phone
        emailTF.text = data?.email
        
        rightArrowView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(rightArrowTapped)))
        leftArrowView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(leftArrowTapped)))
        
        updateCameraCounter()
    }

    @IBAction func cancelTapped(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBAction func nextTapped(_ sender: Any) {
        guard let phoneUnwrapped = phoneTF.text,
              let phone = phoneFormatter.unformat(phoneUnwrapped),
              ("+7" + phone).containsPhone() else {
            showWarningView(
                text: "Некорректное значение",
                info: "Вы ввели некорректный номер телефона"
            )
            return
        }
        
        if nameTF.text!.containsText(),
           emailTF.text!.containsText(),
           camerasCountTF.text!.containsText(),
           camerasCountTF.text != "0" {
            SVProgressHUD.show()
            
            ApiManager.shared.sendSupport(
                name: nameTF.text!,
                email: emailTF.text!,
                phone: phoneTF.text!,
                message: (commentTF.text ?? "") + " Кол-во видеокамер: \(camerasCountTF.text!)",
                subject: .videosales
            ) { [weak self] (info) in
                SVProgressHUD.dismiss()
                ApiManager.shared.errorAlert(title: info, text: "") {
                    self?.dismiss(animated: true)
                }
            }
        } else {
            var info = ""
            if !nameTF.text!.containsText() {
                info = "Поле «Номер телефона» должно быть заполнено"
            }
            if !emailTF.text!.containsText() {
                info = "Поле «Email» должно быть заполнено"
            }
            if !camerasCountTF.text!.containsText() || camerasCountTF.text == "0" {
                info = "Поле «Количество камер» должно быть заполнено"
            }
            showWarningView(
                text: "Некорректное значение",
                info: info
            )
          }
    }
    
    private func updateCameraCounter() {
        guard let cameraCount = Int(camerasCountTF.text ?? ""),
              let leftArrow = leftArrowView else { return }
        
        let oneCamSelected = cameraCount == 1
        leftArrow.isUserInteractionEnabled = !oneCamSelected
        leftArrow.image = leftArrow.image?.withColor(oneCamSelected ? .gray : .primary)
    }
    
    @objc private func rightArrowTapped() {
        rightArrowView.showAnimation() { [weak self] in
            guard let `self` = self, let currentNumber = Int(self.camerasCountTF.text ?? "") else { return }
            self.camerasCountTF.text = "\(currentNumber + 1)"
            self.updateCameraCounter()
        }
    }
    
    @objc private func leftArrowTapped() {
        leftArrowView.showAnimation { [weak self] in
            guard let `self` = self, let currentNumber = Int(self.camerasCountTF.text ?? "") else { return }
            self.camerasCountTF.text = "\(currentNumber - 1)"
            self.updateCameraCounter()
        }
    }
    
}
