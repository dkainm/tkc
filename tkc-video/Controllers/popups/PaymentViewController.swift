//
//  PaymentViewController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 23/9/21.
//

import UIKit

class PaymentViewController: PopUpViewController {
    
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var loginTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    
    @IBOutlet weak var toPayTF: UITextField!
    
    var personal: PersonalData!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpViews()
    }
    
    private func setUpViews() {
        nameTF.text = personal.fullName
        loginTF.text = personal.login
        emailTF.text = personal.email
        toPayTF.text = "\(personal.toPay)"
    }
    
    @IBAction func nextTapped(_ sender: UIButton) {
        guard nameTF.text!.containsText(),
              loginTF.text!.containsText(),
              toPayTF.text!.containsText(),
              toPayTF.text != "0" else {
            
            var info = ""
            if !nameTF.text!.containsText() {
                info = "Поле «ФИО» должно быть заполнено"
            }
            if !loginTF.text!.containsText() {
                info = "Поле «Лицевой счет» должно быть заполнено"
            }
            if !toPayTF.text!.containsText() || toPayTF.text == "0" {
                info = "Поле «Сумма к оплате» должно быть заполнено и не равно нулю"
            }
            
            showWarningView(text: "Некорректное значение",
                            info: info)
            return
        }
        
        ApiManager.shared.payBills(name: nameTF.text!,
                                   login: loginTF.text!,
                                   email: emailTF.text ?? "",
                                   toPay: toPayTF.text!) { [weak self] (paymentUrl) in
            if let url = URL(string: paymentUrl) {
                self?.dismiss(animated: true)
                let webController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WebViewViewController") as! WebViewViewController
                webController.url = url
                ((self?.presentingViewController) as! UINavigationController).pushViewController(webController, animated: true)
            } else {
                ApiManager.shared.showFailure()
            }
        }
    }
    
    @IBAction func cancelTapped(_ sender: UIButton) {
        dismiss(animated: true)
    }
    
}
