//
//  ContractsViewController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 13/9/21.
//

import UIKit

class ContractsViewController: PopUpViewController {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var contracts = [Contract]()
    
    private var selectedContract: Contract?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "ContractTableViewCell", bundle: nil), forCellReuseIdentifier: "contract")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setHeightToTableView()
    }
    
    private func setHeightToTableView() {
        if contracts.count < 5 {
            tableView.isScrollEnabled = false
            let height: CGFloat = CGFloat(contracts.count * 54)
            tableView.heightAnchor.constraint(equalToConstant: height).isActive = true
        } else {
            tableView.isScrollEnabled = true
        }
    }
    
    @IBAction func noAdressTapped(_ sender: Any) {
        let alert = UIAlertController(
            title: "Для активации личного кабинета обратитесь в службу поддержки",
            message: nil,
            preferredStyle: .alert
        )
        alert.view.tintColor = UIColor(named: "primary")!
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel))
        alert.addAction(UIAlertAction(title: "+7 (473) 300-40-00", style: .default, handler: { (_) in
            let phone = "+7 (473) 300-40-00"
            if let url = URL(string: "tel://\(phone)"), UIApplication.shared.canOpenURL(url) {
                UIApplication.shared.open(url)
            }
        }))
        present(alert, animated: true)
    }

@IBAction func acceptTapped(_ sender: Any) {
    guard let contract = selectedContract else { return }
    UserDefaults.standard.set(contract.token, forKey: "session")
    ApiManager.shared.getNewPasswordBySMS { (message) in
        ApiManager.shared.errorAlert(title: message, text: "") { [weak self] in
            self?.dismiss(animated: true)
        }
    }
}

@IBAction func cancelTapped(_ sender: Any) {
    dismiss(animated: true)
}

}

extension ContractsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contracts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contract", for: indexPath) as! ContractTableViewCell
        cell.configure(contract: contracts[indexPath.row])
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! ContractTableViewCell
        cell.isSelected = true
        selectedContract = contracts[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! ContractTableViewCell
        cell.isSelected = false
        selectedContract = nil
    }
    
}
