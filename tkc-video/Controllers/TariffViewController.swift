//
//  TariffViewController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 29/12/21.
//

import UIKit

class TariffViewController: UIViewController {

    @IBOutlet weak var tariffNameLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var sectionName = ""
    
    var tariffs = [Tariff]()
    
    var selectedTariffs: [Tariff] {
        var sTariffs: [Tariff] = []
        for tariffCell in tableView.visibleCells {
            guard let tariffCell = tariffCell as? TariffTypeTableViewCell else { break }
            if tariffCell.isCellSelected { sTariffs.append(tariffCell.tariff) }
        }
        return sTariffs
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tariffNameLabel.text = sectionName
        
        tableView.contentInset = UIEdgeInsets(top: 32, left: 0, bottom: 80, right: 0)
        tableView.register(UINib(nibName: "TariffTypeTableViewCell", bundle: nil), forCellReuseIdentifier: "tariffType")
    }
    
    @IBAction func backTapped(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func connectTapped(_ sender: Any) {
        guard !selectedTariffs.isEmpty else {
            ApiManager.shared.errorAlert(text: "Ни один тариф не выбран")
            return
        }
        let controller = storyboard?.instantiateViewController(withIdentifier: "ApplicationFormViewController") as! ApplicationFormViewController
        controller.selectedTariffs = selectedTariffs.map({ $0.name })
        navigationController?.pushViewController(controller, animated: true)
    }
    
}

extension TariffViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tariffs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tariffType", for: indexPath) as! TariffTypeTableViewCell
        cell.selectionStyle = .none
        cell.configure(tariff: tariffs[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! TariffTypeTableViewCell
        cell.select()
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! TariffTypeTableViewCell
        cell.deselect()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}
