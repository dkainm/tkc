//
//  String extension.swift
//  tkc-video
//
//  Created by Alex Rudoi on 26/9/21.
//

import Foundation

extension String {
    
    func tail(from: Int) -> String {
        return String(suffix(from: index(startIndex, offsetBy: from)))
    }
    
    func applyPatternOnNumbers(pattern: String, replacementCharacter: Character) -> String {
        var pureNumber = self.replacingOccurrences( of: "[^0-9]", with: "", options: .regularExpression)
        for index in 0 ..< pattern.count {
            guard index < pureNumber.count else { return pureNumber }
            let stringIndex = String.Index(utf16Offset: index, in: pattern)
            let patternCharacter = pattern[stringIndex]
            guard patternCharacter != replacementCharacter else { continue }
            pureNumber.insert(patternCharacter, at: stringIndex)
        }
        return pureNumber
    }
    
    func getDate(format: String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = format
        return dateFormatter.date(from: self) ?? Date()
    }
    
    func containsText() -> Bool {
        if self.trimmingCharacters(in: .whitespaces).isEmpty || self == "" {
            return false
        } else {
            return true
        }
    }
    
    func containsPhone() -> Bool {
        print(self)
        let phoneRegex = "^[0-9+]{0,1}+[0-9]{5,16}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        if count == 12 {
            return phoneTest.evaluate(with: self)
        } else {
            return false
        }
    }
    
    func containsEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: self)
    }
}


