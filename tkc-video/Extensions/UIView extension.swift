//
//  UIView extension.swift
//  crypto
//
//  Created by Alex Rudoi on 30/6/21.
//

import UIKit

extension UIView {
    func showAnimation(_ completionBlock: (() -> Void)? = nil) {
      isUserInteractionEnabled = false
        UIView.animate(withDuration: 0.08,
                       delay: 0,
                       options: .curveLinear,
                       animations: { [weak self] in
                        self?.alpha = 0.6
        }) {  (done) in
            UIView.animate(withDuration: 0.1,
                           delay: 0,
                           options: .curveLinear,
                           animations: { [weak self] in
                            self?.alpha = 1
            }) { [weak self] (_) in
                self?.isUserInteractionEnabled = true
                completionBlock?()
            }
        }
    }
    
    func setBackground(_ image: UIImage) {
        let background = image
        var imageView : UIImageView!
        imageView = UIImageView(frame: bounds)
        imageView.contentMode =  UIView.ContentMode.scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = center
        addSubview(imageView)
        sendSubviewToBack(imageView)
    }
}


extension UITextField {
    
    func setCursorLocation(_ location: Int) {
        guard let cursorLocation = position(from: beginningOfDocument, offset: location) else { return }
        DispatchQueue.main.async { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.selectedTextRange = strongSelf.textRange(from: cursorLocation, to: cursorLocation)
        }
    }
    
}
