//
//  UILabel extension.swift
//  tkc-video
//
//  Created by Alex Rudoi on 26/2/22.
//

import UIKit

extension UILabel {

    var isTruncated: Bool {

        guard let labelText = text else {
            return false
        }

        let labelTextSize = (labelText as NSString).boundingRect(
            with: CGSize(width: frame.size.width, height: .greatestFiniteMagnitude),
            options: .usesLineFragmentOrigin,
            attributes: [.font: font ?? UIFont()],
            context: nil).size

        return labelTextSize.height > bounds.size.height
    }
    
    var textHeight: CGFloat {
        let labelTextSize = (text ?? "").boundingRect(
            with: CGSize(width: frame.size.width, height: .greatestFiniteMagnitude),
            options: .usesLineFragmentOrigin,
            attributes: [.font: font ?? UIFont()],
            context: nil).size

        return labelTextSize.height
    }
    
    func actualNumberOfLines() -> Int {
        let maxSize = CGSize(width: frame.size.width, height: CGFloat(MAXFLOAT))
        let textHeight = sizeThatFits(maxSize).height
        let lineHeight = font.lineHeight
        return Int(ceil(textHeight / lineHeight))
    }
    
    func calculateContentHeight() -> CGFloat{
        let maxLabelSize: CGSize = CGSize(width: frame.size.width, height: CGFloat(9999))
        let contentString = text ?? ""
        let expectedLabelSize = contentString.boundingRect(with: maxLabelSize,
                                                           options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                                           attributes: [NSAttributedString.Key.font: font ?? UIFont()],
                                                           context: nil)
        return expectedLabelSize.size.height
    }
}
