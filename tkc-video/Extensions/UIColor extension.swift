//
//  UIColor extension.swift
//  tkc-video
//
//  Created by Alex Rudoi on 2/4/22.
//

import UIKit

extension UIColor {
    
    static var primary = #colorLiteral(red: 0.8196078431, green: 0, blue: 0, alpha: 1)
    
}
