//
//  UIViewController extention.swift
//  tkc-video
//
//  Created by Alex Rudoi on 13/9/21.
//

import UIKit

extension UIViewController {
    func isDissolving() {
        modalTransitionStyle = .crossDissolve
        modalPresentationStyle = .overCurrentContext
    }
}
