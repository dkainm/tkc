//
//  UIImageView extension.swift
//  crypto
//
//  Created by Alex Rudoi on 30/6/21.
//

import UIKit
import Foundation

extension UIImage {
    
    func withColor(_ color: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        color.setFill()
        
        let context = UIGraphicsGetCurrentContext()
        context?.translateBy(x: 0, y: self.size.height)
        context?.scaleBy(x: 1.0, y: -1.0)
        context?.setBlendMode(CGBlendMode.normal)
        
        let rect = CGRect(origin: .zero, size: CGSize(width: self.size.width, height: self.size.height))
        context?.clip(to: rect, mask: self.cgImage!)
        context?.fill(rect)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage ?? UIImage()
    }
    
    func tintPicto(_ fillColor: UIColor) -> UIImage {

       return modifiedImage { context, rect in
           // draw tint color
           context.setBlendMode(.normal)
           fillColor.setFill()
           context.fill(rect)

           // mask by alpha values of original image
           context.setBlendMode(.destinationIn)
           context.draw(cgImage!, in: rect)
       }
   }
    
   fileprivate func modifiedImage(_ draw: (CGContext, CGRect) -> ()) -> UIImage {

       // using scale correctly preserves retina images
       UIGraphicsBeginImageContextWithOptions(size, false, scale)
       let context: CGContext! = UIGraphicsGetCurrentContext()
       assert(context != nil)

       // correctly rotate image
       context.translateBy(x: 0, y: size.height)
       context.scaleBy(x: 1.0, y: -1.0)

       let rect = CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height)

       draw(context, rect)

       let image = UIGraphicsGetImageFromCurrentImageContext()
       UIGraphicsEndImageContext()
       return image!
   }
}
