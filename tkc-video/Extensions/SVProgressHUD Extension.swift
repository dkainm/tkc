//
//  SVProgressHUD Extension.swift
//  tkc-video
//
//  Created by Alex Rudoi on 20/9/21.
//

import UIKit
import SVProgressHUD

extension SVProgressHUD {
    
    public static func showWithAnimation() {
        let loadingView = LoadingViewController()
        loadingView.modalTransitionStyle = .crossDissolve
        loadingView.modalPresentationStyle = .overCurrentContext
        ApiManager.shared.activeView().present(loadingView, animated: true)
    }
    
    public static func dismissWithAnimation() {
        guard ApiManager.shared.activeView().restorationIdentifier == "LoadingViewController" else { return }
        ApiManager.shared.activeView().dismiss(animated: true)
    }
}
