//
//  TimeInterval extension.swift
//  tkc-video
//
//  Created by Alex Rudoi on 20/11/21.
//

import Foundation

extension TimeInterval{
    
    func timeString() -> String {
        let ms = Int(truncatingRemainder(dividingBy: 1) * 1000)
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.hour, .minute, .second]
        return formatter.string(from: self)! + ".\(ms)"
    }
    
    func dateString() -> String {
        let dateVar = Date(timeIntervalSince1970: self)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd.MM.yyyy HH:mm"
        return dateFormatter.string(from: dateVar)
    }
}
