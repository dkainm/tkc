//
//  State.swift
//  tkc-video
//
//  Created by Alex Rudoi on 13/9/21.
//

import Foundation

class State {
    public static var tokenToSave: String?
    public static var currentPersonalData: PersonalData!
    public static var previousSession: String?
    public static var phoneToRestore: String!
    
    public static var isVideoEnabled: Bool!
    public static var privateHouse: Bool!
    public static var serviceMessage: ServiceMessage?
    public static var messageTv: String?
    
    public static var fcmToken: String?
}

struct ServiceMessage: Decodable {
    let activate: Bool
    let message: String?
    let id: String?
    
    enum CodingKeys: String, CodingKey {
        case activate, message, id = "id_message"
    }
}
