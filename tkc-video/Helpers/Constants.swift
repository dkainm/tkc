//
//  Constants.swift
//  tkc-video
//
//  Created by Alex Rudoi on 2/9/21.
//

import UIKit
import SVProgressHUD
import AnyFormatKit
import Whisper

let phoneFormatter = DefaultTextInputFormatter(textPattern: "+7 (###) ###-##-##")
let phoneDataFormatter = DefaultTextInputFormatter(textPattern: "## (###) ###-##-##")
let phoneInputController = TextFieldInputController()
let phoneAttributedPlaceholder: NSMutableAttributedString = {
    let attrs1 = [NSAttributedString.Key.foregroundColor : UIColor.black] as [NSAttributedString.Key : Any]
    let attributedString1 = NSMutableAttributedString(string:"+7", attributes:attrs1)
    return attributedString1
}()

let basicAuth: String = {
    let loginString = NSString(format: "%@:%@", "mobileApp", "cVbHXKuepxWGpvw2")
    let loginData: NSData = loginString.data(using: String.Encoding.utf8.rawValue)! as NSData
    return loginData.base64EncodedString(options: [])
}()

public func getDate(from dateString: String) -> Date {
    guard let timeInterval = TimeInterval(dateString) else { return Date() }
    let date = Date(timeIntervalSince1970: timeInterval)
    return date
}

public struct Header {
    var value: String
    var field: String
}

public enum URLMethod: String {
    case post = "POST"
    case get = "GET"
    case put = "PUT"
}

public func request(url: String,
                    method: URLMethod? = .post,
                    params: [String: Any]? = nil,
                    sessionHeader: Header? = nil,
                    responseIsArray: Bool? = false,
                    complition: @escaping (Any?) -> Void) {
    
    let session = URLSession.shared
    var request = URLRequest(url: URL(string: url)!)
    
    request.httpMethod = method!.rawValue
    
    if let params = params {
        do {
            switch method {
            case .get:
                var components = URLComponents(url: URL(string: url)!, resolvingAgainstBaseURL: false)!
                components.setQueryItems(with: params)

                request = URLRequest(url: components.url!)
            default:
                request.httpBody = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
            }
        } catch let error {
            print("error! -> ", error)
        }
    }
    
    if let sessionHeader = sessionHeader {
        request.addValue(sessionHeader.value, forHTTPHeaderField: sessionHeader.field)
    }

    request.addValue("Basic \(basicAuth)", forHTTPHeaderField: "Authorization")
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    request.addValue("application/json", forHTTPHeaderField: "Accept")

    let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

        guard error == nil else {
            print("error! –> ", error!)
            return
        }

        guard let data = data else {
            print("error! –> ", error!)
            return
        }

        do {
            let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
            let jsn: Any
            
            if responseIsArray! {
                jsn = json as? [[String: Any]]
            } else {
                jsn = json as? [String: Any]
            }
            
            DispatchQueue.main.async {
                    complition(json)
            }
        } catch let error {
            print("error! –> ", error)
            DispatchQueue.main.async {
                complition(nil)
            }
        }
    })
    task.resume()
}

public func endSession(registrationDelegate: RegistrationDelegate? = nil) {
    UserDefaults.standard.set(nil, forKey: "session")
    NotificationManager.shared.deleteAllMessages()
    if let delegate = registrationDelegate {
        ApiManager.shared.showLoginView(delegate: delegate)
    }
}

public func decodeFromDictionary<Object: Decodable>(_ dict: [String: Any], to object: Object.Type, complition: (Object?) -> Void) {
    do {
        let json = try JSONSerialization.data(withJSONObject: dict)
        let decoder = JSONDecoder()
        let decodedObject = try decoder.decode(object, from: json)
        complition(decodedObject)
    } catch {
        complition(nil)
    }
}

public func decodeFromArray<Object: Decodable>(_ value: [[String: Any]], to object: Object.Type, complition: ([Object]?) -> Void) {
    var arr: [Object] = []
    for val in value {
        decodeFromDictionary(val, to: Object.self) { (result) in
            if let res = result {
                arr.append(res)
            }
        }
    }
    complition(arr)
}

public func showWarningView(text: String, info: String? = nil, completion: (() -> Void)? = nil) {
    let alert = UIAlertController(title: text, message: info, preferredStyle: .alert)
    alert.view.tintColor = UIColor(named: "primary")!
    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
        alert.dismiss(animated: true)
        completion?()
    }))
    ApiManager.shared.activeView().present(alert, animated: true)
}
