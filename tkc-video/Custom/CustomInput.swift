//
//  CustomInput.swift
//  crypto
//
//  Created by Alex Rudoi on 30/6/21.
//

import UIKit

protocol CustomInputDelegate {
    func tapped(input: CustomInput)
}

class CustomInput: UIView {
    
    let stackView = UIStackView()
    let textField = UITextField()
    let infoLabel = UILabel()
    public let iconRight = UIImageView()
    public let iconLeft = UIImageView()
    let button = UIButton()
    let button1 = UIButton()
    
    let separator = UIView()
    
    var placeholder = "Placeholder"
    
    var iconColor: UIColor = .black
    var leftImage = #imageLiteral(resourceName: "back")
    var rightImage = #imageLiteral(resourceName: "back")
    var image2 = #imageLiteral(resourceName: "eyeClosed")
    
    var noicons = false
    
    enum Icon {
        case left
        case right
        case all
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func layoutSubviews() {
        setUp()
    }
    
    func hideIcon(_ icon: Icon) {
        noicons = false
        switch icon {
        case .left:
            iconLeft.isHidden = true
            button1.isHidden = true
        case .right:
            button.isHidden = true
            button1.isHidden = true
        case .all:
            iconLeft.isHidden = true
            button.isHidden = true
            button1.isHidden = true
        }
    }
    
    private func setUp() {
        
        if noicons {
            hideIcon(.all)
        }
        
        textField.placeholder = placeholder
        textField.font = UIFont(name: "SFProText-Medium", size: 18)
        textField.textColor = .black
        textField.widthAnchor.constraint(equalToConstant: frame.width-30).isActive = true
        
        iconRight.image = rightImage
        iconRight.widthAnchor.constraint(equalToConstant: 24).isActive = true
        iconRight.heightAnchor.constraint(equalToConstant: 24).isActive = true
        
        button.titleLabel?.textColor = .black
        button.setTitle("rtyjk", for: .normal)
        button.widthAnchor.constraint(equalToConstant: 120).isActive = true
        button.heightAnchor.constraint(equalToConstant: 24).isActive = true
        button.translatesAutoresizingMaskIntoConstraints = false
        
        button1.setImage(image2, for: .normal)
        button1.widthAnchor.constraint(equalToConstant: 24).isActive = true
        button1.heightAnchor.constraint(equalToConstant: 24).isActive = true
        
        iconLeft.image = leftImage
        iconLeft.widthAnchor.constraint(equalToConstant: 24).isActive = true
        iconLeft.heightAnchor.constraint(equalToConstant: 24).isActive = true
        
        stackView.spacing = 5
        stackView.axis = .horizontal
        stackView.alignment = .fill
        
        stackView.addArrangedSubview(iconLeft)
        stackView.addArrangedSubview(textField)
        stackView.addArrangedSubview(button1)
        stackView.addArrangedSubview(button)
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        
        separator.frame = CGRect(x: 0,
                                 y: bounds.maxY,
                                 width: frame.width,
                                 height: 0.5)
        separator.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        
        addSubview(stackView)
        addSubview(separator)
        
        button.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        button.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        
        stackView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        stackView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }
}



extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
