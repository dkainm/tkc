//
//  LoadingViewController.swift
//  tkc-video
//
//  Created by Alex Rudoi on 20/9/21.
//

import UIKit
import SVProgressHUD

class LoadingViewController: UIViewController {

    override func loadView() {
        restorationIdentifier = "LoadingViewController"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        SVProgressHUD.show()
    }
    
    private func setUp() {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)
    }
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        SVProgressHUD.dismiss()
        super.dismiss(animated: flag, completion: completion)
    }
}
