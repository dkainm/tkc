//
//  CustomCheckbox.swift
//  tkc-video
//
//  Created by Alex Rudoi on 14/9/21.
//

import UIKit

protocol CustomCheckboxDelegate {
    func valueChanged()
}

class CustomCheckbox: UIView {
    
    private let redView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(named: "primary")!
        view.frame = CGRect(x: 8, y: 8, width: 12, height: 12)
        return view
    }()
    
    var isOn = false {
        didSet {
            if isOn {
                addSubview(redView)
            } else {
                redView.removeFromSuperview()
            }
        }
    }
    
    var delegate: CustomCheckboxDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func layoutSubviews() {
        setUp()
    }
    
    private func setUp() {
        backgroundColor = .clear
        bounds = CGRect(x: 0, y: 0, width: 28, height: 28)
        
        layer.borderWidth = 1
        layer.borderColor = UIColor.black.cgColor
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        delegate?.valueChanged()
        super.touchesBegan(touches, with: event)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        isOn = !isOn
        super.touchesEnded(touches, with: event)
    }
    
}
