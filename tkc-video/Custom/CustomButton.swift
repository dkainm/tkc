//
//  CustomButton.swift
//  crypto
//
//  Created by Alex Rudoi on 3/7/21.
//

import UIKit

class CustomButton: UIButton {
    
    let stackView = UIStackView()
    let label = UILabel()
    public let iconRight = UIImageView()
    public let iconLeft = UIImageView()
    
    public var text = "Button"
    var attributedText: NSMutableAttributedString?
    
    var iconColor: UIColor?
    var leftImage = UIImage(named: "next")!
    var rightImage = UIImage(named: "next")!
    
    var noicons = false
    var rounded = false
    
    enum Icon {
        case left
        case right
        case all
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func layoutSubviews() {
        setUp()
    }
    
    func hideIcon(_ icon: Icon) {
        noicons = false
        switch icon {
        case .left:
            iconLeft.isHidden = true
        case .right:
            iconRight.isHidden = true
        case .all:
            iconLeft.isHidden = true
            iconRight.isHidden = true
        }
    }
    
    func setRound() {
        rounded = true
        layer.borderColor = #colorLiteral(red: 0.9529411765, green: 0.9568627451, blue: 0.9764705882, alpha: 1)
        layer.borderWidth = 1
        layer.shadowColor = #colorLiteral(red: 0.7803921569, green: 0.7882352941, blue: 0.8196078431, alpha: 1)
        layer.shadowOpacity = 0.15
        layer.shadowRadius = 20
        layer.shadowOffset = CGSize(width: 0, height: 8)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.showAnimation {}
    }
    
    var isCenter = false
    
    private func setUp() {
        
        if rounded {
            setRound()
        }
        
        layer.cornerRadius = 10
        
        if noicons {
            hideIcon(.all)
        }
        
        if attributedText == nil {
            text = (titleLabel?.text)!
            label.text = text
        } else {
            label.attributedText = attributedText
        }
        
        label.font = titleLabel?.font
        label.textColor = titleLabel?.textColor
        label.minimumScaleFactor = 0.8
        label.sizeToFit()
        titleLabel?.removeFromSuperview()
        
        if let color = iconColor {
            rightImage = rightImage.tintPicto(color)
            leftImage = leftImage.tintPicto(color)
        } else {
            rightImage = rightImage.tintPicto(tintColor)
            leftImage = leftImage.tintPicto(tintColor)
        }
        
        iconRight.image = rightImage
        iconRight.widthAnchor.constraint(equalToConstant: 25).isActive = true
        iconRight.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        iconLeft.image = leftImage
        iconLeft.widthAnchor.constraint(equalToConstant: 25).isActive = true
        iconLeft.heightAnchor.constraint(equalToConstant: 25).isActive = true
        
        stackView.spacing = 8
        stackView.axis = .horizontal
        stackView.alignment = .fill
        
        stackView.addArrangedSubview(iconLeft)
        stackView.addArrangedSubview(label)
        stackView.addArrangedSubview(iconRight)
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.isUserInteractionEnabled = false
        
        addSubview(stackView)
        
        if isCenter {
            stackView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        } else {
            stackView.widthAnchor.constraint(equalToConstant: frame.width-32).isActive = true
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16).isActive = true
            
            if rounded {
                layer.shadowColor = #colorLiteral(red: 0.7803921569, green: 0.7882352941, blue: 0.8196078431, alpha: 1)
                layer.shadowOpacity = 0.15
                layer.shadowRadius = 20
                layer.shadowOffset = CGSize(width: 0, height: 8)
            } else {
                layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
                layer.shadowOffset = CGSize(width: 0, height: 4)
                layer.shadowOpacity = 0.16
                layer.shadowRadius = 8
            }
        }
        stackView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }
    
    func alignCenter() {
        isCenter = true
        setUp()
    }
    
    func outlined() {
        layer.borderWidth = 1.5
        layer.borderColor = tintColor.cgColor
        backgroundColor = .clear
    }
    
}

