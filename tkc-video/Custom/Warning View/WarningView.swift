//
//  WarningView.swift
//  tkc-video
//
//  Created by Alex Rudoi on 2/10/21.
//

import UIKit

class WarningView: UIView {

    @IBOutlet weak var label: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func layoutSubviews() {
        setup()
    }
    
    private func setup() {
        Bundle.main.loadNibNamed("WarningView", owner: self, options: nil)
        print("yes")
    }
}
