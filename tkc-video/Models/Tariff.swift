//
//  Tariff.swift
//  tkc-video
//
//  Created by Alex Rudoi on 28/12/21.
//

import Foundation

struct Tariff: Decodable {
    let name: String
    let price: String
    let info: [TariffType]
    
    enum CodingKeys: String, CodingKey {
        case name = "NameTariff"
        case price = "PriceTariff"
        case info = "Tariff"
    }
}

struct TariffType: Decodable {
    let name: String
    let data: String
    let icon: URL
    let desc: String
    let price: String
    
    enum CodingKeys: String, CodingKey {
        case name = "viduslugi"
        case data
        case icon = "urlicon"
        case desc = "description"
        case price
    }
}
