//
//  Camera.swift
//  tkc-video
//
//  Created by Alex Rudoi on 16/5/21.
//

struct Camera: Decodable {
    var object: String?
    let id: String
    var name: String
    let favorite: String
    let server: String
    let token: String
    let renamename: String
    let active: String
}
