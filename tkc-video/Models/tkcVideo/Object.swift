//
//  Object.swift
//  tkc-video
//
//  Created by Alex Rudoi on 29/6/21.
//

import UIKit

struct ObjectDecode: Decodable {
    var objects: [Object]
}

struct Object: Decodable {
    var name: String
    var id: String
    var server: String
    var token: String
}


