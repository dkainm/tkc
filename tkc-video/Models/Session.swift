//
//  Session.swift
//  tkc-video
//
//  Created by Alex Rudoi on 6/6/21.
//

import Foundation

struct Session: Decodable {
    
    var session: String? 
    
    var message: String?
}
