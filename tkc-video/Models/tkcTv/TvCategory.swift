//
//  TvCategory.swift
//  tkc-tv
//
//  Created by Alex Rudoi on 23/8/21.
//

import Foundation

struct TvCategory: Decodable {
    var id: String
    var name: String
    var icon: URL
    
    enum CodingKeys: String, CodingKey {
        case id = "id_category",
             name = "name_category",
             icon = "icon_url"
    }
    
    public static var categories = [TvCategory]()
}
