//
//  TvDate.swift
//  tkc-video
//
//  Created by Alex Rudoi on 2/9/21.
//

import Foundation

struct TvDate {
    var day: Date
    var weekDay: Date
    
    init(day: String, weekDay: String) {
        self.day = getDate(from: day)
        self.weekDay = getDate(from: weekDay)
    }
}
