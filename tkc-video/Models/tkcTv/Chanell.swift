//
//  Chanell.swift
//  tkc-video
//
//  Created by Alex Rudoi on 26/8/21.
//

import Foundation
import CoreData
 
struct Chanell: Decodable {
    var id: String
    var idChanell: String
    var name: String
    var icon: URL
    var server: URL
    var codeName: String
    var token: String
    var pg: String
    var program: String
    
    enum CodingKeys: String, CodingKey {
        case id = "id_num",
             idChanell = "id_chanel",
             name = "display_name",
             icon = "icon_chanel",
             server,
             codeName = "name_chanel",
             token,
             pg,
             program = "name_teleprogramm"
    }
    
    public static var chanells = [Chanell]()
    
    public static func getChannelFromLocal(_ local: LocalChannel) -> Chanell {
        return Chanell(
            id: local.id ?? "",
            idChanell: local.idChannel ?? "",
            name: local.name ?? "",
            icon: URL(string: local.icon!)!,
            server: URL(string: local.server!)!,
            codeName: local.codeName ?? "",
            token: local.token ?? "",
            pg: local.pg ?? "",
            program: local.program ?? ""
        )
    }
    
    func getLocalChannel() -> LocalChannel? {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return nil }
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "LocalChannel")
        do {
            guard let channels = try managedContext.fetch(request) as? [LocalChannel] else {
                print("Unable to fetch data")
                return nil
            }
            
            guard let not = channels.first(
                where: {
                    $0.id == id &&
                    $0.name == name
                }
            ) else {
                return nil
            }
            
            print("Data fetched, no issue")
            
            return not
        } catch {
            print("Unable to fetch data: ", error.localizedDescription)
            return nil
        }
    }
}
