//
//  Teleprogram.swift
//  tkc-video
//
//  Created by Alex Rudoi on 28/8/21.
//

import Foundation

struct Teleprogram: Decodable {
    
    var id: String
    var startDate: String
    var stopDate: String
    
    var name: String
    var desc: String
    var genre: String
    var category: String
    
    var pg: String
    var day: String
    
    enum CodingKeys: String, CodingKey {
        case id = "id_chanel",
             startDate = "start_ut",
             stopDate = "stop_ut",
             name = "title",
             desc = "description",
             genre, pg,
             day = "day_teleprogram",
             category = "category_real_name"
    }
}
