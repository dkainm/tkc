//
//  CameraModel.swift
//  tkc-video
//
//  Created by Alex Rudoi on 24/5/21.
//

import UIKit
import ObjectMapper

let host = "demo-watcher.flussonic.com"

class CameraModel: NSObject, Mappable {
    
    var name = ""
    var title = ""
    var token = ""
    var lastEventTime: UInt32 = 0
    var isOnline = false
    var streamStatus = StreamStatusModel()
    
    var urlForPlayer = ""
    var urlForPreview = ""

    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        super.init()
        self.mapping(map:map)
    }
    
    func mapping(map: Map) {
        name            <- map["name"]
        title           <- map["title"]
        isOnline        <- map["stream_status.alive"]
        streamStatus    <- map["stream_status"]
        lastEventTime   <- map["last_event_time"]
        token           <- map["playback_config.token"]

        urlForPlayer = "https://\(AuthorizationService.shared.session)@\(host)/\(name)?from=\(lastEventTime)/"
        print(urlForPlayer)
        urlForPreview = "https://\(host):\(streamStatus.https_port)/\(name)/preview.mp4?token=\(token)"
        
    }
}

