//
//  Agreement.swift
//  tkc-video
//
//  Created by Alex Rudoi on 17/9/21.
//

import Foundation

class Agreement: Decodable {
    let id: String
    let text: String
    let link: String
    var confirmed = false
    
    enum CodingKeys: String, CodingKey {
        case id = "IdDoc"
        case text = "TextDoc"
        case link = "Link"
    }
}
