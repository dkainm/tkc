//
//  Contract.swift
//  tkc-video
//
//  Created by Alex Rudoi on 30/8/21.
//

import Foundation

struct ContractsResponse: Decodable {
    var address: [Contract]
}

struct Contract: Decodable {
    var licCheck: String
    var name: String
    var token: String
    
    enum CodingKeys: String, CodingKey {
        case licCheck = "LicCheck",
             name = "DisplayName",
             token = "Token"
    }
}
