//
//  PersonalOffer.swift
//  tkc-video
//
//  Created by Alex Rudoi on 20/12/21.
//

import Foundation

struct PersonalOffer: Decodable {
    let id: String
    let title: String
    let previewDesc: String
    let desc: String
    let imageUrlString: String
    let startDateString: String
    let endDateString: String
    let isConnecting: String
    
    enum CodingKeys: String, CodingKey {
        case id, title, previewDesc = "preview_description", desc = "description", imageUrlString = "url_image", startDateString = "begin", endDateString = "end", isConnecting = "view_button_connect"
    }
    
    func startDate() -> Date {
        return getDate(from: startDateString)
    }
    
    func endDate() -> Date {
        return getDate(from: endDateString)
    }
}
