//
//  PersonalData.swift
//  tkc-video
//
//  Created by Alex Rudoi on 17/9/21.
//

import Foundation

struct PersonalData: Decodable {
    let fullName: String
    let phoneNumber: [PersonalDataPhone]
    let email: String
    let address: String
    let numberContact: String
    let login: String
    let contractActivity: String
    let balanceDate: String
    let balance: Int
    let toPay: Int
    let payBeforeDate: String
    let tariffName: String
    let tariffPrice: Int
    let tariffs: [PersonalDataTariff]
    let services: [PersonalDataService]
    let paymentType: PersonalDataPaymentType
    let interfaceType: Int
    
    enum CodingKeys: String, CodingKey {
        case fullName = "FullName"
        case phoneNumber = "NumberPhone"
        case email = "EMail"
        case address = "Address"
        case numberContact = "NumberContract"
        case login = "Login"
        case contractActivity = "ConrtactActiviti"
        case balanceDate = "BalanceDate"
        case balance = "Balance"
        case toPay = "ToPay"
        case payBeforeDate = "PayBefore"
        case tariffName = "NameTariff"
        case tariffPrice = "PriceTariff"
        case tariffs = "Tariff"
        case services = "Services"
        case paymentType = "PaymentType"
        case interfaceType = "InterfaceType"
    }
}

struct PersonalDataPhone: Decodable {
    let phone: String
    let confirmed: Bool
    
    enum CodingKeys: String, CodingKey {
        case phone = "Phone", confirmed = "PhoneConfirmed"
    }
}

struct PersonalDataTariff: Decodable {
    let service: String
    let info: String
    let icon: URL
    
    enum CodingKeys: String, CodingKey {
        case service = "viduslugi"
        case info = "date"
        case icon = "idicon"
    }
}

struct PersonalDataService: Decodable {
    let service: String
    let icon: URL
    let subInfo: [PersonalDataServiceSubInfo]
    let type: String
    
    enum CodingKeys: String, CodingKey {
        case service = "VidUslugi"
        case icon = "idicon"
        case subInfo = "SubServices"
        case type
    }
}

struct PersonalDataServiceSubInfo: Decodable {
    let startDate: String
    let endDate: String
    let title: String
    let sn: String
    let price: Int
    let desc: String
    
    enum CodingKeys: String, CodingKey {
        case startDate = "Date1"
        case endDate = "Date2"
        case title = "Title"
        case sn = "SN"
        case price = "Price"
        case desc = "Description"
    }
}

struct PersonalDataPaymentType: Decodable {
    let type: Int
    let message: String
    
    enum CodingKeys: String, CodingKey {
        case type = "Type"
        case message = "Message"
    }
}
