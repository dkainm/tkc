//
//  FBNotification.swift
//  tkc-video
//
//  Created by Alex Rudoi on 25/2/22.
//

import Foundation

struct FBNotification: Decodable, Hashable {
    
    var title: String
    var body: String
    var date: Date?
    var dateString: String?
    
    var isRead: Bool? = false
    
    enum CodingKeys: String, CodingKey {
        case title, body
        case dateString = "dt_create"
    }
    
    func getDate() -> Date {
        if let date = date {
            return date
        }
        return dateString?.getDate(format: "yyyy-MM-dd HH:mm:ss") ?? Date()
    }
    
}
