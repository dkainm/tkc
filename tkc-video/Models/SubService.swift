//
//  SubService.swift
//  tkc-video
//
//  Created by Alex Rudoi on 29/12/21.
//

import Foundation

struct SubService: Decodable {
    let name: String
    let price: String
    let icon: URL
    let desc: String
    
    enum CodingKeys: String, CodingKey {
        case name = "viduslugi"
        case price
        case icon = "urlicon"
        case desc = "description"
    }
}
