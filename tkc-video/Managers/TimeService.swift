//
//  TimeService.swift
//  tkc-video
//
//  Created by Alex Rudoi on 9/11/21.
//

import Foundation

final class TimeService {
    
    static var shared: TimeService {
        let instance = TimeService()
        instance.dateFormatter.locale = Locale(identifier: "ru")
        return instance
    }
    
    private var dateFormatter = DateFormatter()
    
    let day = 86400
    
    //MARK: – Unix
    
    func getDateFromUnix(_ unix: TimeInterval) -> Date {
        let date = Date(timeIntervalSince1970: unix)
        return date
    }
    
    func getUnixFromDate(_ date: Date) -> TimeInterval {
        let unix = date.timeIntervalSince1970
        return unix
    }
    
    //MARK: – Parts
    
    enum DatePart: String {
        case day = "d"
        case dayAndMonth = "dd MMMM"
        case weekday = "EEEE"
        case month = "MMMM"
    }
    
    let russianWeedays = ["Mon": "Понедельник",
                          "Tue": "Вторник",
                          "Wed": "Среда",
                          "Thu": "Четверг",
                          "Fri": "Пятница",
                          "Sat": "Суббота",
                          "Sun": "Восскресение"]
    
    
    func getPartFromDate(_ date: Date, part: DatePart) -> String {
        dateFormatter.dateFormat = part.rawValue
        return dateFormatter.string(from: date)
        
    }
    
}
