//
//  LoadingService.swift
//  tkc-video
//
//  Created by Alex Rudoi on 10/11/21.
//

import SVProgressHUD

final class LoadingService {
    
    static let shared: LoadingService = {
        return LoadingService()
    }()
    
    private func startInteraction() {
        UIApplication.shared.endIgnoringInteractionEvents()
    }
    
    private func stopInteraction() {
        UIApplication.shared.beginIgnoringInteractionEvents()
    }
    
    func show() {
        stopInteraction()
        SVProgressHUD.show()
    }
    
    func show(withStatus status: String) {
        stopInteraction()
        SVProgressHUD.show(withStatus: status)
    }
    
    func dismiss() {
        startInteraction()
        SVProgressHUD.dismiss()
    }
    
}

