//
//  DatabaseManager.swift
//  tkc-video
//
//  Created by Alex Rudoi on 5/4/22.
//

import CoreData

class DatabaseManager {
    
    static var shared: DatabaseManager {
        return DatabaseManager()
    }
    
    //MARK: – Base
    
    func getAllEntities<T: NSManagedObject>(entityName: String, completion: ([T]?) -> Void) {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        do {
            guard let array = try managedContext.fetch(request) as? [T] else {
                print("Unable to fetch data")
                completion(nil)
                return
            }
            
            print("Data fetched, no issue")
            completion(array)
        } catch {
            print("Unable to fetch data: ", error.localizedDescription)
            completion(nil)
        }
    }
    
    func deleteAllEntities(named name: String) {
        getAllEntities(entityName: name) { (entities) in
            guard let entities = entities else { return }
            for entity in entities {
                self.deleteEntity(entity)
            }
        }
    }
    
    func deleteEntity<T: NSManagedObject>(_ entity: T) {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
        managedContext.delete(entity)
        do {
            try managedContext.save()
            print("Data deleted")
        } catch {
            print("Failed to delete data: ", error.localizedDescription)
        }
    }
    
    //MARK: – Channels
    
    func saveChannel(_ channel: Chanell, completion: ((_ finished: Bool) -> ())? = nil) {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
        
        let localChannel = LocalChannel(context: managedContext)
        localChannel.id = channel.id
        localChannel.idChannel = channel.idChanell
        localChannel.name = channel.name
        localChannel.icon = channel.icon.absoluteString
        localChannel.server = channel.server.absoluteString
        localChannel.codeName = channel.codeName
        localChannel.token = channel.token
        localChannel.pg = channel.pg
        localChannel.program = channel.program
        
        do {
            try managedContext.save()
            print("Channel saved")
            completion?(true)
        } catch {
            print("Failed to save the channel: ", error.localizedDescription)
            completion?(false)
        }
    }
    
    func deleteChannel(_ channel: Chanell) {
        guard let localChannel = channel.getLocalChannel() else { return }
        deleteEntity(localChannel)
    }

    func getAllChannels(completion: @escaping ([Chanell]?) -> ()) {
        getAllEntities(entityName: "LocalChannel") { (lChannels) in
            
            guard let lChannels = lChannels as? [LocalChannel] else {
                completion(nil)
                return
            }
            
            var channels = [Chanell]()
            for localChannel in lChannels {
                channels.append(Chanell.getChannelFromLocal(localChannel))
            }
            
            completion(channels)
            
        }
    }
    
}
