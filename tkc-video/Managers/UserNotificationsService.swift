//
//  UserNotificationsService.swift
//  tkc-video
//
//  Created by Alex Rudoi on 15/2/22.
//

import UserNotifications

class UserNotificationsService: NSObject {
    
    let shared: UserNotificationsService = {
        var instance = UserNotificationsService()
        return instance
    }()
    
}

extension UserNotificationsService: UNUserNotificationCenterDelegate {
    
}
