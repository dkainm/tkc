//
//  NotificationManager.swift
//  tkc-video
//
//  Created by Alex Rudoi on 25/2/22.
//

import UIKit
import CoreData

class NotificationManager {
    
    static var shared: NotificationManager {
        return NotificationManager()
    }
    
    private func getCoreDataMessage(from notification: FBNotification) -> LocalNot? {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return nil }
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "LocalNot")
        do {
            guard let messagesArray = try managedContext.fetch(request) as? [LocalNot] else {
                print("Unable to fetch data")
                return nil
            }
            
            guard let not = messagesArray.first(
                where: {
                    $0.title == notification.title &&
                    $0.body == notification.body
                }
            ) else {
                return nil
            }
            
            print("Data fetched, no issue")
            
            return not
        } catch {
            print("Unable to fetch data: ", error.localizedDescription)
            return nil
        }
    }
    
    private func getAllCoreDataMessages(completion: ([LocalNot]?) -> Void) {
        DatabaseManager.shared.getAllEntities(entityName: "LocalNot") { messages in completion(messages) }
    }
    
    func updateBadgeCount() {
        self.getAllCoreDataMessages { (messages) in
            guard let messages = messages else { return }
            let unreadMessages = messages.filter({$0.isRead == false})
            print("unread messages count: ", unreadMessages.count)
            UIApplication.shared.applicationIconBadgeNumber = unreadMessages.count
            NotificationCenter.default.post(name: NSNotification.Name("badgeUpdate"), object: nil)
        }
    }
    
    func newMessage(notification: FBNotification) {
        saveMessage(notification: notification)
    }
    
    func readMessages(_ messages: [FBNotification]) {
        for mes in messages {
            var m = mes
            m.isRead = true
            updateMessage(m)
        }
        
        updateBadgeCount()
    }
    
    func saveMessage(notification: FBNotification, completion: ((_ finished: Bool) -> ())? = nil) {
        
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
        let not = LocalNot(context: managedContext)
        not.title = notification.title
        not.body = notification.body
        not.isRead = notification.isRead ?? false
        not.date = notification.getDate()
        
        do {
            try managedContext.save()
            print("Data saved")
            completion?(true)
        } catch {
            print("Failed to save data: ", error.localizedDescription)
            completion?(false)
        }
    }
    
    func updateMessage(_ notification: FBNotification) {
        guard let not = getCoreDataMessage(from: notification) else {
            print("Failed to update the message")
            return
        }
        
        not.isRead = notification.isRead ?? false
        
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
        do {
            try managedContext.save()
            print("Data updated")
        } catch {
            print("Failed to update data: ", error.localizedDescription)
        }
    }
    
    func deleteMessage(_ notification: FBNotification) {
        guard let not = getCoreDataMessage(from: notification) else {
            print("Failed to delete the message")
            return
        }
        
        DatabaseManager.shared.deleteEntity(not)
        updateBadgeCount()
    }
    
    func fetchMessages(completion: (_ complete: Bool, _ messagesArray: [FBNotification]?) -> ()) {
        guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "LocalNot")
        do {
            guard let messagesArray = try managedContext.fetch(request) as? [LocalNot] else {
                print("Unable to fetch data")
                completion(false, nil)
                return
            }
            
            var messages = [FBNotification]()
            
            for mes in messagesArray {
                let fbnot = FBNotification(
                    title: mes.title ?? "",
                    body: mes.body ?? "",
                    date: mes.date ?? Date(),
                    isRead: mes.isRead
                )
                messages.append(fbnot)
            }
            
            updateBadgeCount()
            
            print("Data fetched, no issue")
            
            completion(true, messages)
        } catch {
            print("Unable to fetch data: ", error.localizedDescription)
            completion(false, nil)
        }
    }
    
    
    public func updateServerMessages() {
        ApiManager.shared.getAllServerMessages { (nots) in
            NotificationManager.shared.fetchMessages { (_, notifications) in
                let messages = notifications ?? []
                if let nots = nots {
                    for not in nots {
                        if !messages.contains(where: {$0.body == not.body}) {
                            NotificationManager.shared.saveMessage(notification: not)
                        }
                    }
                }
                self.updateBadgeCount()
            }
        }
    }

    func deleteAllMessages(completion: (() -> Void)? = nil) {
        getAllCoreDataMessages { (nots) in
            if let nots = nots {
                
                nots.forEach { not in
                    
                    guard let managedContext = appDelegate?.persistentContainer.viewContext else { return }
                    managedContext.delete(not)
                    do {
                        try managedContext.save()
                        print("Data deleted")
                    } catch {
                        print("Failed to delete data: ", error.localizedDescription)
                    }
                    
                    if not == nots.last {
                        completion?()
                    }
                    
                }
                
            }
        }
    }
    
    
}

extension Sequence where Element == FBNotification {
    
    func uniqued() -> Array<Element> {
        var buffer = Array<Element>()
        var added = Set<Element>()
        for elem in buffer {
            if !added.contains(elem) {
                buffer.append(elem)
                added.insert(elem)
            }
        }
        return buffer
    }
}
