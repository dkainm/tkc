//
//  Loader.swift
//  tkc-video
//
//  Created by Alex Rudoi on 16/5/21.
//

import UIKit

class Loader: UIActivityIndicatorView {
    
    var view = UIView()
    
    init(at view: UIView) {
        super.init(frame: CGRect())
        self.view = view
        
        hidesWhenStopped = true
        view.addSubview(self)
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func start() {
        startAnimating()
    }
    
    func stop() {
        stopAnimating()
    }
}
