//
//  CamsLoader.swift
//  tkc-video
//
//  Created by Alex Rudoi on 16/5/21.
//

import Foundation
import Alamofire
import SVProgressHUD
import SwiftyJSON
import AnyFormatKit

class ApiManager {
    
    enum SupportType: String {
        case support = "support"
        case sales = "sales"
        case service = "service"
        case videosales = "videosales"
    }
    
    static let shared: ApiManager = {
        var instance = ApiManager()
        return instance
    }()
    
    let startUrl = "https://xn--b1agd0aean.xn--80asehdb/vsaas/api/app/"
    let tvUrl = "https://xn--b1agd0aean.xn--80asehdb/vsaas/api/app/tv/"
    let accountUrl = "https://telecom-service.site/"
    
    var session: String? {
        didSet {
            sessionHeader = Header(value: session ?? "abcdefg", field: "X-Vsaas-Session")
        }
    }
    var headers = HTTPHeaders()
    
    let urlAuth = URLCredential(user: "mobileApp", password: "cVbHXKuepxWGpvw2", persistence: .none)
    var sessionHeader: Header?
    
    var isBack = false
    
    public var backController: UIViewController?
    
    private init() {}
    
    //MARK: – Authorization
    
    private func createHeaders() -> HTTPHeaders? {
        
        session = UserDefaults.standard.string(forKey: "session") ?? nil
        if let session = session {
            let headers = HTTPHeaders([HTTPHeader(name: "X-Vsaas-Session", value: session),
                                       HTTPHeader(name: "Content-Type", value: "application/json")])
            return headers
        } else {
            return nil
        }
    }
    
    private func checkAuthorization() {
        if let headers = createHeaders() {
            self.headers = headers
        } else {
            print("No authorizated")
            showLoginView()
        }
        
    }
    
    public func showLoginView(delegate: RegistrationDelegate? = nil) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RegistrationViewController") as! RegistrationViewController
        vc.modalPresentationStyle = .fullScreen
        if let delegate = delegate {
            vc.delegate = delegate
        }
        vc.isNotStart = true
        backController = activeView()
        activeView().present(vc, animated: true)
    }
    
    public func activeView() -> UIViewController {
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        
        var topController = keyWindow?.rootViewController!
        while let presentedViewController = topController?.presentedViewController {
            topController = presentedViewController
        }
        return topController!
    }
    
    public func showFailure() {
        errorAlert(text: "Что-то пошло не так. Попробуйте еще раз")
    }
    
    public func validateSession(complition: (() -> Void)? = nil) {
        let url = accountUrl + "vsaas/api/v1/valid-token"
        
        checkAuthorization()
        
        let sessionHeader = Header(value: session ?? "abcaposdgjeos", field: "X-Vsaas-Session")
        
        request(url: url, sessionHeader: sessionHeader) { [weak self] (value) in
            if let value = value as? [String: Any] {
                guard let valid = value["valid"] as? Bool else { return }
                if valid {
                    print("validated")
                    DispatchQueue.main.async {
                        complition?()
                    }
                } else {
                    if let login = UserDefaults.standard.string(forKey: "userLogin"),
                       let pass = UserDefaults.standard.string(forKey: "userPass") {
                        ApiManager.shared.authorizate(login: login, pass: pass, isValidation: true) {
                            complition?()
                        }
                    } else {
                        self?.showLoginView()
                    }
                }
            } else {
                
                if let login = UserDefaults.standard.string(forKey: "userLogin"),
                   let pass = UserDefaults.standard.string(forKey: "userPass") {
                    ApiManager.shared.authorizate(login: login, pass: pass, isValidation: true) {
                        complition?()
                    }
                } else {
                    self?.showLoginView()
                }
            }
        }
        
    }
    
    public func authorizate(login: String, pass: String, isValidation: Bool? = false, comlition: (() -> Void)? = nil) {
        
        print("auth")
        
        let url = accountUrl + "vsaas/api/v1/user-authorization"
        
        let params: [String: Any] = [
            "login": login,
            "password": pass
        ]
        
        LoadingService.shared.show()
        
        request(url: url,
                params: params) { [weak self] (value) in
            
            LoadingService.shared.dismiss()
            if let value = value as? [String: Any] {
                if let token = value["token"] as? String {
                    State.tokenToSave = token
                    State.previousSession = self?.session
                    
                    UserDefaults.standard.set(login, forKey: "userLogin")
                    UserDefaults.standard.set(pass, forKey: "userPass")
                    
                    self?.getUserInfo(login: login, complition: { (phoneConf, policyConf) in
                        if phoneConf && policyConf {
                            UserDefaults.standard.set(token, forKey: "session")
                            comlition?()
                        } else if !policyConf && phoneConf {
                            let agreementController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AgreementViewController") as! AgreementViewController
                            agreementController.modalPresentationStyle = .fullScreen
                            self?.activeView().present(agreementController, animated: true)
                        } else if !phoneConf && policyConf {
                            self?.confirmPhone()
                        } else {
                            self?.confirmPhone(isBck: true) {
                                let agreementController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "AgreementViewController") as! AgreementViewController
                                agreementController.modalPresentationStyle = .fullScreen
                                self?.activeView().present(agreementController, animated: true)
                            }
                        }
                    })
                } else if let message = value["message"] as? String {
                    
                    if isValidation! {
                        self?.showLoginView()
                    } else {
                        self?.errorAlert(text: message)
                    }
                    
                }
            } else {
                self?.errorAlert(text: "Что-то пошло не так. Попробуйте еще раз")
            }
        }
    }
    
    private func sendSms(phone: String, complition: @escaping (Bool) -> Void) {
        
        let sHeader = Header(value: State.tokenToSave!, field: "X-Vsaas-Session")
        
        request(url: accountUrl + "vsaas/api/v1/get-operation-sms-code",
                params: ["phone": phone],
                sessionHeader: sHeader) { [weak self] (value) in
            if let value = value as? [String: Any],
               let result = value["SendSMSCode"] as? Bool {
                complition(result)
            } else {
                self?.showFailure()
            }
        }
    }
    
    private func checkCode(phone: String, code: String, complition: @escaping (Bool) -> Void) {
        request(url: accountUrl + "vsaas/api/v1/confirm-operation-sms-code",
                params: ["phone": phone,
                         "code": code],
                sessionHeader: Header(value: State.tokenToSave!, field: "X-Vsaas-Session")) { [weak self] (value) in
            if let value = value as? [String: Any],
               let result = value["SMSCodeCorrect"] as? Bool {
                complition(result)
            } else {
                self?.showFailure()
            }
        }
    }
    
    private func confirmPhone(isBck: Bool? = false, completion: (() -> Void)? = nil) {
        
        if let isBck = isBck {
            isBack = isBck
        }
        
        let alert = UIAlertController(title: "Подтверждение номера", message: "Введите номер телефона привязанный  к договору", preferredStyle: .alert)
        alert.view.tintColor = UIColor(named: "primary")!
        
        alert.addTextField { (textField : UITextField!) -> Void in
            textField.text = "+7"
            textField.keyboardType = .phonePad
            textField.textContentType = .telephoneNumber
            textField.delegate = phoneInputController
        }
        
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Отправить СМС", style: .default, handler: { [weak self] (_) in
            guard let phoneUnwrapped = alert.textFields?.first?.text,
                  let phone = phoneFormatter.unformat(phoneUnwrapped),
                  ("+7" + phone).containsPhone() else {
                showWarningView(text: "Некорректное значение",
                                info: "Вы ввели некорректный номер телефона")
                return
            }
            self?.sendSms(phone: phone) {
                completion?()
            }
        }))
        
        activeView().present(alert, animated: true)
    }
    
    public func smsAlert(phone: String, completion: (() -> Void)? = nil) {
        let codeAlert = UIAlertController(title: "Введите код из СМС", message: nil, preferredStyle: .alert)
        codeAlert.view.tintColor = UIColor(named: "primary")!
        codeAlert.addTextField { (textField : UITextField!) -> Void in
            textField.text = ""
            textField.placeholder = "Код"
            if #available(iOS 12.0, *) {
                textField.textContentType = .oneTimeCode
            }
        }
        codeAlert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
        codeAlert.addAction(UIAlertAction(title: "Подтвердить", style: .default, handler: { [weak self] (_) in
            guard let code = codeAlert.textFields?.first?.text else { return }
            
            
            self?.checkCode(phone: phone, code: code) { (isSuccess) in
                
                if isSuccess {
                    
                    self?.changePhone(to: phone,
                                      sHeader: Header(value: State.tokenToSave!, field: "X-Vsaas-Session"),
                                      completion: { (success) in
                                        if success {
                                            if !self!.isBack {
                                                UserDefaults.standard.set(State.tokenToSave, forKey: "session")
                                                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mainTabBar") as! UITabBarController
                                                vc.modalPresentationStyle = .fullScreen
                                                self?.activeView().present(vc, animated: true)
                                            } else {
                                                completion?()
                                            }
                                        }
                                      })
                    
                } else {
                    self?.wrongCode() {
                        self?.smsAlert(phone: phone) {
                            completion?()
                        }
                    }
                }
            }
        }))
        activeView().present(codeAlert, animated: true)
    }
    
    public func sendSms(phone: String, completion: (() -> Void)? = nil) {
        sendSms(phone: "\(phone)") { [weak self] (isSuccess) in
            if isSuccess {
                self?.smsAlert(phone: phone) {
                    completion?()
                }
            } else {
                let alert = UIAlertController(title: "Указанный номер телефона не привязан к договору. Пожалуйста, обратитесь в службу поддержки", message: nil, preferredStyle: .alert)
                alert.view.tintColor = UIColor(named: "primary")!
                
                alert.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: nil))
                alert.addAction(UIAlertAction(title: "+7 (473) 300-40-00", style: .default, handler: { (_) in
                    let phone = "+7 (473) 300-40-00"
                    if let url = URL(string: "tel://\(phone)"), UIApplication.shared.canOpenURL(url) {
                        UIApplication.shared.open(url)
                    }
                }))
                alert.addAction(UIAlertAction(title: "Попробовать еще раз", style: .default, handler: { (_) in
                    self?.smsAlert(phone: phone) {
                        completion?()
                    }
                }))
                self?.activeView().present(alert, animated: true)
            }
        }
    }
    
    public func wrongCode(againHandler: @escaping () -> Void) {
        let alert = UIAlertController(title: "Неверный код подтверждения", message: nil, preferredStyle:  .alert)
        alert.view.tintColor = UIColor(named: "primary")!
        alert.addAction(UIAlertAction(title: "Отмена", style: .cancel,
                                      handler: nil))
        alert.addAction(UIAlertAction(title: "Попробовать еще раз",
                                      style: .default, handler: { (_) in
                                        againHandler()
                                      }))
        activeView().present(alert, animated: true)
    }
    
    public func getUserInfo(login: String, complition: @escaping (Bool, Bool) -> Void) {
        let url = accountUrl + "vsaas/api/v1/get-user-info"
        
        let params: [String: Any] = [
            "login": login
        ]
        
        request(url: url,
                params: params,
                sessionHeader: Header(value: State.tokenToSave!, field: "X-Vsaas-Session")) { (value) in
            if let value = value as? [String: Any] {
                guard let phoneConf = value["PhoneConfirmed"] as? Bool else { return }
                guard let policyConf = value["PolicyConfirmed"] as? Bool else { return }
                complition(phoneConf, policyConf)
            }
        }
    }
    
    public func confirmPolicy(agrs: [Agreement], complition: @escaping () -> Void) {
        let url = accountUrl + "vsaas/api/v1/confirm-read-doc"
        
        let ids = agrs.map { (agr) -> String in
            return agr.id
        }
        
        let params: [String: Any] = [
            "IdDoc": ids
        ]
        
        request(url: url,
                params: params,
                sessionHeader: Header(value: State.tokenToSave!, field: "X-Vsaas-Session")) { (value) in
            if let value = value as? [String: Any],
               let success = value["Success"] as? Bool {
                if success {
                    complition()
                }
            }
        }
    }
    
    public func errorAlert(title: String? = "Ошибка", text: String, tintColor: UIColor? = UIColor(named: "primary"), comlition: (() -> Void)? = nil) {
        let vc = activeView()
        let alert = UIAlertController(title: title, message: text, preferredStyle: .alert)
        if let tintColor = tintColor {
            alert.view.tintColor = tintColor
        }
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
            comlition?()
        }))
        vc.present(alert, animated: true)
    }
    
    public func isDemoUser() -> Bool {
        guard let user = UserDefaults.standard.string(forKey: "userLogin") else { return false }
        guard let pass = UserDefaults.standard.string(forKey: "userPass") else { return false }
        
        if user == "androiddev1" && pass == "androiddev1" {
            return true
        } else {
            return false
        }
    }
    
    public func forgetPass() {
        if let url = URL(string: "https://воронеж.онлайн/camlogin/?action=lostpassword") {
            UIApplication.shared.open(url)
        }
    }
    
    public func sendSupportMail(name: String, contact: String, message: String, complition: @escaping () -> Void?) {
        let url = self.startUrl + "sendmail"
        
        validateSession() {
            
            LoadingService.shared.show()
            
            let request = AF.request(url,
                                     method: .post,
                                     parameters: ["name": name,
                                                  "emailorphone": contact,
                                                  "message": message],
                                     headers: self.headers)
            request.response { response in
                switch response.result {
                case .success(_):
                    DispatchQueue.main.async {
                        
                        LoadingService.shared.dismiss()
                        complition()
                    }
                case .failure(let err):
                    self.errorAlert(text: err.localizedDescription)
                }
            }
        }
    }
    
    public func logIn(login: String, password: String, complition: @escaping (Session) -> Void) {
        let url = "https://xn--b1agd0aean.xn--80asehdb/vsaas/jwt-auth/v1/token"
        
        UserDefaults.standard.set(login, forKey: "userLogin")
        UserDefaults.standard.set(password, forKey: "userPass")
        
        
        LoadingService.shared.show()
        
        let request = AF.request(url,
                                 method: .post,
                                 parameters: ["login": login,
                                              "password": password])
        request.responseDecodable(of: Session.self) { (response) in
            switch response.result {
            case .success(let value):
                DispatchQueue.main.async {
                    
                    LoadingService.shared.dismiss()
                    if value.session != nil {
                        complition(value)
                    } else if let messageError = value.message {
                        self.errorAlert(text: messageError)
                    }
                }
            case .failure(_):
                DispatchQueue.main.async {
                    
                    LoadingService.shared.dismiss()
                    self.errorAlert(text: "Логин или пароль неверны. Проверьте введеные данные и попробуйте еще раз")
                }
            }
        }
    }
    
    public func isVideoEnabled(complition: @escaping (Bool) -> Void) {
        let url = accountUrl + "vsaas/api/v1/get-cur-services-list"
        
        createHeaders()
        
        request(url: url,
                sessionHeader: sessionHeader!) { (value) in
            if let value = value as? [String: Any],
               let video = value["video"] as? Bool,
               let privateHouse = value["PrivateHouse"] as? Bool {
                
                State.privateHouse = privateHouse
                
                if let serviceMessageValue = value["Message"] as? [String: Any] {
                    decodeFromDictionary(
                        serviceMessageValue,
                        to: ServiceMessage.self) { (serviceMessage) in
                        State.serviceMessage = serviceMessage
                    }
                }
                
                if let messageTv = value["MessageTV"] as? String {
                    State.messageTv = messageTv
                }
                
                complition(video)
            }
        }
        
    }
    
    public func confirmReadDoc(id: String, completion: @escaping () -> Void) {
        let url = accountUrl + "vsaas/api/v1/confirm-read-doc"
        
        let params = [
            "IdDoc": [id]
        ]
        
        createHeaders()
        
        
        LoadingService.shared.show()
        request(url: url,
                params: params,
                sessionHeader: sessionHeader!) { (value) in
            
            LoadingService.shared.dismiss()
            if let value = value as? [String: Any],
               let success = value["Success"] as? Bool {
                if success {
                    completion()
                }
            }
        }
    }
    
    public func payBills(name: String,
                         login: String,
                         email: String,
                         toPay: String,
                         complition: @escaping (String) -> Void) {
        let url = accountUrl + "vsaas/api/v1/payement"
        
        let params: [String: Any] = [
            "clientid": name,
            "client_email": email,
            "sum": toPay,
            "orderid": login
        ]
        
        createHeaders()
        
        
        LoadingService.shared.show()
        
        request(url: url,
                params: params,
                sessionHeader: sessionHeader!) { [weak self] (value) in
            
            LoadingService.shared.dismiss()
            if let value = value as? [String: Any],
               let payUrl = value["url"] as? String {
                complition(payUrl)
            } else {
                self?.showFailure()
            }
        }
    }
    
    public func changePhone(to newPhone: String,
                            sHeader: Header? = nil,
                            completion: @escaping (Bool) -> Void) {
        let url = accountUrl + "vsaas/api/v1/change-mob-number"
        
        createHeaders()
        
        
        LoadingService.shared.show()
        
        var header = sHeader
        if header == nil {
            header = sessionHeader!
        }
        
        request(url: url,
                params: ["new-phone": newPhone],
                sessionHeader: header) { [weak self] (value) in
            
            LoadingService.shared.dismiss()
            if let value = value as? [String: Any],
               let result = value["Success"] as? Bool {
                completion(result)
            } else {
                self?.showFailure()
            }
        }
    }
    
    public func changeEmail(to newEmail: String,
                            sHeader: Header? = nil,
                            completion: @escaping (Bool) -> Void) {
        let url = accountUrl + "vsaas/api/v1/change-mail"
        
        createHeaders()
        
        LoadingService.shared.show()
        
        var header = sHeader
        if header == nil {
            header = sessionHeader!
        }
        
        request(url: url,
                params: ["new-email": newEmail],
                sessionHeader: header) { [weak self] (value) in
            
            LoadingService.shared.dismiss()
            if let value = value as? [String: Any],
               let result = value["Success"] as? Bool {
                completion(result)
            } else {
                self?.showFailure()
            }
        }
    }
    
    public func videoConnectRequest(name: String,
                                    phone: String,
                                    email: String,
                                    message: String,
                                    count: String,
                                    completion: @escaping (String) -> Void) {
        let url = accountUrl + "vsaas/api/v1/sendmailsell"
        
        validateSession() { [weak self] in
            request(url: url,
                    params: [
                        "name": name,
                        "phone": phone,
                        "email": email,
                        "message": message,
                        "count": count
                    ],
                    sessionHeader: self?.sessionHeader!) { (value) in
                if let value = value as? [String: Any],
                   let info = value["info"] as? String {
                    completion(info)
                } else {
                    self?.showFailure()
                }
            }
        }
    }
    
    //MARK: – Восстановление аккаунта
    
    //Отправить код на номер телефона
    public func restorePassWithPhone(phone: String, complition: @escaping (Bool) -> Void) {
        
        let url = accountUrl + "vsaas/api/v1/get-operation-sms-code"
        
        let params: [String: Any] = [
            "phone": phone
        ]
        
        request(url: url,
                params: params) { (value) in
            guard let value = value as? [String: Any] else { return }
            if let smsCorrect = value["SendSMSCode"] as? Bool {
                State.phoneToRestore = phone
                complition(smsCorrect)
            } 
        }
        
    }
    
    //Ввести код из смс
    public func getContracts(phone: String, code: String, complition: @escaping (Bool, Bool, [Contract]) -> Void) {
        let url = accountUrl + "vsaas/api/v1/get-contracts-in-phone"
        
        let params = [
            "phone": phone,
            "code": code
        ]
        
        request(url: url,
                params: params) { (value) in
            guard let value = value as? [String: Any] else { return }
            if let smsCorrect = value["SMSCodeCorrect"] as? Bool,
               let phoneExists = value["PhoneExists"] as? Bool {
                if let address = value["address"] as? [[String: Any]] {
                    decodeFromArray(address, to: Contract.self) { (contracts) in
                        complition(smsCorrect, phoneExists, contracts ?? [])
                    }
                } else {
                    complition(smsCorrect, phoneExists, [])
                }
            }
        }
    }
    
    public func getNewPasswordBySMS(complition: @escaping (String) -> Void) {
        let url = accountUrl + "vsaas/api/v1/restore-pswd-by-phone"
        
        let params: [String: Any] = [
            "phone": State.phoneToRestore
        ]
        
        validateSession() { [weak self] in
            request(url: url,
                    params: params,
                    sessionHeader: self?.sessionHeader) { (value) in
                if let value = value as? [String: Any] {
                    complition(value["message"] as! String)
                }
            }
        }
    }
    
    public func getAgreements(complition: @escaping ([Agreement]) -> Void) {
        let url = accountUrl + "vsaas/api/v1/get-list-docs-rules-for-review"
        
        let sHeader = Header(value: State.tokenToSave!, field: "X-Vsaas-Session")
        
        request(url: url,
                sessionHeader: sHeader,
                responseIsArray: true) { [weak self] (value) in
            if let value = value as? [[String: Any]] {
                decodeFromArray(value, to: Agreement.self) { (agreements) in
                    if let agreements = agreements {
                        complition(agreements)
                    } else {
                        self?.errorAlert(text: "Что-то пошло не так. Попробуйте еще раз")
                    }
                }
            } else {
                self?.errorAlert(text: "Что-то пошло не так. Попробуйте еще раз")
            }
        }
    }
    
    public func getUserData(complition: @escaping (PersonalData) -> Void) {
        let url = accountUrl + "vsaas/api/v2/get-contract-detail"
        
        validateSession() { [weak self] in
            request(url: url,
                    params: [
                        "login": UserDefaults.standard.string(forKey: "userLogin")!
                    ],
                    sessionHeader: self?.sessionHeader!) { (value) in
                if let value = value as? [String: Any] {
                    decodeFromDictionary(value, to: PersonalData.self) { [weak self] (personalData) in
                        if let personalData = personalData {
                            complition(personalData)
                        } else {
                            self?.errorAlert(text: "Что-то пошло не так. Попробуйте еще раз")
                        }
                    }
                } else {
                    self?.errorAlert(text: "Что-то пошло не так. Попробуйте еще раз")
                }
            }
        }
    }
    
    public func sendSupport(name: String,
                            email: String,
                            phone: String,
                            message: String,
                            subject: SupportType,
                            complition: @escaping (String) -> Void) {
        
        let url = accountUrl + "vsaas/api/v2/app/sendmail"
        
        let params: [String: Any] = [
            "name": name,
            "email": email,
            "phone": phone,
            "message": message,
            "subject": subject.rawValue
        ]
        
        checkAuthorization()
        
        LoadingService.shared.show()
        request(url: url,
                params: params,
                sessionHeader: sessionHeader!) { (value) in
            
            LoadingService.shared.dismiss()
            guard let value = value as? [String: Any],
                  let info = value["info"] as? String else { return }
            complition(info)
        }
    }
    
    public func getPersonalOffers(complition: @escaping ([PersonalOffer]) -> Void) {
        let url = accountUrl + "vsaas/api/v1/get-special-offer"
        
        checkAuthorization()
        
        LoadingService.shared.show()
        request(url: url,
                sessionHeader: sessionHeader!) { [weak self] (value) in
            
            LoadingService.shared.dismiss()
            if let value = value as? [[String: Any]] {
                decodeFromArray(value, to: PersonalOffer.self) { (offers) in
                    if let offers = offers {
                        complition(offers)
                    } else {
                        self?.errorAlert(text: "Что-то пошло не так. Попробуйте еще раз")
                    }
                }
            } else {
                self?.errorAlert(text: "Что-то пошло не так. Попробуйте еще раз")
            }
            
        }
    }
    
    public func getPersonalAkcis(complition: @escaping ([PersonalOffer]) -> Void) {
        let url = accountUrl + "vsaas/api/v1/get-actions"
        
        checkAuthorization()
        
        LoadingService.shared.show()
        request(url: url,
                sessionHeader: sessionHeader!) { [weak self] (value) in
            
            LoadingService.shared.dismiss()
            if let value = value as? [[String: Any]] {
                decodeFromArray(value, to: PersonalOffer.self) { (offers) in
                    if let offers = offers {
                        complition(offers)
                    } else {
                        self?.errorAlert(text: "Что-то пошло не так. Попробуйте еще раз")
                    }
                }
            } else {
                self?.errorAlert(text: "Что-то пошло не так. Попробуйте еще раз")
            }
            
        }
    }
    
    public func getTariffsAndSubServices(complition: @escaping ([Tariff], [Tariff], [Tariff], [SubService]) -> Void) {
        let url = accountUrl + "vsaas/api/v1/get-recommended-offer"
        
        checkAuthorization()
        
        LoadingService.shared.show()
        request(url: url,
                sessionHeader: sessionHeader!) { [weak self] (value) in
            
            var intNTv: [Tariff] = []
            var int: [Tariff] = []
            var tv: [Tariff] = []
            var subServcs: [SubService] = []
            
            LoadingService.shared.dismiss()
            if let value = value as? [String: Any] {
                
                if let tariffs = value["internt_tv"] as? [[String: Any]] {
                    decodeFromArray(tariffs, to: Tariff.self) { (internetTariffs) in
                        if let internetTariffs = internetTariffs {
                            intNTv = internetTariffs
                        } else {
                            self?.errorAlert(text: "Что-то пошло не так. Попробуйте еще раз")
                        }
                    }
                } else {
                    self?.errorAlert(text: "Что-то пошло не так. Попробуйте еще раз")
                }
                
                if let tariffs = value["internet"] as? [[String: Any]] {
                    decodeFromArray(tariffs, to: Tariff.self) { (internetTariffs) in
                        if let internetTariffs = internetTariffs {
                            int = internetTariffs
                        } else {
                            self?.errorAlert(text: "Что-то пошло не так. Попробуйте еще раз")
                        }
                    }
                } else {
                    self?.errorAlert(text: "Что-то пошло не так. Попробуйте еще раз")
                }
                
                if let tariffs = value["tv"] as? [[String: Any]] {
                    decodeFromArray(tariffs, to: Tariff.self) { (internetTariffs) in
                        if let internetTariffs = internetTariffs {
                            tv = internetTariffs
                        } else {
                            self?.errorAlert(text: "Что-то пошло не так. Попробуйте еще раз")
                        }
                    }
                } else {
                    self?.errorAlert(text: "Что-то пошло не так. Попробуйте еще раз")
                }
                
                if let tariffs = value["sub_services"] as? [[String: Any]] {
                    decodeFromArray(tariffs, to: SubService.self) { (internetTariffs) in
                        if let internetTariffs = internetTariffs {
                            subServcs = internetTariffs
                        } else {
                            self?.errorAlert(text: "Что-то пошло не так. Попробуйте еще раз")
                        }
                    }
                } else {
                    self?.errorAlert(text: "Что-то пошло не так. Попробуйте еще раз")
                }
                
            }
            
            complition(intNTv, int, tv, subServcs)
        }
    }
    
    //MARK: – TKC Video
    
    //Получение списка всех камер на аккаунте
    public func getAllCams(animated: Bool? = true, complition: @escaping ([Camera]) -> Void) {
        let url = accountUrl + "vsaas/api/app/allcam"
        
        validateSession() { [weak self] in
            
            if animated! {
                
                LoadingService.shared.show()
            }
            
            request(url: url,
                    sessionHeader: self?.sessionHeader!,
                    responseIsArray: true) { (value) in
                if let value = value as? [[String: Any]] {
                    decodeFromArray(value, to: Camera.self) { (cameras) in
                        guard let cameras = cameras else {
                            if UserDefaults.standard.string(forKey: "account") != nil {
                                self?.errorAlert(text: "Что-то пошло не так. Попробуйте еще раз")
                            }
                            return
                        }
                        
                        LoadingService.shared.dismiss()
                        complition(cameras)
                    }
                } else {
                    
                    LoadingService.shared.dismiss()
                    if UserDefaults.standard.string(forKey: "account") != nil {
                        self?.errorAlert(text: "Что-то пошло не так. Попробуйте еще раз")
                    }
                }
            }
        }
        
    }
    
    //Получение списка организаций
    public func getAllOrganizations(complition: @escaping ([Object]) -> Void) {
        let url = accountUrl + "vsaas/api/app/object"
        
        validateSession() { [weak self] in
            
            
            LoadingService.shared.show()
            request(url: url,
                    sessionHeader: self!.sessionHeader) { (value) in
                if let value = value as? [String: Any] {
                    decodeFromDictionary(value, to: ObjectDecode.self) { (response) in
                        
                        LoadingService.shared.dismiss()
                        guard let objects = response?.objects else {
                            self?.showFailure()
                            return
                        }
                        complition(objects)
                    }
                } else {
                    
                    LoadingService.shared.dismiss()
                    self?.showFailure()
                }
            }
        }
        
    }
    
    //Получение списка камер по организации
    public func getCams(by organization: String, complition: @escaping ([Camera]) -> Void) {
        let url = accountUrl + "organizationcam"
        
        let params: [String: Any] = [
            "organization": organization
        ]
        
        validateSession() { [weak self] in
            
            
            LoadingService.shared.show()
            
            request(url: url,
                    params: params,
                    sessionHeader: self!.sessionHeader,
                    responseIsArray: true) { (value) in
                if let value = value as? [[String: Any]] {
                    decodeFromArray(value, to: Camera.self) { (response) in
                        
                        LoadingService.shared.dismiss()
                        guard let cameras = response else {
                            self?.showFailure()
                            return
                        }
                        complition(cameras)
                    }
                } else {
                    
                    LoadingService.shared.dismiss()
                    self?.showFailure()
                }
            }
        }
        
    }
    
    //Данные камеры
    public func getCameraPort(name: String, completion: @escaping (Int) -> Void) {
        let url = accountUrl + "vsaas/api/v2/cameras?search=\(name)"
        
        createHeaders()
        
        request(url: url,
                sessionHeader: sessionHeader!,
                responseIsArray: true) { (value) in
            if let value = value as? [[String: Any]],
               let status = value.first?["stream_status"] as? [String: Any],
               let port = status["https_port"] as? Int {
                completion(port)
            }
        }
    }
    
    //Изменение имени камеры
    public func renameName(to newName: String, for cameraID: String, complition: @escaping () -> Void) {
        let url = accountUrl + "vsaas/api/app/renamecam"
        
        validateSession() { [weak self] in
            //
            LoadingService.shared.show()
            
            request(url: url,
                    method: .put,
                    params: ["id_cam": cameraID,
                             "rename_text": newName],
                    sessionHeader: self!.sessionHeader!) { (value) in
                
                LoadingService.shared.dismiss()
                if let value = value as? [String: Any],
                   let info = value["info"] as? String,
                   info == "complite" {
                    complition()
                }
            }
        }
        
        
    }
    
    //Добавление камеры в избранное
    public func changeFavorite(for cameraID: String, to favorite: String, complition: @escaping () -> Void?) {
        
        let url = accountUrl + "vsaas/api/app/changefavorite"
        
        validateSession() { [weak self] in
            //
            LoadingService.shared.show()
            
            request(url: url,
                    method: .put,
                    params: ["id_cam": cameraID,
                             "change": favorite],
                    sessionHeader: self!.sessionHeader!) { (value) in
                
                LoadingService.shared.dismiss()
                if let value = value as? [String: Any],
                   let info = value["info"] as? String,
                   info == "complite" {
                    complition()
                }
            }
        }
        
        
    }
    
    //MARK: – TKC TV
    
    //Обновление списка категорий
    public func updateCategories(complition: @escaping ([TvCategory]) -> Void) {
        let url = accountUrl + "vsaas/api/app/tv/category"
        //tvUrl + "category"
        
        validateSession() {
            
            
            LoadingService.shared.show()
            
            let request = AF.request(url, method: .get, headers: self.headers)
            request.responseDecodable(of: [TvCategory].self) { response in
                switch response.result {
                case .success(let value):
                    DispatchQueue.main.async {
                        
                        LoadingService.shared.dismiss()
                        TvCategory.categories = value
                        complition(value)
                    }
                case .failure(let err):
                    DispatchQueue.main.async {
                        
                        LoadingService.shared.dismiss()
                        print(err)
                    }
                }
            }
        }
    }
    
    //Обновление списка тв каналов
    public func updateChanells(categoryId: Int, complition: @escaping ([Chanell]) -> Void) {
        let url = accountUrl + "vsaas/api/app/tv/chanel"
        
        let params: [String: Any] = [
            "id_category": categoryId
        ]
        
        validateSession() { [weak self] in
            
            //
            LoadingService.shared.show()
            
            request(url: url,
                    method: .post,
                    params: params,
                    sessionHeader: self?.sessionHeader!,
                    responseIsArray: true) { (value) in
                
                LoadingService.shared.dismiss()
                
                if let value = value as? [[String: Any]] {
                    var chanells: [Chanell] = []
                    for val in value {
                        print(val)
                        decodeFromDictionary(val, to: Chanell.self) { (chanell) in
                            if let chanell = chanell {
                                chanells.append(chanell)
                            } else {
                                print("Error with decoding val")
                            }
                        }
                    }
                    complition(chanells)
                    print(chanells)
                }
            }
        }
    }

    
    public func registerTokenOnServer(token: String, completion: @escaping (Bool) -> Void) {
        let url = accountUrl + "vsaas/api/v1/sent-token-apppushnotification"
        
        let params: [String: Any] = [
            "token": token,
            "typeOS": "iOS"
        ]
        
        createHeaders()
        
        validateSession() { [weak self] in
            request(url: url,
                    method: .post,
                    params: params,
                    sessionHeader: self?.sessionHeader,
                    responseIsArray: false) { value in
                if let value = value as? [String: Any],
                   let success = value["Success"] as? Bool {
                    DispatchQueue.main.async {
                        completion(success)
                    }
                } else {
                    print("Error with register token to server")
                }
            }
        }
    }
    
    func getAllServerMessages(completion: @escaping ([FBNotification]?) -> Void) {
        let url = accountUrl + "vsaas/api/v1/get-push-notification"
        
        guard let token = State.fcmToken else { return }
        
        validateSession() { [weak self] in
            request(url: url,
                    method: .post,
                    sessionHeader: self?.sessionHeader,
                    responseIsArray: true) { (value) in
                if let value = value as? [[String: Any]] {
                    decodeFromArray(value, to: FBNotification.self) { (nots) in
                        guard let nots = nots else {
                            print("Error with decoding val")
                            completion(nil)
                            return
                        }
                        completion(nots)
                    }
                }
            }
        }
    }
    
    //Получение телепрограммы
    public func getTeleprogram(chanellId: String, complition: @escaping ([Teleprogram]) -> Void) {
        let url = accountUrl + "vsaas/api/app/tv/teleprogram"
        
        let params: [String: Any] = [
            "id_chanel": chanellId
        ]
        
        createHeaders()
        
        validateSession() { [weak self] in
            
            request(url: url,
                    method: .post,
                    params: params,
                    sessionHeader: self?.sessionHeader!,
                    responseIsArray: true) { (value) in
                
                if let value = value as? [[String: Any]] {
                    var teleprograms: [Teleprogram] = []
                    print(value)
                    decodeFromArray(value, to: Teleprogram.self) { (tlmps) in
                        if let tlpms = tlmps {
                            teleprograms = tlpms
                        } else {
                            print("Error with decoding val")
                        }
                    }
                    complition(teleprograms)
                    print(teleprograms)
                }
            }
        }
    }
    
    func makeDates(program: [Teleprogram]) {
        
        for p in program {
            let pDay = p.day
            let today = Calendar.current.component(.day, from: Date())
            
            print("\(pDay) to \(today)")
        }
    }
    
    
}
